-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2017 at 03:55 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klana`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_sessions`
--

CREATE TABLE `app_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_sessions`
--

INSERT INTO `app_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('f03056318c4e297bb3ceabca7518524a55890522', '192.168.1.226', 1513671239, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333635313332323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303031223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133363530373438223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a31353a2261646d696e4061646d696e2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('c07e38a78812f665a86d6222e92c91e1e2349404', '192.168.1.226', 1513678019, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333635313939303b),
('1f88c9d444f0575a76f3f2f49dfe27d7f0688631', '192.168.1.211', 1513677526, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333635323936383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303031223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133363531333331223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a31353a2261646d696e4061646d696e2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('86b8072b4225345889a5436163a08d299caef07a', '192.168.1.51', 1513679478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333636353939353b6964656e746974797c733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b656d61696c7c733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303035223b6f6c645f6c6173745f6c6f67696e7c4e3b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('e3dff03c9c2fa0fec8dab4b1238dc7ffa188d229', '192.168.1.105', 1513680832, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333636363733323b),
('6d41f6351660557e1b3330eb408c9fea95bdbcb3', '192.168.1.124', 1513678751, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333637383735313b),
('0ee32ff7e3170a836c6f3cc7d340b941e843a3d7', '::1', 1513685522, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333638303133353b),
('331f6d4b894cf588a91bad1e9cf2d0ec4f25c3c6', '::1', 1513691329, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333638353533363b),
('0f1a92c06fea7ecb1bb3c4d9e386a819503124a0', '::1', 1513695746, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333836343134353b),
('447c24e7c1fe33e1d76f1551490162eb6cc8eb66', '192.168.1.51', 1513737394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733373132353b6964656e746974797c733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b656d61696c7c733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303035223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133363636303038223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a32343a226c7a756c6b61726e61656e31313140676d61696c2e636f6d223b733a383a2270617373776f7264223b733a393a226d6961616e64696e69223b7d),
('751f46448f367cbd770d4ee342a098bb5510640e', '192.168.1.211', 1513737145, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733373133323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303031223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133363637343036223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a31353a2261646d696e4061646d696e2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('0d89de4ce4fe824196d2298fa62920314696d16e', '::1', 1513737442, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733373136323b),
('f70b61945867b0000645a735023248f6a5353cdf', '192.168.1.124', 1513737521, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733373530383b6964656e746974797c733a31393a226d68617379696d303740676d61696c2e636f6d223b656d61696c7c733a31393a226d68617379696d303740676d61696c2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303033223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133363530383534223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a31393a226d68617379696d303740676d61696c2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('372f402e205ea5c99cbaf12012da27405ab710ad', '::1', 1513738213, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733383038383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a31353a22555352323031373132303030303031223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353133373337313334223b757365725f6c6f67696e7c613a323a7b733a353a22656d61696c223b733a31353a2261646d696e4061646d696e2e636f6d223b733a383a2270617373776f7264223b733a383a2270617373776f7264223b7d),
('b0a9f4e9201cc3551ecf664585e99b18a77fb470', '192.168.1.226', 1513738353, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333733383335333b);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Superadmin', 'Super Administrator'),
(2, 'Agent', 'Agent'),
(4, 'Tripper', 'Tripper'),
(5, 'Admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id_logs` int(11) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `data_new` text,
  `data_old` text,
  `data_change` text,
  `message` text NOT NULL,
  `created_on` datetime NOT NULL,
  `activity` enum('C','U','D') NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id_logs`, `id_user`, `data_new`, `data_old`, `data_change`, `message`, `created_on`, `activity`, `type`) VALUES
(2, 'USR201712000001', '{\"Code Agent\":\"AGT201712000001\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Approve Date\":\"18 December 2017 19:36:11\",\"Approve by\":\"Super Admin\"}', NULL, NULL, 'Agent Muhammar Rafsanjani is successfuly approved by Super Admin', '2017-12-18 19:36:11', 'C', 10),
(3, 'USR201712000002', '{\"Code FAQ\":\"FAQ201712003\",\"Title\":\"Aa\",\"Content\":\"<p>aa<\\/p>\\r\\n\",\"File Name\":\"\",\"Status\":{\"status\":\"image-blank\"},\"Created On\":\"2017-12-18 20:18:33\"}', NULL, NULL, 'Muhammar Rafsanjani add to data  Aa', '2017-12-18 20:18:33', 'C', 9),
(4, 'USR201712000001', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Monumen Nasional\"}', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"Aceh\",\"Destination\":\"Monumen Nasional\"}', '{\"Province\":\"DKI Jakarta\"}', 'Destination Monumen Nasional is successfuly changed by Super Admin', '2017-12-18 21:54:54', 'U', 5),
(5, 'USR201712000001', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Monumen Nasional\"}', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Monumen Nasional\"}', '[]', 'Destination Monumen Nasional is successfuly changed by Super Admin', '2017-12-18 21:55:00', 'U', 5),
(6, 'USR201712000001', '{\"Code Destination\":\"DST2017120003\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Pulau Seribu\"}', '{\"Code Destination\":\"DST2017120003\",\"Province\":\"Aceh\",\"Destination\":\"Pulau Seribu\"}', '{\"Province\":\"DKI Jakarta\"}', 'Destination Pulau Seribu is successfuly changed by Super Admin', '2017-12-18 21:55:08', 'U', 5),
(7, 'USR201712000001', '{\"Code Destination\":\"DST2017120001\",\"Province\":\"Nusa Tenggara Barat\",\"Destination\":\"Gili Trawangan\"}', '{\"Code Destination\":\"DST2017120001\",\"Province\":\"Aceh\",\"Destination\":\"Gili Trawangan\"}', '{\"Province\":\"Nusa Tenggara Barat\"}', 'Destination Gili Trawangan is successfuly changed by Super Admin', '2017-12-18 21:55:22', 'U', 5),
(8, 'USR201712000001', '{\"Code Destination\":\"DST2017120001\",\"Province\":\"Nusa Tenggara Barat\",\"Destination\":\"Gili Trawangan\"}', '{\"Code Destination\":\"DST2017120001\",\"Province\":\"Nusa Tenggara Barat\",\"Destination\":\"Gili Trawangan\"}', '[]', 'Destination Gili Trawangan is successfuly changed by Super Admin', '2017-12-18 21:55:26', 'U', 5),
(9, 'USR201712000001', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Monumen Nasional\"}', '{\"Code Destination\":\"DST2017120002\",\"Province\":\"DKI Jakarta\",\"Destination\":\"Monumen Nasional\"}', '[]', 'Destination Monumen Nasional is successfuly changed by Super Admin', '2017-12-18 21:55:30', 'U', 5),
(10, 'USR201712000001', '{\"Full Name\":null,\"Username\":\"daffapratama\",\"Email\":\"daffapratama2011@gmail.com\",\"Users Level\":\"Tripper\",\"Registration Date\":\"18 December 2017\"}', NULL, NULL, 'administrator has registered  as Tripper', '2017-12-18 09:44:27', 'C', 1),
(11, 'USR201712000001', '{\"Code Package\":\"PCK2017120001\",\"Package Name\":\"Gold Package\",\"Days\":\"3\",\"Nights\":\"2\"}', '{\"Code Package\":\"PCK2017120001\",\"Package Name\":\"Gold Package\",\"Days\":3,\"Nights\":2}', '[]', 'Package Gold Package is successfuly changed by Super Admin', '2017-12-18 10:01:06', 'U', 7),
(12, 'USR201712000001', '{\"Code Package\":\"PCK2017120002\",\"Package Name\":\"Medal Package\",\"Days\":\"8\",\"Nights\":\"7\"}', '{\"Code Package\":\"PCK2017120002\",\"Package Name\":\"Medal Package\",\"Days\":8,\"Nights\":7}', '[]', 'Package Medal Package is successfuly changed by Super Admin', '2017-12-18 10:01:10', 'U', 7),
(13, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung \"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Bandung is successfuly changed by Super Admin', '2017-12-18 10:21:16', 'U', 11),
(14, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Bandung is successfuly changed by Super Admin', '2017-12-18 10:23:18', 'U', 11),
(15, 'USR201712000003', '{\"Full Name\":\"Muhamad Fachry\",\"Username\":\"fachryuzaki\",\"Email\":\"fachryuzaki@email.com\",\"Users Level\":\"2\"}', '{\"Full Name\":\"Muhamad Fachry\",\"Username\":\"fachryuzaki\",\"Email\":\"fachryuzaki@email.com\",\"Users Level\":\"2\"}', '[]', 'User  is successfuly changed by Muhamad Fachry', '2017-12-18 10:29:33', 'U', 1),
(16, 'USR201712000003', '{\"Full Name\":\"Muhamad Fachry\",\"Username\":\"fachryuzaki\",\"Email\":\"mhasyim07@gmail.com\",\"Users Level\":\"1\"}', '{\"Full Name\":\"Muhamad Fachry\",\"Username\":\"fachryuzaki\",\"Email\":\"mhasyim07@gmail.com\",\"Users Level\":\"1\"}', '[]', 'User  is successfuly changed by Muhamad Fachry', '2017-12-18 10:30:26', 'U', 1),
(17, 'USR201712000003', '{\"Full Name\":\"Muhamad Fachry Hasyim\",\"Username\":\"fachryuzaki\",\"Email\":\"mhasyim07@gmail.com\",\"Users Level\":\"2\"}', '{\"Full Name\":\"Muhamad Fachry Hasyim\",\"Username\":\"fachryuzaki\",\"Email\":\"mhasyim07@gmail.com\",\"Users Level\":\"2\"}', '[]', 'User  is successfuly changed by Muhamad Fachry', '2017-12-18 10:30:37', 'U', 1),
(18, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Approve Date\":\"18 December 2017 11:15:17\",\"Approval Status\":\"Approve\"}', NULL, NULL, 'Content Tour to Bandung is successfuly approved by Super Admin', '2017-12-18 11:15:17', 'C', 11),
(19, 'USR201712000001', '{\"Code Content\":\"CNT2017120002\",\"Content Title\":\"TOUR ToUR\",\"Approve Date\":\"18 December 2017 11:26:16\",\"Approval Status\":\"Reject\"}', NULL, NULL, 'Content TOUR ToUR is successfuly rejected by Super Admin', '2017-12-18 11:26:16', 'C', 11),
(20, 'USR201712000001', NULL, '{\"Code Content\":\"CNT2017120002\",\"Content Title\":\"TOUR ToUR\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', NULL, 'Content TOUR ToUR is successfuly deleted by Super Admin', '2017-12-18 11:31:45', 'D', 11),
(21, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"18 December 2017 12:09:44\",\"Publish Status\":\"Published\"}', NULL, NULL, 'Content Tour to Bandung is successfuly published by Super Admin', '2017-12-18 12:09:44', 'C', 11),
(22, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"18 December 2017 13:26:06\",\"Publish Status\":\"Unpublished\"}', NULL, NULL, 'Content Tour to Bandung is successfuly unpublished by Super Admin', '2017-12-18 13:26:06', 'C', 11),
(23, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"18 December 2017 13:29:56\",\"Publish Status\":\"Published\"}', NULL, NULL, 'Content Tour to Bandung is successfuly published by Super Admin', '2017-12-18 13:29:56', 'C', 11),
(24, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:06:25', 'U', 11),
(25, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:07:34', 'U', 11),
(26, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:08:23', 'U', 11),
(27, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:09:33', 'U', 11),
(28, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:13:31', 'U', 11),
(29, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:24:13', 'U', 11),
(30, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:28:17', 'U', 11),
(31, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:29:27', 'U', 11),
(32, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:31:33', 'U', 11),
(33, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:32:51', 'U', 11),
(34, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:33:05', 'U', 11),
(35, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120003\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:33:52', 'U', 11),
(36, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":null,\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung<\\/p>\\r\\n\"}', '{\"Code Content\":\"CNT2017120001\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Bandung is successfuly changed by Super Admin', '2017-12-18 14:36:30', 'U', 11),
(37, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>lorem lorem lorem lorem lorem lorem lorem<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:42:08', 'U', 11),
(38, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<h2><strong>Pending Tour<\\/strong><\\/h2>\\r\\n\\r\\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.<\\/p>\\r\\n\\r\\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.<\\/p>\\r\\n\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.<\\/p>\\r\\n\\r\\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.<\\/p>\\r\\n\\r\\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.<\\/p>\\r\\n\\r\\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.<\\/p>\\r\\n\\r\\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.<\\/p>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<\\/li>\\r\\n\\t<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-18 14:56:21', 'U', 11),
(39, 'USR201712000001', '{\"Code Category\":\"CAT2017120001\",\"Category\":\"Diving\",\"Changed By\":\"USR201712000001\",\"Changed On\":\"2017-12-18 16:38:44\"}', '{\"Code Category\":\"CAT2017120001\",\"Category\":\"Sport\",\"Changed By\":null,\"Changed On\":null}', '{\"Category\":\"Diving\",\"Changed By\":\"USR201712000001\",\"Changed On\":\"2017-12-18 16:38:44\"}', 'Category Diving is successfuly changed by Super Admin', '2017-12-18 16:38:44', 'U', 6),
(40, 'USR201712000001', '{\"Code Category\":\"CAT2017120004\",\"Category\":\"Snorkling\",\"Created By\":\"USR201712000001\",\"Created On\":\"2017-12-18 16:39:00\"}', NULL, NULL, 'Super Admin add to data  Snorkling', '2017-12-18 16:39:00', 'C', 6),
(41, 'USR201712000001', '{\"Code Package\":\"PCK2017120003\",\"Package Name\":\"Silver Package\",\"Days\":\"2\",\"Night\":\"1\"}', NULL, NULL, 'Super Admin add to data  Silver Package', '2017-12-18 16:39:41', 'C', 7),
(42, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"18 December 2017 16:59:36\",\"Publish Status\":\"Unpublished\"}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"-\",\"Publish Status\":\"Published\"}', '{\"Publish Date\":\"18 December 2017 16:59:36\",\"Publish Status\":\"Unpublished\"}', 'Content Tour to Bandung is successfuly unpublished by Super Admin', '2017-12-18 16:59:36', 'U', 11),
(43, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"18 December 2017 16:59:43\",\"Publish Status\":\"Published\"}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"-\",\"Publish Status\":\"Unpublished\"}', '{\"Publish Date\":\"18 December 2017 16:59:43\",\"Publish Status\":\"Published\"}', 'Content Tour to Bandung is successfuly published by Super Admin', '2017-12-18 16:59:43', 'U', 11),
(44, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<h2><strong>Pending Tour<\\/strong><\\/h2>\\r\\n\\r\\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.<\\/p>\\r\\n\\r\\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.<\\/p>\\r\\n\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.<\\/p>\\r\\n\\r\\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.<\\/p>\\r\\n\\r\\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.<\\/p>\\r\\n\\r\\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.<\\/p>\\r\\n\\r\\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.<\\/p>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<\\/li>\\r\\n\\t<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-19 09:42:54', 'U', 11),
(45, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"1000000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<h2><strong>Pending Tour<\\/strong><\\/h2>\\r\\n\\r\\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.<\\/p>\\r\\n\\r\\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.<\\/p>\\r\\n\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.<\\/p>\\r\\n\\r\\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.<\\/p>\\r\\n\\r\\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.<\\/p>\\r\\n\\r\\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.<\\/p>\\r\\n\\r\\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.<\\/p>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<\\/li>\\r\\n\\t<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-19 10:16:29', 'U', 11),
(46, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"19 December 2017 10:16:52\",\"Publish Status\":\"Unpublished\"}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"-\",\"Publish Status\":\"Published\"}', '{\"Publish Date\":\"19 December 2017 10:16:52\",\"Publish Status\":\"Unpublished\"}', 'Content Tour to Bandung is successfuly unpublished by Super Admin', '2017-12-19 10:16:52', 'U', 11),
(47, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"19 December 2017 10:16:58\",\"Publish Status\":\"Published\"}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"-\",\"Publish Status\":\"Unpublished\"}', '{\"Publish Date\":\"19 December 2017 10:16:58\",\"Publish Status\":\"Published\"}', 'Content Tour to Bandung is successfuly published by Super Admin', '2017-12-19 10:16:58', 'U', 11),
(48, 'USR201712000001', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"19 December 2017 10:19:02\",\"Publish Status\":\"Unpublished\"}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Publish Date\":\"-\",\"Publish Status\":\"Published\"}', '{\"Publish Date\":\"19 December 2017 10:19:02\",\"Publish Status\":\"Unpublished\"}', 'Content Tour to Bandung is successfuly unpublished by Super Admin', '2017-12-19 10:19:02', 'U', 11),
(49, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"1000000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<h2><strong>Pending Tour<\\/strong><\\/h2>\\r\\n\\r\\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.<\\/p>\\r\\n\\r\\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.<\\/p>\\r\\n\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.<\\/p>\\r\\n\\r\\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.<\\/p>\\r\\n\\r\\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.<\\/p>\\r\\n\\r\\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.<\\/p>\\r\\n\\r\\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.<\\/p>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<\\/li>\\r\\n\\t<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-19 10:28:39', 'U', 11),
(50, 'USR201712000001', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120003\",\"Content Title\":\"Pending Tour\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"1000000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<h2><strong>Pending Tour<\\/strong><\\/h2>\\r\\n\\r\\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.<\\/p>\\r\\n\\r\\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.<\\/p>\\r\\n\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.<\\/p>\\r\\n\\r\\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.<\\/p>\\r\\n\\r\\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.<\\/p>\\r\\n\\r\\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.<\\/p>\\r\\n\\r\\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.<\\/p>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<\\/li>\\r\\n\\t<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Pending Tour is successfuly changed by Super Admin', '2017-12-19 11:10:35', 'U', 11),
(51, 'USR201712000001', '{\"Full Name\":null,\"Username\":\"lzulkarnaen\",\"Email\":\"lzulkarnaen111@gmail.com\",\"Users Level\":\"Super Administrator\",\"Registration Date\":\"19 December 2017\"}', NULL, NULL, 'administrator has registered  as Super Administrator', '2017-12-19 13:46:16', 'C', 1),
(52, 'USR201712000005', '{\"Code Content Comment\":\"CNT2017120003\",\"Content Title\":null,\"Approve Date\":\"19 December 2017 14:11:54\",\"Approval Status\":\"Approve\"}', '{\"Code Content Comment\":\"CNT2017120003\",\"Content Title\":null,\"Approve Date\":\"-\",\"Approval Status\":\"Pending\"}', '{\"Approve Date\":\"19 December 2017 14:11:54\",\"Approval Status\":\"Approve\"}', 'Content Comment is successfuly approved by Iskandar Zulkarnaen', '2017-12-19 14:11:54', 'U', 12),
(53, 'USR201712000005', '{\"Code Content Comment\":\"CMT2017120002\",\"Content Title\":null,\"Approve Date\":\"19 December 2017 14:29:44\",\"Approval Status\":\"Approve\"}', '{\"Code Content Comment\":\"CMT2017120002\",\"Content Title\":null,\"Approve Date\":\"-\",\"Comment\":\"This Trip is fake, admin just make some joke for this.\",\"Approval Status\":\"Pending\"}', '{\"Approve Date\":\"19 December 2017 14:29:44\",\"Approval Status\":\"Approve\"}', 'Content CommentCMT2017120002 is successfuly approved by Iskandar Zulkarnaen', '2017-12-19 14:29:44', 'U', 12),
(54, 'USR201712000005', '{\"Code Content Comment\":\"CMT2017120003\",\"Content Title\":null,\"Approve Date\":\"19 December 2017 14:30:04\",\"Comment\":\"This Trip is fake, admin just make some joke for this.\",\"Approval Status\":\"Reject\"}', '{\"Code Content Comment\":\"CMT2017120003\",\"Content Title\":null,\"Approve Date\":\"-\",\"Comment\":\"This Trip is fake, admin just make some joke for this.\",\"Approval Status\":\"Pending\"}', '{\"Approve Date\":\"19 December 2017 14:30:04\",\"Approval Status\":\"Reject\"}', 'Content CommentCMT2017120003 is successfuly rejected by Iskandar Zulkarnaen', '2017-12-19 14:30:04', 'U', 12),
(55, 'USR201712000005', '{\"Code Content Comment\":\"CMT2017120004\",\"Content Title\":null,\"Approve Date\":\"19 December 2017 14:30:47\",\"Comment\":\"This Trip is fake, admin just make some joke for this.\",\"Approval Status\":\"Reject\"}', '{\"Code Content Comment\":\"CMT2017120004\",\"Content Title\":null,\"Approve Date\":\"-\",\"Comment\":\"This Trip is fake, admin just make some joke for this.\",\"Approval Status\":\"Pending\"}', '{\"Approve Date\":\"19 December 2017 14:30:47\",\"Approval Status\":\"Reject\"}', 'Content CommentCMT2017120004 is successfuly rejected by Iskandar Zulkarnaen', '2017-12-19 14:30:47', 'U', 12),
(56, 'USR201712000005', NULL, '{\"Code Content Comment\":\"CMT2017120004\",\"Content Title\":null,\"Comment\":\"This Trip is fake, admin just make some joke for this.\"}', NULL, 'Content CommentCMT2017120004 is successfuly deleted by Iskandar Zulkarnaen', '2017-12-19 14:32:33', 'D', 12),
(57, 'USR201712000005', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"5500000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Bandung is successfuly changed by Iskandar Zulkarnaen', '2017-12-19 15:22:04', 'U', 11),
(58, 'USR201712000005', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Gili\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Bandung\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"5500000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung<\\/p>\\r\\n\"}', '{\"Content Title\":\"Tour to Gili\",\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Gili is successfuly changed by Iskandar Zulkarnaen', '2017-12-19 15:23:52', 'U', 11),
(59, 'USR201712000005', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Gili\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', '{\"Code Content\":\"CNT2017120001\",\"Content Title\":\"Tour to Gili\",\"Agent Name\":\"Muhammar Rafsanjani\",\"Destination\":null,\"Price\":\"5500000\",\"Trevel Start\":\"01 January 1970\",\"Trevel End\":\"01 January 1970\",\"Description\":\"<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung<\\/p>\\r\\n\"}', '{\"Trevel Start\":\"20 December 2017\",\"Trevel End\":\"29 December 2017\",\"Description\":null}', 'Content Tour to Gili is successfuly changed by Iskandar Zulkarnaen', '2017-12-19 15:48:12', 'U', 11);

-- --------------------------------------------------------

--
-- Table structure for table `ms_category`
--

CREATE TABLE `ms_category` (
  `id_category` varchar(14) NOT NULL COMMENT 'CAT2017120001',
  `category` varchar(50) NOT NULL,
  `photopath` varchar(100) DEFAULT NULL,
  `photofilename` varchar(50) DEFAULT NULL,
  `createdon` timestamp NULL DEFAULT NULL,
  `createdby` varchar(15) DEFAULT NULL COMMENT 'from users',
  `changedon` timestamp NULL DEFAULT NULL,
  `changedby` varchar(15) DEFAULT NULL COMMENT 'from users',
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT 'F=> NOT DELETE, T => DELETED',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_category`
--

INSERT INTO `ms_category` (`id_category`, `category`, `photopath`, `photofilename`, `createdon`, `createdby`, `changedon`, `changedby`, `is_delete`, `delete_date`) VALUES
('CAT2017120001', 'Diving', NULL, '', '2017-12-15 08:06:33', 'USR201712000001', '2017-12-18 09:38:44', 'USR201712000001', 'F', NULL),
('CAT2017120002', 'Mountain', NULL, '', '2017-12-15 08:06:42', 'USR201712000001', NULL, NULL, 'F', NULL),
('CAT2017120003', 'Test', NULL, NULL, '2017-12-18 07:55:41', 'CAT2017120004', '2017-12-18 08:13:28', 'CAT2017120004', 'T', '2017-12-18 08:27:59'),
('CAT2017120004', 'Snorkling', NULL, '', '2017-12-18 09:39:00', 'USR201712000001', NULL, NULL, 'F', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_city`
--

CREATE TABLE `ms_city` (
  `id_city` varchar(14) NOT NULL COMMENT 'CTY2017120001',
  `id_province` varchar(14) NOT NULL,
  `city` varchar(30) NOT NULL,
  `is_delete` enum('F','T') NOT NULL COMMENT 'F=> NOT DELETE, T => DELETE',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_city`
--

INSERT INTO `ms_city` (`id_city`, `id_province`, `city`, `is_delete`, `delete_date`) VALUES
('CTY2017120001', 'PRV2017120001', 'Jakarta Selatan', 'F', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_content`
--

CREATE TABLE `ms_content` (
  `id_content` varchar(14) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `travel_title` varchar(100) NOT NULL,
  `travel_description` text NOT NULL,
  `activity` text NOT NULL,
  `id_category` varchar(14) NOT NULL,
  `id_destination` varchar(14) NOT NULL,
  `travel_start` date NOT NULL,
  `travel_end` date NOT NULL,
  `status_participant` enum('L','U') NOT NULL DEFAULT 'L' COMMENT 'L = Limited , U = Unlimited',
  `participants` int(11) DEFAULT NULL,
  `approval` enum('P','A','R') NOT NULL COMMENT 'P = Pending, R = Reject A = Approve',
  `date_approval` timestamp NULL DEFAULT NULL,
  `approve_by` varchar(15) NOT NULL,
  `publish_status` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'T = Publish , F = Unpublish',
  `publish_date` timestamp NULL DEFAULT NULL,
  `publish_by` varchar(15) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT '	F=> NOT DELETE, T => DELETE',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_content`
--

INSERT INTO `ms_content` (`id_content`, `id_user`, `travel_title`, `travel_description`, `activity`, `id_category`, `id_destination`, `travel_start`, `travel_end`, `status_participant`, `participants`, `approval`, `date_approval`, `approve_by`, `publish_status`, `publish_date`, `publish_by`, `created_on`, `is_delete`, `delete_date`) VALUES
('CNT2017120001', 'USR201712000002', 'Tour to Gili', '<p>Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung Bandung</p>\r\n', '', 'CAT2017120002', 'DST2017120001', '2017-12-20', '2017-12-29', 'L', 0, 'A', '2017-12-18 04:15:17', '', 'F', '2017-12-19 03:19:02', 'USR201712000001', '2017-12-15 05:18:31', 'F', NULL),
('CNT2017120002', 'USR201712000002', 'TOUR ToUR', '<p>lorem lorem lorem lorem lorem lorem lorem</p>\r\n', '', 'CAT2017120002', 'DST2017120001', '2017-12-20', '2017-12-29', 'L', 0, 'R', '2017-12-18 04:26:16', '', 'F', NULL, '', '2017-12-15 05:18:31', 'F', NULL),
('CNT2017120003', 'USR201712000002', 'Pending Tour', '<h2><strong>Pending Tour</strong></h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>\r\n\r\n<p>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.</p>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.</p>\r\n\r\n<p>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.</p>\r\n\r\n<p>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.</p>\r\n\r\n<p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.</p>\r\n\r\n<p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>\r\n\r\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.</p>\r\n\r\n<ul>\r\n	<li>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</li>\r\n	<li>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis</li>\r\n</ul>\r\n\r\n<p>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', 'CAT2017120002', 'DST2017120003', '2017-12-20', '2017-12-29', 'U', NULL, 'P', NULL, '', 'F', NULL, '', '2017-12-15 05:18:31', 'F', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_content_detail`
--

CREATE TABLE `ms_content_detail` (
  `id_content_detail` varchar(14) NOT NULL,
  `id_content` varchar(14) NOT NULL,
  `id_package` varchar(14) NOT NULL,
  `price` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_content_detail`
--

INSERT INTO `ms_content_detail` (`id_content_detail`, `id_content`, `id_package`, `price`) VALUES
('CND2017120001', 'CNT2017120001', 'PCK2017120001', ''),
('CND2017120002', 'CNT2017120001', 'PCK2017120002', ''),
('CND2017120003', 'CNT2017120001', 'PCK2017120003', '');

-- --------------------------------------------------------

--
-- Table structure for table `ms_content_images`
--

CREATE TABLE `ms_content_images` (
  `id_content_images` varchar(14) NOT NULL,
  `id_content` varchar(14) NOT NULL,
  `images` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_content_images`
--

INSERT INTO `ms_content_images` (`id_content_images`, `id_content`, `images`) VALUES
('CNI2017120001', 'CNT2017120003', 'CNT2017120003-5a376f60d7e16.jpg'),
('CNI2017120002', 'CNT2017120003', 'CNT2017120003-5a376f60da527.jpg'),
('CNI2017120003', 'CNT2017120003', 'CNT2017120003-5a376f60dc84f.jpg'),
('CNI2017120004', 'CNT2017120003', 'CNT2017120003-5a376f60deb78.jpg'),
('CNI2017120005', 'CNT2017120003', 'CNT2017120003-5a376f60e0ea0.jpg'),
('CNI2017120006', 'CNT2017120001', 'CNT2017120001-5a376ffe928cb.jpg'),
('CNI2017120007', 'CNT2017120001', 'CNT2017120001-5a38cc2c59a7b.jpg'),
('CNI2017120008', 'CNT2017120001', 'CNT2017120001-5a38cc2c603f4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ms_country`
--

CREATE TABLE `ms_country` (
  `id_country` varchar(13) NOT NULL COMMENT 'CTR201712001',
  `country` varchar(30) NOT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT 'F => NOT DELETE, T => DELETE',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_country`
--

INSERT INTO `ms_country` (`id_country`, `country`, `is_delete`, `delete_date`) VALUES
('CTR201712001', 'Indonesia', 'F', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_destination`
--

CREATE TABLE `ms_destination` (
  `id_destination` varchar(14) NOT NULL,
  `id_province` varchar(14) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `is_delete` enum('F','T') NOT NULL COMMENT '	F=> NOT DELETE, T => DELETE',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_destination`
--

INSERT INTO `ms_destination` (`id_destination`, `id_province`, `destination`, `is_delete`, `delete_date`) VALUES
('DST2017120001', 'PRV2017120018', 'Gili Trawangan', 'F', NULL),
('DST2017120002', 'PRV2017120011', 'Monumen Nasional', 'F', NULL),
('DST2017120003', 'PRV2017120011', 'Pulau Seribu', 'F', NULL),
('DST2017120004', 'PRV2017120018', 'Mau Test', 'T', '2017-12-18 09:40:00'),
('DST2017120005', 'PRV2017120017', 'Test', 'T', '2017-12-18 09:39:54'),
('DST2017120006', 'PRV2017120018', 'Baru', 'T', '2017-12-18 09:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `ms_location`
--

CREATE TABLE `ms_location` (
  `id_location` varchar(14) NOT NULL COMMENT 'LOC2017120001',
  `location` varchar(100) NOT NULL,
  `id_city` varchar(14) NOT NULL,
  `latitude` decimal(10,0) NOT NULL,
  `longitude` decimal(10,0) NOT NULL,
  `active` enum('T','F') NOT NULL COMMENT '	F=> Inactive, T => Active',
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `changed_by` int(11) NOT NULL,
  `changed_on` timestamp NULL DEFAULT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT '	F=> NOT DELETE, T => DELETE',
  `delete_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_package`
--

CREATE TABLE `ms_package` (
  `id_package` varchar(13) NOT NULL COMMENT 'PKEY: PCK20171200001',
  `package_name` varchar(100) NOT NULL,
  `days` int(11) NOT NULL,
  `nights` int(11) NOT NULL,
  `is_delete` enum('T','F') NOT NULL DEFAULT 'F' COMMENT 'F: NOT DELETE, T: DELETE',
  `delete_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_package`
--

INSERT INTO `ms_package` (`id_package`, `package_name`, `days`, `nights`, `is_delete`, `delete_date`) VALUES
('PCK2017120001', 'Gold Package', 3, 2, 'F', NULL),
('PCK2017120002', 'Medal Package', 8, 7, 'F', NULL),
('PCK2017120003', 'Silver Package', 2, 1, 'F', NULL),
('PCK2017120004', 'Coba', 3, 2, 'T', '2017-12-18');

-- --------------------------------------------------------

--
-- Table structure for table `ms_promotion`
--

CREATE TABLE `ms_promotion` (
  `id_promotion` varchar(14) NOT NULL,
  `promotion` varchar(100) NOT NULL,
  `periode_start` date NOT NULL,
  `periode_end` date NOT NULL,
  `image_promotion` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT 'F=> NOT DELETE, T => DELETED',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_promotion`
--

INSERT INTO `ms_promotion` (`id_promotion`, `promotion`, `periode_start`, `periode_end`, `image_promotion`, `description`, `is_delete`, `delete_date`) VALUES
('PRM201712001', 'Lorem Ipsum', '2017-12-05', '2017-12-11', 'PRM201712001_20171215.png', '<p><strong><em>Lorem ipsum </em></strong></p>\r\n\r\n<p><em>dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</em></p>\r\n\r\n<p><em>Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.</em></p>\r\n\r\n<p><em>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.</em></p>\r\n\r\n<p><em>Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.</em></p>\r\n\r\n<p><em>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.</em></p>\r\n\r\n<p><em>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.</em></p>\r\n\r\n<p><em>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</em></p>\r\n\r\n<ul>\r\n	<li><em>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.</em></li>\r\n	<li><em>Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</em></li>\r\n	<li><em>Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis.</em></li>\r\n</ul>\r\n', 'F', NULL),
('PRM201712002', 'Fatin', '2017-12-05', '2017-12-12', 'PRM201712002_20171215.jpg', '<p>akhirnya</p>\r\n', 'T', '2017-12-15 08:18:48'),
('PRM201712003', 'dsads', '2017-12-19', '2017-12-27', '', '<p>dadads</p>\r\n', 'T', '2017-12-15 09:06:25'),
('PRM201712004', 'Test Lagi', '2017-12-05', '2017-12-15', 'PRM201712004_20171215.png', 'Mau Test Doang', 'T', '2017-12-15 09:21:16'),
('PRM201712005', 'Test', '2017-12-05', '2017-12-15', 'PRM201712005_20171219.png', 'Mau Test Doang', 'F', NULL),
('PRM201712006', 'Test Lagi', '2017-12-05', '2017-12-15', '', 'Mau Test Doang', 'F', NULL),
('PRM201712007', 'Test3', '2017-12-05', '2017-12-15', 'PRM201712007_20171219.png', 'Mau Test Doang', 'T', '2017-12-19 09:49:59');

-- --------------------------------------------------------

--
-- Table structure for table `ms_promotion_detail`
--

CREATE TABLE `ms_promotion_detail` (
  `id_promotion_detail` varchar(14) NOT NULL,
  `id_promotion` varchar(14) NOT NULL,
  `id_package` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_promotion_detail`
--

INSERT INTO `ms_promotion_detail` (`id_promotion_detail`, `id_promotion`, `id_package`) VALUES
('PRD201712001', 'PRM201712001', 'PCK2017120002'),
('PRD201712002', 'PRM201712001', 'PCK2017120001'),
('PRD201712003', 'PRM201712005', 'PCK2017120003'),
('PRD201712005', 'PRM201712007', 'PCK2017120003'),
('PRD201712006', 'PRM201712006', 'PCK2017120003'),
('PRD201712007', 'PRM201712004', 'PCK2017120003');

-- --------------------------------------------------------

--
-- Table structure for table `ms_province`
--

CREATE TABLE `ms_province` (
  `id_province` varchar(14) NOT NULL COMMENT 'PRV2017120001',
  `id_country` varchar(13) NOT NULL,
  `province` varchar(30) NOT NULL,
  `is_delete` enum('F','T') NOT NULL COMMENT 'F => NOT DELETE, T => DELETE',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_province`
--

INSERT INTO `ms_province` (`id_province`, `id_country`, `province`, `is_delete`, `delete_date`) VALUES
('PRV2017120001', 'CTR201712001', 'Aceh', 'F', '0000-00-00 00:00:00'),
('PRV2017120002', 'CTR201712001', 'Sumatera Utara', 'F', '0000-00-00 00:00:00'),
('PRV2017120003', 'CTR201712001', 'Sumatera Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120004', 'CTR201712001', 'Riau', 'F', '0000-00-00 00:00:00'),
('PRV2017120005', 'CTR201712001', 'Jambi', 'F', '0000-00-00 00:00:00'),
('PRV2017120006', 'CTR201712001', 'Sumatera Selatan', 'F', '0000-00-00 00:00:00'),
('PRV2017120007', 'CTR201712001', 'Bengkulu', 'F', '0000-00-00 00:00:00'),
('PRV2017120008', 'CTR201712001', 'Lampung', 'F', '0000-00-00 00:00:00'),
('PRV2017120009', 'CTR201712001', 'Kepulauan Bangka Belitung', 'F', '0000-00-00 00:00:00'),
('PRV2017120010', 'CTR201712001', 'Kepulauan Riau', 'F', '0000-00-00 00:00:00'),
('PRV2017120011', 'CTR201712001', 'DKI Jakarta', 'F', '0000-00-00 00:00:00'),
('PRV2017120012', 'CTR201712001', 'Jawa Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120013', 'CTR201712001', 'Jawa Tengah', 'F', '0000-00-00 00:00:00'),
('PRV2017120014', 'CTR201712001', 'DI Yogyakarta', 'F', '0000-00-00 00:00:00'),
('PRV2017120015', 'CTR201712001', 'Jawa Timur', 'F', '0000-00-00 00:00:00'),
('PRV2017120016', 'CTR201712001', 'Banten', 'F', '0000-00-00 00:00:00'),
('PRV2017120017', 'CTR201712001', 'Bali', 'F', '0000-00-00 00:00:00'),
('PRV2017120018', 'CTR201712001', 'Nusa Tenggara Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120019', 'CTR201712001', 'Nusa Tenggara Timur', 'F', '0000-00-00 00:00:00'),
('PRV2017120020', 'CTR201712001', 'Kalimantan Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120021', 'CTR201712001', 'Kalimantan Tengah', 'F', '0000-00-00 00:00:00'),
('PRV2017120022', 'CTR201712001', 'Kalimantan Selatan', 'F', '0000-00-00 00:00:00'),
('PRV2017120023', 'CTR201712001', 'Kalimantan Timur', 'F', '0000-00-00 00:00:00'),
('PRV2017120024', 'CTR201712001', 'Kalimantan Utara', 'F', '0000-00-00 00:00:00'),
('PRV2017120025', 'CTR201712001', 'Sulawesi Utara', 'F', '0000-00-00 00:00:00'),
('PRV2017120026', 'CTR201712001', 'Sulawesi Tengah', 'F', '0000-00-00 00:00:00'),
('PRV2017120027', 'CTR201712001', 'Sulawesi Selatan', 'F', '0000-00-00 00:00:00'),
('PRV2017120028', 'CTR201712001', 'Sulawesi Tenggara', 'F', '0000-00-00 00:00:00'),
('PRV2017120029', 'CTR201712001', 'Gorontalo', 'F', '0000-00-00 00:00:00'),
('PRV2017120030', 'CTR201712001', 'Sulawesi Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120031', 'CTR201712001', 'Maluku', 'F', '0000-00-00 00:00:00'),
('PRV2017120032', 'CTR201712001', 'Maluku Utara', 'F', '0000-00-00 00:00:00'),
('PRV2017120033', 'CTR201712001', 'Papua Barat', 'F', '0000-00-00 00:00:00'),
('PRV2017120034', 'CTR201712001', 'Papua', 'F', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_booking`
--

CREATE TABLE `tb_booking` (
  `id_booking` varchar(18) NOT NULL COMMENT 'PKEY: BOK20171231000001',
  `id_content` varchar(14) NOT NULL COMMENT 'FKEY: CNT2017120001',
  `id_user` varchar(15) NOT NULL COMMENT 'FKEY: USR201712000002 || USER AS TRIPPER ',
  `booking_date` date NOT NULL,
  `is_approve_booking` enum('P','A','R') NOT NULL DEFAULT 'P' COMMENT 'P: Pending, A: Approve, R: Reject',
  `approve_booking_date` date DEFAULT NULL,
  `approve_by` varchar(15) NOT NULL COMMENT 'FKEY: USR201712000002 || USER AS AGENT'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_booking`
--

INSERT INTO `tb_booking` (`id_booking`, `id_content`, `id_user`, `booking_date`, `is_approve_booking`, `approve_booking_date`, `approve_by`) VALUES
('BOK20171218000001', 'CNT2017120001', 'USR201712000004', '2017-12-18', 'P', NULL, 'USR201712000003'),
('BOK20171219000002', 'CNT2017120001', 'USR201712000002', '2017-12-19', 'P', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_content_comment`
--

CREATE TABLE `tb_content_comment` (
  `id_content_comment` varchar(14) NOT NULL,
  `id_content` varchar(14) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `comment` text NOT NULL,
  `comment_date` timestamp NULL DEFAULT NULL,
  `approval` enum('P','A','R','') NOT NULL DEFAULT 'P' COMMENT 'P = Pending, R = Reject A = Approve',
  `date_approval` timestamp NULL DEFAULT NULL,
  `approve_by` varchar(15) NOT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_content_comment`
--

INSERT INTO `tb_content_comment` (`id_content_comment`, `id_content`, `id_user`, `comment`, `comment_date`, `approval`, `date_approval`, `approve_by`, `is_delete`, `delete_date`) VALUES
('CMT2017120001', 'CNT2017120003', 'USR201712000004', 'This Trip is fake, admin just make some joke for this.', '2017-12-19 06:50:37', 'P', NULL, '', 'F', NULL),
('CMT2017120002', 'CNT2017120003', 'USR201712000004', 'This Trip is fake, admin just make some joke for this.', '2017-12-19 06:50:37', 'A', '2017-12-19 07:29:44', 'USR201712000005', 'F', NULL),
('CMT2017120003', 'CNT2017120003', 'USR201712000004', 'This Trip is fake, admin just make some joke for this.', '2017-12-19 06:50:37', 'R', '2017-12-19 07:30:04', 'USR201712000005', 'F', NULL),
('CMT2017120004', 'CNT2017120003', 'USR201712000004', 'This Trip is fake, admin just make some joke for this.', '2017-12-19 06:50:37', 'R', '2017-12-19 07:30:47', 'USR201712000005', 'T', '2017-12-19 07:32:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_content_rate`
--

CREATE TABLE `tb_content_rate` (
  `id_content_rate` varchar(14) NOT NULL,
  `id_content` varchar(14) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_faq`
--

CREATE TABLE `tb_faq` (
  `id_faq` varchar(13) NOT NULL COMMENT 'FAQ201712001',
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` enum('U','P') NOT NULL COMMENT 'U => UNPUBLISH, P => PUBLISH',
  `created_on` timestamp NULL DEFAULT NULL,
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT 'F => NOT DELETE, T => DELETED',
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_faq`
--

INSERT INTO `tb_faq` (`id_faq`, `title`, `content`, `file_name`, `status`, `created_on`, `is_delete`, `delete_date`) VALUES
('FAQ201712001', 'Lorem Ipsum', '<h1><strong>Lorem Ipsum Dolor Sit Amet, </strong></h1>\r\n\r\n<p>Consectetuer Adipiscing Elit. Maecenas Porttitor Congue Massa. Fusce Posuere, Magna Sed Pulvinar Ultricies, Purus Lectus Malesuada Libero, Sit Amet Commodo Magna Eros Quis Urna.</p>\r\n\r\n<p>Nunc Viverra Imperdiet Enim. Fusce Est. Vivamus A Tellus.</p>\r\n\r\n<p>Pellentesque Habitant Morbi Tristique Senectus Et Netus Et Malesuada Fames Ac Turpis Egestas. Proin Pharetra Nonummy Pede. Mauris Et Orci.</p>\r\n\r\n<p>Aenean Nec Lorem. In Porttitor. Donec Laoreet Nonummy Augue.</p>\r\n\r\n<p>Suspendisse Dui Purus, Scelerisque At, Vulputate Vitae, Pretium Mattis, Nunc. Mauris Eget Neque At Sem Venenatis Eleifend. Ut Nonummy.</p>\r\n\r\n<p>Fusce Aliquet Pede Non Pede. Suspendisse Dapibus Lorem Pellentesque Magna. Integer Nulla.</p>\r\n\r\n<p>Donec Blandit Feugiat Ligula. Donec Hendrerit, Felis Et Imperdiet Euismod, Purus Ipsum Pretium Metus, In Lacinia Nulla Nisl Eget Sapien. Donec Ut Est In Lectus Consequat Consequat.</p>\r\n\r\n<p>Etiam Eget Dui. Aliquam Erat Volutpat. Sed At Lorem In Nunc Porta Tristique.</p>\r\n\r\n<p>Proin Nec Augue. Quisque Aliquam Tempor Magna. Pellentesque Habitant Morbi Tristique Senectus Et Netus Et Malesuada Fames Ac Turpis Egestas.</p>\r\n\r\n<p>Nunc Ac Magna. Maecenas Odio Dolor, Vulputate Vel, Auctor Ac, Accumsan Id, Felis. Pellentesque Cursus Sagittis Felis.</p>\r\n', 'FAQ201712001_20171215.png', 'P', '2017-12-15 08:40:30', 'F', NULL),
('FAQ201712002', 'Test', '<p>tertbaru</p>\r\n', 'FAQ201712002_20171216.png', 'P', '2017-12-15 10:03:25', 'F', NULL),
('FAQ201712003', 'Aa', '<p>aa</p>\r\n', '', '', '2017-12-18 13:18:33', 'F', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(15) NOT NULL COMMENT 'USR201712000001',
  `code_agent` varchar(16) DEFAULT NULL COMMENT 'AGT201712000001 => if user agent',
  `code_tripper` varchar(16) DEFAULT NULL COMMENT 'TRP201712000001 => if user tripper',
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `password_mobile` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_by` varchar(15) NOT NULL,
  `created_on` date NOT NULL,
  `edited_by` varchar(15) DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` enum('1','0') NOT NULL COMMENT '1 => ACTIVE  0 => NOT ACTIVE',
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `full_name` varchar(225) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `is_approve_agent` enum('P','A','R') DEFAULT NULL COMMENT 'P => PENDING , A => APPROVED, R => REJECTED',
  `date_approve_agent` timestamp NULL DEFAULT NULL COMMENT 'DATE APPROVE AND DATE REJECT',
  `approve_agent_by` varchar(15) DEFAULT NULL COMMENT 'APPROVE AND REJECT AGENT BY',
  `is_delete` enum('F','T') NOT NULL DEFAULT 'F' COMMENT 'F: NOT DELETE, T: DELETE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code_agent`, `code_tripper`, `ip_address`, `username`, `password`, `password_mobile`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_by`, `created_on`, `edited_by`, `last_login`, `active`, `first_name`, `last_name`, `full_name`, `photo`, `is_approve_agent`, `date_approve_agent`, `approve_agent_by`, `is_delete`) VALUES
('USR201712000001', NULL, NULL, '127.0.0.1', 'administrator', '$2y$08$4vFd1om5UNo9bLk1mpIB7eQpa2uM7TFVuTNFVRGz7rlH4ZFgtVeVu', '5f4dcc3b5aa765d61d8327deb882cf99', '', 'admin@admin.com', '', 'mTOpjjOQhspRLjikqo0XCebee0bc6a3f16b2642b', 1511187946, NULL, '0', '2017-08-01', '0', 1513738097, '1', 'Super', 'Admin', 'administrator', 'USR201712000001_1513608512.jpg', 'P', NULL, '', 'F'),
('USR201712000002', 'AGT201712000001', NULL, '::1', 'amarafsanjani', '$2y$08$b.8haF12e57DKx69IbeTAuf6rX6ahw/gs2vTzHeNYaBkHmOkyZjaC', NULL, NULL, 'amarafsanjani@gmail.com', NULL, NULL, NULL, NULL, 'USR201712000001', '2017-12-18', NULL, 1513580397, '1', 'Muhammar', 'Rafsanjani', 'Muhammar Rafsanjani', 'USR201712000002_1513651139.jpg', 'A', '2017-12-18 12:36:11', 'USR201712000001', 'F'),
('USR201712000003', 'AGT201712000002', NULL, '::1', 'fachryuzaki', '$2y$08$4vFd1om5UNo9bLk1mpIB7eQpa2uM7TFVuTNFVRGz7rlH4ZFgtVeVu', NULL, NULL, 'mhasyim07@gmail.com', NULL, NULL, NULL, NULL, 'USR201712000001', '2017-12-18', 'USR201712000003', 1513737521, '1', 'Muhamad', 'Fachry Hasyim', 'Muhamad Fachry Hasyim', 'USR201712000003_1513650631.JPG', 'A', '2017-12-18 08:28:47', 'USR201712000001', 'F'),
('USR201712000004', NULL, 'TRP201712000001', '192.168.1.226', 'daffapratama', '$2y$08$ZDgNsJr3VKTPwm/n2SqfIeZo/PEkt/N0Cym49xTxINodBDCtzDdRi', NULL, NULL, 'daffapratama2011@gmail.com', NULL, NULL, NULL, NULL, 'USR201712000001', '2017-12-18', NULL, 1513591503, '1', 'Daffa', 'Pratama', 'Daffa Pratama', 'USR201712000004_1513591571.jpg', NULL, NULL, NULL, 'F'),
('USR201712000005', NULL, NULL, '192.168.1.51', 'lzulkarnaen', '$2y$08$3Ax5XVRDIzBMuPKEnhlLU.sYulaN/sfc43WsHg8f4WmcqATceJyHG', NULL, NULL, 'lzulkarnaen111@gmail.com', NULL, NULL, NULL, NULL, 'USR201712000001', '2017-12-19', NULL, 1513737332, '1', 'Iskandar', 'Zulkarnaen', 'Iskandar Zulkarnaen', 'USR201712000005_1513666036.png', NULL, NULL, NULL, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` varchar(15) NOT NULL COMMENT 'GR201731000001',
  `user_id` varchar(16) NOT NULL COMMENT 'USR201712000001',
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
('GR201712000001', 'USR201712000001', 1),
('GR201712000002', 'USR201712000002', 2),
('GR201712000003', 'USR201712000003', 2),
('GR201712000004', 'USR201712000004', 4),
('GR201712000005', 'USR201712000005', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_sessions`
--
ALTER TABLE `app_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id_logs`),
  ADD KEY `users_id` (`id_user`);

--
-- Indexes for table `ms_category`
--
ALTER TABLE `ms_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `ms_city`
--
ALTER TABLE `ms_city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `ms_content`
--
ALTER TABLE `ms_content`
  ADD PRIMARY KEY (`id_content`);

--
-- Indexes for table `ms_content_detail`
--
ALTER TABLE `ms_content_detail`
  ADD PRIMARY KEY (`id_content_detail`);

--
-- Indexes for table `ms_content_images`
--
ALTER TABLE `ms_content_images`
  ADD PRIMARY KEY (`id_content_images`);

--
-- Indexes for table `ms_country`
--
ALTER TABLE `ms_country`
  ADD PRIMARY KEY (`id_country`);

--
-- Indexes for table `ms_destination`
--
ALTER TABLE `ms_destination`
  ADD PRIMARY KEY (`id_destination`);

--
-- Indexes for table `ms_location`
--
ALTER TABLE `ms_location`
  ADD PRIMARY KEY (`id_location`);

--
-- Indexes for table `ms_package`
--
ALTER TABLE `ms_package`
  ADD PRIMARY KEY (`id_package`);

--
-- Indexes for table `ms_promotion`
--
ALTER TABLE `ms_promotion`
  ADD PRIMARY KEY (`id_promotion`);

--
-- Indexes for table `ms_promotion_detail`
--
ALTER TABLE `ms_promotion_detail`
  ADD PRIMARY KEY (`id_promotion_detail`);

--
-- Indexes for table `ms_province`
--
ALTER TABLE `ms_province`
  ADD PRIMARY KEY (`id_province`);

--
-- Indexes for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indexes for table `tb_content_comment`
--
ALTER TABLE `tb_content_comment`
  ADD PRIMARY KEY (`id_content_comment`);

--
-- Indexes for table `tb_content_rate`
--
ALTER TABLE `tb_content_rate`
  ADD PRIMARY KEY (`id_content_rate`);

--
-- Indexes for table `tb_faq`
--
ALTER TABLE `tb_faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_agent` (`code_agent`),
  ADD UNIQUE KEY `code_tripper` (`code_tripper`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id_logs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
