/**
 * @name Function id encode decode
 * @param String
 * @return String
**/
function encryptID(id) {
    var data   = window.btoa(id);
    var output = encodeURIComponent(data);
    return output;
}

function decryptID(id) {
    var data   = decodeURIComponent(id);
    var output = window.atob(data);
    return output;
}


/**
 * @name Function date formated
 * @param {type} date
 * @return String
**/
function formattedDateddmmyyyyDash(date) {
    // dateformat : dd-mm-yyyy
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
}

function formattedDateddmmyyyySlash(date) {
    // dateformat : dd/mm/yyyy
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}

function formattedDateyyyymmddDash(date) {
    // dateformat : yyyy-mm-dd
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function formattedDateyyyymmddSlash(date) {
    // dateformat : yyyy/mm/dd
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}

function formattedDateyyyymmddNoDash(date) {
    // dateformat : yyyymmdd
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('');
}

function formattedDateddMMyyyyDash(date) {
    // dateformat : dd-MM-yyyy
    var bln = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Agt','Sep','Oct','Nov','Dec'];
    var d = new Date(date || Date.now()),
        month = bln[d.getMonth()],
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
}

function formattedDateddMMyyyyHisDash(date) {
    // dateformat : dd-MM-yyyy H:i:s
    var bln = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Agt','Sep','Oct','Nov','Dec'];
    var d = new Date(date || Date.now()),
        month = bln[d.getMonth()],
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        min = d.getMinutes(),
        sec = d.getSeconds();

    if (day.length < 2) day = '0' + day;

    var date = [day, month, year].join('-');
    var time = [hour, min, sec].join(':');
    return [date, time].join(' ');
}


/**
 * @name Function number formated
 * @param {string}
 * @return String
 */
function formattedNumber(number, currency, precision) {
    var n   = JSON.parse(locale);
    var num = clearformattedNumber(number);

    /* Set Precision */
    if (num.indexOf(n.decimal) <= 0)
    {
        number = $.number(num, 0, n.decimal, n.separator);
    }
    else
    {
        var n2 = num.split(n.decimal);
        if (n2.length > 1)
        {
            if (n2[1] == '')
            {
                number = number;
            // }
            // else if (n2[1] < 10) {
                // number = $.number(num, 1, n.decimal, n.separator);
            }
            else
            {
                if (n2[1] == 0)
                {
                    number = $.number(num, 0, n.decimal, n.separator);
                }
                else
                {
                    if (precision != undefined)
                    {
                        if (precision == true)
                        {
                            number = $.number(num, 2, n.decimal, n.separator);
                        }
                        else
                        {
                            number = $.number(num, 0, n.decimal, n.separator);
                        }
                    }
                    else
                    {
                        number = $.number(num, 0, n.decimal, n.separator);
                    }
                }
            }
        }
    }

    /* Set Currency */
    if (currency != undefined)
    {
        if (currency == true)
        {
            currency = 'Rp. ';
            number = currency + number;
        }
        else
        {
            number = number;
        }
    }

    return number;
}


/**
 * @name Function clear number formated
 * @param {string}
 * @return String
 */
function clearformattedNumber(number) {
    var n    = JSON.parse(locale);
    number   = number.toString();
    number   = number.split(' ');
    number   = number[number.length-1];

    if (n.separator == '.') {
        number = number.replace(/[.]/g,'');
    } else {
        number = number.replace(/[,]/g,'');
    }

    return number;
}


/**
 * @name Function add error notification element
 */
function elementHasError(element, help) {
    elm = $(element);
    div = $(element).closest('div');
    lab = $(element).closest('label');

    /* Default Notification */
    if (help == null || help == "") {
        help = 'This field is required.';
    }

    /* Add Class Error at Element */
    elm.closest('.form-group').addClass('has-error');

    /* Add Text Error at Element */
    if (div[0].className == 'input-group') {
        div = div;
    } else {
        if (elm[0].type == 'checkbox' || elm[0].type == 'radio') {
            div = lab;
        } else {
            div = elm;
        }
    }

    /* If hidden not adding */
    if (elm[0].type != 'hidden') {
        div.after('<span class="help-block help-block-error">' + help + '</span>');
    }

    jQuery(document).ready(function() {
        $(elm).on('keyup change', function() {
            elementHasntError($(this));
        })
    })
}


/**
 * @name Function remove error notification element
**/
function elementHasntError(element) {
    elm = $(element);
    div = $(element).closest('div');

    /* Remove Class Error at Element */
    elm.closest('.form-group').removeClass('has-error');

    /* Remove Text Error at Element */
    if (div[0].className == 'input-group') {
        div = div.parent();
    } else {
        div = elm.parent();
    }
    div.find('span.help-block').remove();
}


/**
 * @name Function init numbers
**/
var Numbers = function () {

    var handleNumbers = function () {
        /* Number Format on Key Up */
        $('.numbers').on('keyup change', function() {
            var n = $(this).val();
            $(this).val(formattedNumber(n));
        });
    }

    var handleNumbers_precision = function () {
        /* Number Format on Key Up */
        $('.numbers_precision').on('keyup change', function() {
            var n = $(this).val();
            $(this).val(formattedNumber(n, '', true));
        });
    }

    return {
        init: function () {
            handleNumbers();
            handleNumbers_precision();
        }
    };

}();


function ShowBoxConfirmation(message, callback)
{
    bootbox.confirm({
        title       : '<i class="fa fa-warning font-red"></i> Konfirmasi',
        message     : message + ' ?',
        size        : 'small',
        buttons     : {
                        confirm: {
                            label: 'Confirm',
                            className: 'btn blue-chambray'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn default'
                        }
                      },
        callback    : function (result) {
                        callback&&callback(result);
                      }
    });
}


function myAlert(message)
{
    bootbox.alert({
        title   : '<i class="fa fa-warning font-red"></i> Peringatan',
        message : message,
        size    : 'small',
        buttons : {
                    ok :{
                        className: 'btn blue-chambray'
                    }
                }
    });
}


/**
 * @name Function go To Date
 * date format: yyyy-mm-dd
 */
function goToDate(date, action, go)
{
    var goToDate;
    
    if(action == "plus"){

        goToDate       = new Date(date);
        goToDate.setDate(goToDate.getDate() + go);
        
        var date      = new Date(goToDate);
        var year      = date.getFullYear();
        var month     = date.getMonth() + 1 //getMonth is zero based;
        var day       = date.getDate();
        var formatted = year + '-' +  month + '-' + day;

    }else if(action == "min"){

        goToDate       = new Date(date);
        goToDate.setDate(goToDate.getDate() - go);
        
        var date      = new Date(goToDate);
        var year      = date.getFullYear();
        var month     = date.getMonth() + 1 //getMonth is zero based;
        var day       = date.getDate();
        var formatted = year + '-' +  month + '-' + day;

    }else{

        var formatted = date;
    }

    return formatted;
}