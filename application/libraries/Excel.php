<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'third_party/PHPExcel/Classes/PHPExcel.php';

class Excel extends PHPExcel {

    function __construct() {
        parent::__construct();
    }
    
    function object_drawing() {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        return $objDrawing;
    }

}
