<?php

require_once APPPATH . 'libraries/JWT.php';

use \Firebase\JWT\JWT;

class Authorization
{
    public static $auth;

    public function __construct()
    {
        
    }

    public static function validateToken($token)
    {
        $CI =& get_instance();
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        return JWT::decode($token, $key, array($algorithm));
    }

    public static function generateToken($data)
    {
        $CI =& get_instance();
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        return JWT::encode($data, $key);
    }

    public static function tokenIsExist($headers)
    {
        return (array_key_exists('Authorization', $headers)
            && !empty($headers['Authorization']));
    }

    public static function clearToken($token)
    {
        $CI     =& get_instance();  
        $header = $CI->input->request_headers();
        self::expiresTimes("+1 minutes");
    }

    public static function hasLogin()
    {
        $instance   =& get_instance();
        $headers    = $instance->input->request_headers();
        self::$auth = new \stdClass;
        
        if(!empty($headers['Authorization'])){
            // check token if exist
            $isToken = self::tokenIsExist($headers);
    
            if ($isToken === true) {
                // get token id
                $hasToken = self::validateToken($headers['Authorization']);

                $res = [];
                if ($hasToken != null) {

                    self::$auth->user = self::getUserByToken($hasToken);

                    $res['data']['user'] = self::$auth->user;
                    
                    return $res;
                }
            }

            // self::$user = null;
            $res = ['status' => 401, 'msg' => 'Unauthorized'];
            return $instance->set_response($res, 401); exit;
        }else{
            $res = ['status' => 'error', 'message' => 'No Authorization'];
            return $instance->set_response($res); exit;
        }
    }

    protected static function getUserByToken($token)
    {
        return User::findOrFail($token->id);
    }

    public static function expiresTimes($howLong, $timestamp = null)
    {
        $timestamp  = ($timestamp !== null)
                    ? $timestamp
                    : date('Y-m-d H:i:s');
        
        $match      = preg_match("%(days)%", $howLong);

        $bind       = ($match < 1) ? $howLong : "+{$howLong} days";
        
        $expire     = strtotime($bind, strtotime($timestamp));

        $date_diff  = ($expire - strtotime($timestamp)) / 86400;

        return $expire;
    }
}
