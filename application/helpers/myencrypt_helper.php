<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @abstract Function id encode
 * @return string
 * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
 */
function encryptID($id)
{
    $data   = base64_encode($id);
    $output = urlencode($data);
    return $output;
}


/**
 * @abstract Function id decode
 * @return string
 * @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
 */
function decryptID($id)
{
    $data   = urldecode($id);
    $output = base64_decode($data);
    return $output;
}
