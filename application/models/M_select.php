<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @abstract Class Model Select Function
**/

class M_Select extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     *  @abstract Function select Status Publish
     *  @return Drop down
    **/
    public function selectStatusPublish()
    {
        $status_options      = array();
        $status_options['']  = '- Select Status -';
        $status_options['U'] = 'Unpublished';
        $status_options['P'] = 'Published';
        return $status_options;
    }

    /**
     *  @abstract Function select Status User
     *  @return Drop down
    **/
    public function selectStatusUser()
    {
        $status_options      = array();
        $status_options['']  = '- Select Status User -';
        $status_options['1'] = 'Active';
        $status_options['0'] = 'Inactive';
        return $status_options;
    }

    /**
     *  @abstract Function select Group (Level User)
     *  @return Drop down
    **/
    public function selectGroup()
    {
        $group_options     = array();
        $group_options[''] = '- Select User Level -';
        $group             = Group::get();

        if (!empty($group)) {
            foreach ($group as $group) {
                $group_options[$group->id] = $group->name;
            }
        }

        return $group_options;
    }

    /**
     *  @abstract Function select Country
     *  @return Drop down
    **/
    public function selectCountry()
    {
        $country_options     = array();
        $country_options[''] = '- Select Country -';
        $country             = M_Country::where('is_delete','f')->get();

        if (!empty($country)) {
            foreach ($country as $country) {
                $country_options[encryptID($country->id_country)] = $country->country;
            }
        }

        return $country_options;
    }

    /**
     *  @abstract Function select Province
     *  @return Drop down
    **/
    public function selectProvince_()
    {
        $province_options       = array();
        $province_options['']   = '- Select Province -';
        $province               = M_Province::where('is_delete','f')->get();

        if (!empty($province)) {
            foreach ($province as $province) {
                $province_options[encryptID($province->id_province)] = $province->province;
            }
        }

        return $province_options;
    }

    /**
     *  @abstract Function select Province
     *  @return Drop down
    **/
    public function selectProvince($id_country)
    {
        $province_options     = array();
        $province_options[''] = '- Select Province -';
        $province             = M_Province::where('id_country', $id_country)->where('is_delete','f')->get();

        if (!empty($province)) {
            foreach ($province as $province) {
                $province_options[encryptID($province->id_province)] = $province->province;
            }
        }

        return $province_options;
    }


    /**
     *  @abstract Function select City 
     *  @param   id_province
     *  @return Drop down
    **/
    public function selectCity($id_province)
    {
        $city_options     = array();
        $city_options[''] = '- Select City -';
        $cities           = M_City::where('id_province', $id_province)->where('is_delete','f')->get();

        if (!empty($cities)) {
            foreach ($cities as $city) {
                $city_options[encryptID($city->id_city)] = $city->city;
            }
        }

        return $city_options;
    }


    /**
     *  @abstract Function select Package 
     *  @return Drop down
    **/
    public function selectPackage()
    {
        $package_options     = array();
        $package_options[''] = array(''=>'- Select Package -');
        $packages            = M_Package::where('is_delete','f')->get();

        if (!empty($packages)) {
            foreach ($packages as $package) {
                $package_options[$package->package_name] = $package->package_name;
            }
        }

        return $package_options;
    }

    /**
     *  @abstract Function select Category 
     *  @return Drop down
    **/
    public function selectCategory()
    {
        $category_options     = array();
        $category_options[''] = array(''=>'- Select Category -');
        $categories            = M_Category::where('is_delete','f')->get();

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $category_options[encryptID($category->id_category)] = $category->category;
            }
        }

        return $category_options;
    }

     /**
     *  @abstract Function select Destination 
     *  @param   id_province
     *  @return Drop down
    **/
    public function selectDestination($id_province)
    {
        $des_options     = array();
        $des_options[''] = '- Select Destination -';
        $destination           = M_Destination::where('id_province', $id_province)->where('is_delete','f')->get();

        if (!empty($destination)) {
            foreach ($destination as $des) {
                $des_options[encryptID($des->id_destination)] = $des->destination;
            }
        }

        return $des_options;
    }
}
