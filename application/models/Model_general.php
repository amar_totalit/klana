<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @abstract Class Model General
**/

class Model_general extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     *  @abstract Function get max id
     *  @return String row data
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    public function getMaxId($table="", $col_ID="", $condition="")
    {
        /* Select Max */
        $this->db->select_max($col_ID, 'id');

        /* Set Condition */
        if (is_array($condition) && !empty($condition))
        {
            foreach ($condition as $key => $value)
            {
                if (!empty($value))
                {
                    $this->db->where($key,$value);
                } else {
                    $this->db->where($key);
                }
            }
        }

        /* Get Data */
        $q   = $this->db->get($table);
        $key = 0;

        if ($q->num_rows() > 0)
        {
            $res = $q->row();
            $key = $res->id + 1;
        }
        else
        {
            $key = 1;
        }
        return $key;
    }


    /**
     *  @abstract Function code prefix
     *  @return String code prefix
     *  @author Muhammar Rafsanjani <amarafsanjani@gmail.com>
    **/
    function code_prefix($table, $field, $data)
    {
        if (!empty($data) && is_array($data))
        {

            $length   = $data['length'];
            $numbers  = str_repeat('0',$length);
            $prefix   = $data['prefix'];
            $position = $data['position'];

            if ($position == 'right') {
                $prefix     = $data['prefix'] . $numbers;
                $len_prefix = strlen(str_replace($numbers,'',$prefix));
                $fieldset   = 'right('.$field.', '.$length.')';
                $condition  = array('substring('.$field.' from 1 for '.$len_prefix.') =' => str_replace($numbers,'',$prefix));
            } else {
                $prefix     = $numbers . $data['prefix'];
                $len_prefix = strlen(str_replace($numbers, '', $prefix));
                $fieldset   = 'substring('.$field.'from 1 for '.$length.')';
                $condition  = array('right('.$field.', '.$len_prefix.') =' => str_replace($numbers,'',$prefix));
            }

            /* Get Max */
            if (!empty($data['prefix'])) {
                $max = $this->getMaxId($table,$fieldset,$condition);
            } else {
                $max = $this->getMaxId($table,$fieldset);
            }
        }
        else
        {
            /* Default Setting Prefix (001/I/2017) */
            $length     = 3;
            $numbers    = str_repeat('0', $length);
            $prefix     = $numbers . '/' . NumbertoRomawi(date('m')) . '/' . date('Y');
            $len_prefix = strlen(str_replace($numbers, '', $prefix));
            $fieldset   = 'substring('.$field.' from 1 for '.$length.')';
            $condition  = array('right('.$field.', '.$len_prefix.') =' => str_replace($numbers,'',$prefix));

            /* Get Max */
            $max = $this->getMaxId($table,$fieldset,$condition);
        }

        /* Generate New Code */
        $new_code = substr(str_repeat('0',$length) . $max, -$length);
        $new_code = str_replace($numbers,$new_code,$prefix);

        return $new_code;
    }

}