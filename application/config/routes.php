<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller']   = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override']         = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller']       = 'dashboard/dashboard/index';
$route['404_override']             = '';
$route['translate_uri_dashes']     = FALSE;


/* User */
$route['login']                    = 'user/users/login';
$route['logout']                   = 'user/users/logout';
$route['profile']                  = 'user/users/profile';
$route['profile/save']             = 'user/users/profile_save';
$route['profile/data']             = 'user/users/profile_data';
$route['change-password']          = 'user/users/change_password';
$route['change-password/save']     = 'user/users/change_password_save';
$route['forgot-password']          = 'user/users/forgot_password';
$route['reset-password/(:any)']    = 'user/users/reset_password/$1';
$route['profile']                  = 'user/users/profile';
$route['profile-save']             = 'user/users/profile_save';

/* Dashboard */
$route['dashboard']                = 'dashboard/dashboard/index';

/* Log */
$route['log']                      = 'log/activities/index';
$route['log-lists/(:num)']         = 'log/activities/lists/$1';
$route['log-fetch-data']           = 'log/activities/fetch_data';
$route['log-fetch-data-group']     = 'log/activities/fetch_data_group';
$route['log-fetch-data/(:num)']    = 'log/activities/fetch_data/$1';
$route['log-detail/(:num)']        = 'log/activities/detail/$1';

/* API -------------------------------------------------------------------------------------------------------------------- */ 

/* Auth */
$route['api/login']                = 'apis/auth/login';
$route['api/logout']               = 'apis/auth/logout';


/* Category */ 
$route['api/list-category']        = 'apis/master_category/list_category';
$route['api/view-category']        = 'apis/master_category/view_category';
$route['api/search-category']      = 'apis/master_category/search_category';
$route['api/save-category']        = 'apis/master_category/save_category';
$route['api/update-category']      = 'apis/master_category/update_category';
$route['api/delete-category']      = 'apis/master_category/delete_category';

/* Destination */ 
$route['api/list-destination']     = 'apis/master_destination/list_destination';
$route['api/view-destination']     = 'apis/master_destination/view_destination';
$route['api/search-destination']   = 'apis/master_destination/search_destination';
$route['api/save-destination']     = 'apis/master_destination/save_destination';
$route['api/update-destination']   = 'apis/master_destination/update_destination';
$route['api/delete-destination']   = 'apis/master_destination/delete_destination';

/* Package */ 
$route['api/list-package']         = 'apis/master_package/list_package';
$route['api/view-package']         = 'apis/master_package/view_package';
$route['api/search-package']       = 'apis/master_package/search_package';
$route['api/save-package']         = 'apis/master_package/save_package';
$route['api/update-package']       = 'apis/master_package/update_package';
$route['api/delete-package']       = 'apis/master_package/delete_package';

/* Promotion */ 
$route['api/list-promotion']       = 'apis/master_promotion/list_promotion';
$route['api/view-promotion']       = 'apis/master_promotion/view_promotion';
$route['api/search-promotion']     = 'apis/master_promotion/search_promotion';
$route['api/save-promotion']       = 'apis/master_promotion/save_promotion';
$route['api/update-promotion']     = 'apis/master_promotion/update_promotion';
$route['api/delete-promotion']     = 'apis/master_promotion/delete_promotion';

/* Province */
$route['api/list-province']        = 'apis/master_province/list_province';
$route['api/view-province']        = 'apis/master_province/view_province';
$route['api/search-province']      = 'apis/master_province/search_province';
$route['api/save-province']        = 'apis/master_province/save_province';
$route['api/update-province']      = 'apis/master_province/update_province';
$route['api/delete-province']      = 'apis/master_province/delete_province';

