<html>
    <body style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif;">
        <div class="wrapper">
            <!-- title row -->
            <div style="padding: 20px; margin: 10px 25px;">
                <div style="margin-right: -15px; margin-left: -15px; width: 100%">
                    <p>Pelanggan Yth, </p>
                    <br/>
                    <p>Terima kasih telah melakukan pembelian Asuransi Mesin Kendaraan Bapak/Ibu dengan rincian sebagai berikut :</p>
                    <table style='width:100%;margin-top:20px'>
                        <tbody>
                            <tr>
                                <td style='width:80px;'>Nama Penjual</td>
                                <td>:</td>
                                <td><?= $seller_name; ?></td>
                            </tr>
                            <tr>
                                <td style='width:80px;'>REG ID</td>
                                <td>:</td>
                                <td><?= $voucher_code; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div>

            <div style="padding: 20px; margin: 10px 25px;">
                <div style="margin-right: -15px; margin-left: -15px; width: 100%;">
                    <p>
                        Berikut adalah E-Voucher untuk Asuransi Mesin Kendaraan. Pengaktifan E-voucher Asuransi ini berlaku<br/>
                        30 hari setelah email ini diterima dan periode perlindungan Asuransi berlaku selama 20 hari sejak<br/>
                        tanggal pengaktifan voucher
                    </p>
                    <p>
                        Bapak/Ibu dapat teruskan e-voucher ini kepada pembeli kendaraan Bapak/Ibu agar dapat di registrasi
                        melalui link berikut : <a href="<?= base_url(); ?>customer" target="_blank"><?= base_url(); ?>customer</a>.
                    </p>
                    <p>
                        Untuk informasi lebih lanjut silahkan hubungi Adira Care 1500456.
                    </p>
                    <p>
                        Terima Kasih<br/>
                        PT.  Asuransi Adira Dinamika
                    </p>
                </div><!-- /.col -->
            </div>
        </div>
    </body>
</html>