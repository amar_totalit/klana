<html>
    <body style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif; font-size: 12px;">
        <div class="wrapper">
            <!-- title row -->
            <div style="padding: 20px; margin: 10px 25px;">
                <div style="margin-right: -15px; margin-left: -15px; width: 100%">
                    <p><?= $creator; ?> membuatkan anda akun baru. Silahkan <a href="<?= base_url(); ?>wahana/login" style="color:#1a5c00;text-decoration:underline" target="_blank">log in</a> dengan kredensial berikut ini:</p>
                    <table style='width:100%;margin-top:20px'>
                        <tbody>
                            <tr>
                                <td style='width:80px;'>Halaman Login</td>
                                <td>:</td>
                                <td><a href="<?= base_url(); ?>wahana/login" target="_blank"><?= base_url(); ?>wahana/login</a></td>
                            </tr>
                            <tr>
                                <td style='width:80px;'>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $full_name; ?></td>
                            </tr>
                            <tr>
                                <td style='width:80px;'>Email</td>
                                <td>:</td>
                                <td><?= $email; ?></td>
                            </tr>
                            <tr>
                                <td style='width:80px;'>Password</td>
                                <td>:</td>
                                <td><?= $password; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div>
        </div>
    </body>
</html>