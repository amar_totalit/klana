<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();
        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function contentList()
	{
        $data['approve']     = site_url() . $this->site . '/approve';
        $data['publish']     = site_url() . $this->site . '/publish';
        $data['unpublish']   = site_url() . $this->site . '/unpublish';
        $data['reject']      = site_url() . $this->site . '/reject';
        $data['add']         = site_url() . $this->site . '/add';
        $data['edit']        = site_url() . $this->site . '/edit';
        $data['delete']      = site_url() . $this->site . '/delete';
        $data['loadPending'] = site_url() . $this->site . '/loadPending';
        $data['loadApprove'] = site_url() . $this->site . '/loadApprove';
        $data['loadReject']  = site_url() . $this->site . '/loadReject';
        $data['detail']      = site_url() . $this->site . '/detail';

		$this->load_view('content','content','v_content',$data);
	}

	/**
    * Serverside load table:ms_content
    * @param Approval = 'P'
    * @return ajax
    **/
	public function loadPending()
	{
		$database_columns = array('id_content','users.full_name','ms_content.created_on','ms_category.category','travel_title','id_content');

        $from   = "ms_content";
        $where  = "ms_content.is_delete = 'F' AND approval = 'P'";
        $join[] = array('users','users.id = ms_content.id_user','');
        $join[] = array('ms_category','ms_category.id_category = ms_content.id_category','');

        $order_by = 'id_content desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_start LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%'";
            $where .= "OR category LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_end LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_content;
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->created_on;
            $row_value[]   = $row->category;
            $row_value[]   = $row->travel_title;
			$row_value[]   = encryptID($row->id_content);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

    /**
    * Serverside load table:ms_content
    * @param Approval = 'A'
    * @return ajax
    **/
    public function loadApprove()
    {
        $database_columns = array('id_content','users.full_name','ms_category.category','travel_title','id_content','publish_status');

        $from   = "ms_content";
        $where  = "ms_content.is_delete = 'F' AND approval = 'A'";
        $join[] = array('users','users.id = ms_content.id_user','');
        $join[] = array('ms_category','ms_category.id_category = ms_content.id_category','');

        $order_by = 'id_content desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_start LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%'";
            $where .= "OR category LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_end LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = array('id'=>$row->id_content,'publish'=>$row->publish_status);
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->category;
            $row_value[]   = $row->travel_title;
            $row_value[]   = encryptID($row->id_content);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    /**
    * Serverside load table:ms_content
    * @param Approval = 'P'
    * @return ajax
    **/
    public function loadReject()
    {
        $database_columns = array('id_content','users.full_name','ms_category.category','travel_title','id_content');

        $from   = "ms_content";
        $where  = "ms_content.is_delete = 'F' AND approval = 'R'";
        $join[] = array('users','users.id = ms_content.id_user','');
        $join[] = array('ms_category','ms_category.id_category = ms_content.id_category','');

        $order_by = 'id_content desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_start LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%'";
            $where .= "OR category LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_end LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id_content;
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->category;
            $row_value[]   = $row->travel_title;
            $row_value[]   = encryptID($row->id_content);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }
	
    /**
    * Get data Package From detail
    * @return Data
    **/
    function getPackage($id)
    {        
         if ($this->input->is_ajax_request()) {
            $model_detail = M_ContentDetail::
                    where('ms_content_detail.id_content', decryptID($id))
                    ->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_content_detail.id_package')
                    ->get([
                        'ms_content_detail.id_package',
                        'ms_package.days',
                        'ms_package.nights',
                        'ms_package.package_name'
                        ]);
            $this->output->set_content_type('application/json')->set_output(json_encode($model_detail));
        }
    }

    /**
    * Direct to page Edit data
    * @return page
    **/
    function edit($id)
    {        
        $data['destination_options'] = array(''=>'- Select Destination -');
        $data['ajax_get_des']        = site_url() . $this->site . '/ajax_get_des';
        $data['ajax_images']         = site_url() . $this->site . '/getBanner';
        $data['province_option']     = $this->m_select->selectProvince_();
        $data['category_option']     = $this->m_select->selectCategory();
        $data['package_option']      = $this->m_select->selectPackage();
        $data['packages']            = M_ContentDetail:: where('ms_content_detail.id_content', decryptID($id))
                    ->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_content_detail.id_package')
                    ->get([
                        'ms_content_detail.id_package',
                        'ms_package.days',
                        'ms_package.nights',
                        'ms_package.package_name'
                        ]);
        $data['cancel']          = site_url() . $this->site . '/contentList';
        $data['action']          = site_url() . $this->site . '/update';
        $id_content              = decryptID($id);
        
        $content = M_Content::where('id_content',$id_content)->first();
		if(!empty($content)){

            $data['content']  = $content;
            $data['agent']    = User::find($content->id_user);
            $data['province'] = M_Destination::find($content->id_destination);

    		$this->load_view('content','content','v_content_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Ajax get Destination from Province Parameter
    * @return page
    * @param id_country
    **/
    function ajax_get_des() {
        $id_province = decryptID($this->input->get('province'));
        
        $destination = $this->m_select->selectDestination($id_province);
        $result      = form_dropdown('destination', $destination, '', 'class="form-control"');

        echo json_encode($result);
    }

 
    /**
    * Get data Images Banner 
    * @return Data
    **/
    public function getBanner() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->get('id');
            $content = M_Content::find($id);
            $content_detail = M_ContentImages::where('id_content', $id)->get();
            foreach($content_detail as $detail){
                $data[] = array(
                    'id' => $detail->id_content_images,
                    'file_name' => $detail->images,
                    'file_path' => 'assets/upload/content',
                    'status' => 'success' 
                );
            }
        } else {
            $data = array('status' => 'error', 'message' => 'Not Found.');
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Update data to table:ms_destination
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user           = $this->ion_auth->user()->row();
            $id_content     = decryptID($this->input->post('id_content'));
            $category       = decryptID($this->input->post('category'));
            $destination    = decryptID($this->input->post('destination'));
            $firstDate      = $this->input->post('firstDate');
            $secondDate     = $this->input->post('secondDate');
            $description    = $this->input->post('description');
            $agent_name     = $this->input->post('agent_name');
            $travel_title   = $this->input->post('travel_title');
            $price          = $this->input->post('price');
            $s_participants = $this->input->post('status_participants');
            $participants   = $this->input->post('participants');
            $model_update   = M_Content::find($id_content);
            $model_data     = M_Content::where('ms_content.id_content', $id_content)
                    ->leftjoin('ms_content_detail','ms_content_detail.id_content', '=' , 'ms_content.id_content')
                    ->leftjoin('ms_content_images','ms_content_images.id_content', '=' , 'ms_content.id_content')
                    ->leftjoin('users','users.id', '=' , 'ms_content.id_user')
                    ->first();

			if(!empty($model_data)){
				$data_old = array(
                    "Code Content"  => $model_data->id_content,
                    "Content Title" => $model_data->travel_title,
                    "Agent Name"    => $model_data->full_name,
                    "Destination"   => $model_data->destination,
                    "Price"         => $model_data->price,
                    "Trevel Start"  => date('d F Y',strtotime($model_data->trevel_start)),
                    "Trevel End"    => date('d F Y',strtotime($model_data->trevel_end)),
                    "Description"   => $model_data->travel_description
            	);


                $other_images = array();
                for ($i = 1; $i <= 5; $i++) {
                    $image_id = $this->input->post('image_id_' . $i);
                    $image_pro = M_ContentImages::find($image_id);
                    if ($this->input->post('delete_image_' . $i) == "yes" and $image_id != '') {
                        if (!empty($image_pro)) {
                            if (file_exists("./assets/upload/content/" . $image_pro->images)) {
                                unlink("./assets/upload/content/" . $image_pro->images);
                            }
                            $delete_detail = $image_pro->delete();
                        }
                    }
                    if ($this->input->post('image_update_' . $i) == "yes") {
                        if ($this->input->post('image_id_' . $i) != '') {
                    // print_r($image_pro->images);
                    // die();
                            if (!empty($image_pro)) {
                                if (file_exists("./assets/upload/content/" . $image_pro->images)) {
                                    unlink("./assets/upload/content/" . $image_pro->images);
                                }
                            }
                            $delete_detail = $image_pro->delete();
                        }
                        if(!empty($_FILES['image_' . $i])){
                            if ($_FILES['image_' . $i]['error'] != 4) {
                                $image_name_detail = $model_update->id_content . "-" . uniqid();
                                $config['upload_path'] = './assets/upload/content/';
                                $config['allowed_types'] = '*';
                                $config['file_name'] = $image_name_detail;
                                $config['overwrite'] = TRUE;
                                $this->upload->initialize($config);
                                if ($this->upload->do_upload('image_' . $i)) {
                                    $datafile = $this->upload->data();
                                    $image_filename = $datafile['file_name'];
                                    $other_images[] = $image_filename;
                                } else {
                                    $status = array('status' => 'error');
                                }
                            }
                        }
                    }
                }


                /* Initialize Data */
                $model_update->travel_title       = $travel_title;
                $model_update->status_participant = $s_participants;
                if ($s_participants == 'L') {
                    $model_update->participants   = $participants;
                }else{
                    $model_update->participants   = null;
                }
                $model_update->travel_start       = date('Y-m-d', strtotime($firstDate));
                $model_update->travel_end         = date('Y-m-d', strtotime($secondDate));
                $model_update->id_category        = $category;
                $model_update->id_destination     = $destination;
                $model_update->price              = $price;
                $model_update->travel_description = $description;

            	/* Update */
                $update = $model_update->save();

                if($update){
                    /*Update banner Images*/
                    if (!empty($other_images)) {
                        foreach ($other_images as $img) {
                            /* Generate Code Prefix: CNI201712001 */
                            $year           = date('Y');
                            $month          = date('m');
                            $prefix         = array('length' => 4, 'prefix' => 'CNI' . $year . $month , 'position' => 'right');
                            $kode           = $this->model_general->code_prefix('ms_content_images', 'id_content_images', $prefix);
                            $model_image = new M_ContentImages();
                            $model_image->id_content_images = $kode;
                            $model_image->id_content        = $model_update->id_content;
                            $model_image->images            = $img;
                            $save = $model_image->save();
                        }
                    }
                    /*Update banner Images*/

                    /*For Upgrad Program next time*/
                    // $update_package = M_PromotionDetail::where('id_promotion', $model->id_promotion);
                    // $update_package->delete();
                    // //buat event_tag table
                    // $count_package = count($this->input->post('id_package'));

                    // for ($i = 0; $i < $count_package; $i++) {
                    //      /* Generate Code Prefix: PRD201712001 */
                    //     $year_detail   = date('Y');
                    //     $month_detail  = date('m');
                    //     $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
                    //     $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);

                    //     $id[$i]                         = M_Package::where('package_name', $this->input->post('id_package')[$i])->first();
                    //     $model_det                      = new M_PromotionDetail();
                    //     $model_det->id_promotion_detail = $kode_detail;
                    //     $model_det->id_promotion        = $model->id_promotion;
                    //     $model_det->id_package          = $id[$i]->id_package;
                    //     $model_det->save();
                    // }
                    /*For Upgrad Program next time*/

                	$data_new = array(
                        "Code Content"  => $id_content,
                        "Content Title" => $travel_title,
                        "Agent Name"    => $model_data->full_name,
                        "Destination"   => $model_data->destination,
                        "Trevel Start"  => date('d F Y',strtotime($firstDate)),
                        "Trevel End"    => date('d F Y',strtotime($secondDate)),
                        "Description"   => $travel_description
                    );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Content " . $travel_title . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 11);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site.'/contentList');
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site.'/contentList');
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site.'/contentList');
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site.'/contentList');
        }
    }


    /**
    * Delete data from table:ms_promotion => Soft Delete
    * Delete data from table:ms_promotion_detail => Hard Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        $user         = $this->ion_auth->user()->row();
        $id_content = decryptID($id);
        
        $model          = M_Content::where('id_content',$id_content)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
                $model_det = M_ContentDetail::where('id_content',$id_content);
                $model_im  = M_ContentImages::where('id_content',$id_content);
                $model_det->delete();
                $model_im->delete();
            	/* Write Log */
            	 $data_notif = array(
                    "Code Content"  => $model->id_content,
                    "Content Title" => $model->travel_title,
                    "Description"   => $model->travel_description
                );
                $message = "Content " . $model->travel_title . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 11);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site . '/contentList');
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site . '/contentList');
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site  . '/contentList');
		}
    }

     /**
    * Detail data from table:ms_promotion
    * @param Id
    * @return page index
    **/
    public function detail($id) {
        $data['back'] = site_url() . $this->site . '/contentList';

        $id_content = decryptID($id);
        $content    = M_Content::find($id_content);
        if (!empty($content)) {

            $data['content']  = M_Content::where('id_content',$id_content)
                            ->leftjoin('ms_category','ms_category.id_category', '=' , 'ms_content.id_category')
                            ->leftjoin('ms_destination','ms_destination.id_destination', '=' , 'ms_content.id_destination')
                            ->leftjoin('users','users.id', '=' , 'ms_content.id_user')
                            ->first();
            $data['status']   = array('L'=>'Limited','U'=>'Unlimited');
            $data['images']   = M_ContentImages::where('id_content', $id_content)->get();
            $data['packages'] = M_ContentDetail::where('id_content', $id_content)
                            ->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_content_detail.id_package')->get();
            $this->load_view('content','content','v_content_detail',$data);
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Approve Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function approve($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_content = decryptID($id);
        
        $model      = M_Content::where('id_content',$id_content)->first();

        if(!empty($model)){

            $data_old = array(
                    "Code Content"    => $model->id_content,
                    "Content Title"   => $model->travel_title,
                    "Approve Date"    => '-',
                    "Approval Status" => 'Pending'
                );

            /* Initialize Data */
            $model->approval      = 'A';
            $model->date_approval = date('Y-m-d H:i:s');
            $model->approve_by    = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new = array(
                    "Code Content"    => $model->id_content,
                    "Content Title"   => $model->travel_title,
                    "Approve Date"    => date('d F Y H:i:s'),
                    "Approval Status" => 'Approve'
                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content " . $model->travel_title. " is successfuly approved by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 11);

                $this->session->set_flashdata('success', lang("message_approve_success"));
                redirect(site_url() . $this->site . '/contentList');
            }else{
                $this->session->set_flashdata('error', lang("message_approve_failed"));
                redirect(site_url() . $this->site . '/contentList');
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site . '/contentList');
        }
    }

    /**
    * Reject Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function reject($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_content = decryptID($id);
        
        $model      = M_Content::where('id_content',$id_content)->first();

        if(!empty($model)){

            $data_old   = array(
                "Code Content"    => $model->id_content,
                "Content Title"   => $model->travel_title,
                "Approve Date"    => '-',
                "Approval Status" => 'Pending'
            );


            /* Initialize Data */
            $model->approval      = 'R';
            $model->date_approval = date('Y-m-d H:i:s');
            $model->approve_by    = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new   = array(
                    "Code Content"    => $model->id_content,
                    "Content Title"   => $model->travel_title,
                    "Approve Date"    => date('d F Y H:i:s'),
                    "Approval Status" => 'Reject'
                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content " . $model->travel_title. " is successfuly rejected by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 11);

                $this->session->set_flashdata('success', lang("message_reject_success"));
                redirect(site_url() . $this->site . '/contentList');
            }else{
                $this->session->set_flashdata('error', lang("message_reject_failed"));
                redirect(site_url() . $this->site . '/contentList');
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site . '/contentList');
        }
    }

    /**
    * Publish Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function publish($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_content = decryptID($id);
        
        $model      = M_Content::where('id_content',$id_content)->first();

        if(!empty($model)){

            $data_old   = array(
                "Code Content"   => $model->id_content,
                "Content Title"  => $model->travel_title,
                "Publish Date"   => '-',
                "Publish Status" => 'Unpublished'
            );

            /* Initialize Data */
            $model->publish_status = 'T';
            $model->publish_date   = date('Y-m-d H:i:s');
            $model->publish_by     = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new   = array(
                    "Code Content"   => $model->id_content,
                    "Content Title"  => $model->travel_title,
                    "Publish Date"   => date('d F Y H:i:s'),
                    "Publish Status" => 'Published'
                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content " . $model->travel_title. " is successfuly published by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 11);

                $this->session->set_flashdata('success', 'Content successfuly published');
                redirect(site_url() . $this->site . '/contentList');
            }else{
                $this->session->set_flashdata('error', 'Content failed published');
                redirect(site_url() . $this->site . '/contentList');
            }
        }else{
            $this->session->set_flashdata('error', 'Content not found');
            redirect(site_url() . $this->site . '/contentList');
        }
    }

    /**
    * Pnpublish Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function unpublish($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_content = decryptID($id);
        
        $model      = M_Content::where('id_content',$id_content)->first();

        if(!empty($model)){

            $data_old   = array(
                "Code Content"   => $model->id_content,
                "Content Title"  => $model->travel_title,
                "Publish Date"   => '-',
                "Publish Status" => 'Published'
            );
            /* Initialize Data */
            $model->publish_status = 'F';
            $model->publish_date   = date('Y-m-d H:i:s');
            $model->publish_by     = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new   = array(
                    "Code Content"   => $model->id_content,
                    "Content Title"  => $model->travel_title,
                    "Publish Date"   => date('d F Y H:i:s'),
                    "Publish Status" => 'Unpublished'
                );

                
                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content " . $model->travel_title. " is successfuly unpublished by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 11);

                $this->session->set_flashdata('success', 'Content successfuly unpublished');
                redirect(site_url() . $this->site . '/contentList');
            }else{
                $this->session->set_flashdata('error', 'Content failed unpublished');
                redirect(site_url() . $this->site . '/contentList');
            }
        }else{
            $this->session->set_flashdata('error', 'Content not found');
            redirect(site_url() . $this->site . '/contentList');
        }
    }

    // /**
    // * Save data to table:ms_promotion & ms_promotion_detail
    // * @param Post Data
    // * @return page index
    // **/
    // function save()
    // {
    //     /* Get Data Post */
    //     if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //         $user        = $this->ion_auth->user()->row();
    //         $promotion   = $this->input->post('promotion_name');
    //         $firstDate   = $this->input->post('firstDate');
    //         $secondDate  = $this->input->post('secondDate');
    //         $description = $this->input->post('description');
            
    //         /* Generate Code Prefix: PRM201712001 */
    //         $year           = date('Y');
    //         $month          = date('m');
    //         $prefix         = array('length' => 3, 'prefix' => 'PRM' . $year . $month , 'position' => 'right');
    //         $kode           = $this->model_general->code_prefix('ms_promotion', 'id_promotion', $prefix);

    //         $data_promotion = M_Promotion::where('promotion', $promotion)->first();

    //         if(empty($data_promotion)){
    //             /* Initialize Data */
    //             $model = new M_Promotion();

    //             $image_name = '';

    //             /** Upload Image * */
    //             if ($this->input->post('image_exist') != "") {
    //                 if ($_FILES['photofilename']['error'] != 4) {

    //                     /* Config Upload */
    //                     $config['upload_path']   = './assets/upload/promotions';
    //                     $config['allowed_types'] = '*';
    //                     $config['file_name']     = $kode . "_" . date('Y') . date('m') . date('d');
    //                     $config['overwrite']     = TRUE;
                        
    //                     $this->upload->initialize($config);
                        
    //                     if ($this->upload->do_upload('photofilename')) {
    //                         $datafile   = $this->upload->data();
    //                         $image_name = $datafile['file_name'];
    //                     } else {
    //                         $status = array('status' => 'error');
    //                     }
    //                 }
    //             } else {
    //                 $status = array('status' => 'image-blank');
    //             }
    //             /** END Upload Image **/   


    //             $model->id_promotion    = $kode;
    //             $model->promotion       = $promotion;
    //             $model->periode_start   = date('Y-m-d', strtotime($firstDate));
    //             $model->periode_end     = date('Y-m-d', strtotime($secondDate));
    //             $model->image_promotion = $image_name;
    //             $model->description     = $description;
    //             $model->is_delete       = 'f';
                    
    //             /* Save */
    //             $save = $model->save();
    //             if($save){

                    
    //                 //Detail Promotion
    //                 $count_package = count($this->input->post('id_package'));

    //                 for ($i = 0; $i < $count_package; $i++) {

    //                     /* Generate Code Prefix: PRD201712001 */
    //                     $year_detail   = date('Y');
    //                     $month_detail  = date('m');
    //                     $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
    //                     $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);


    //                     $id[$i]                         = M_Package::where('package_name', $this->input->post('id_package')[$i])->first();
    //                     $model_det                      = new M_PromotionDetail();
    //                     $model_det->id_promotion_detail = $kode_detail;
    //                     $model_det->id_promotion        = $kode;
    //                     $model_det->id_package          = $id[$i]->id_package;
    //                     $model_det->save();
    //                 }

    //                 /* Write Log */
    //                 $data_notif = array(
    //                     "Code Promotion" => $kode,
    //                     "Promotion"      => $promotion,
    //                     "Periode Start"  => $firstDate,
    //                     "Periode End"    => $secondDate,
    //                     "Description"    => $description
    //                 );

    //                 $message = $user->first_name . " " . $user->last_name . " add data  " . $promotion;
    //                 $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 8);

    //                 $this->session->set_flashdata('success', lang("message_save_success"));
    //                 redirect(site_url() . $this->site);
    //             }else{
    //                 $this->session->set_flashdata('error', lang("message_save_failed"));
    //                 redirect(site_url() . $this->site);
    //             }
    //         }else{
    //             $this->session->set_flashdata('error', lang("message_data_exist"));
    //             redirect(site_url() . $this->site);
    //         }
    //     } else {
    //         $this->session->set_flashdata('error', lang("message_save_failed"));
    //         redirect(site_url() . $this->site);
    //     }
    // }
}
