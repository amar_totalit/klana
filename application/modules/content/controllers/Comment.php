<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();
        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['approve']     = site_url() . $this->site . '/approve';
        $data['publish']     = site_url() . $this->site . '/publish';
        $data['unpublish']   = site_url() . $this->site . '/unpublish';
        $data['reject']      = site_url() . $this->site . '/reject';
        $data['add']         = site_url() . $this->site . '/add';
        $data['edit']        = site_url() . $this->site . '/edit';
        $data['delete']      = site_url() . $this->site . '/delete';
        $data['loadPending'] = site_url() . $this->site . '/loadPending';
        $data['loadApprove'] = site_url() . $this->site . '/loadApprove';
        $data['loadReject']  = site_url() . $this->site . '/loadReject';
        $data['detail']      = site_url() . $this->site . '/detail';

		$this->load_view('content','comment','v_comment',$data);
	}

	/**
    * Serverside load table:ms_content
    * @param Approval = 'P'
    * @return ajax
    **/
	public function loadPending()
	{
		$database_columns = array('id_content_comment','ms_content.travel_title','users.full_name','comment','comment_date','id_content_comment');

        $from   = "tb_content_comment";
        $where  = "tb_content_comment.is_delete = 'F' AND tb_content_comment.approval = 'P'";
        $join[] = array('users','users.id = tb_content_comment.id_user','');
        $join[] = array('ms_content','ms_content.id_content = tb_content_comment.id_content','');

        $order_by = 'id_content_comment desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content_comment LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR comment_date LIKE '%" . $sSearch . "%'";
            $where .= "OR comment LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content_comment');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_content_comment;
            $row_value[]   = $row->travel_title;
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->comment_date;
            $row_value[]   = $row->comment;
			$row_value[]   = encryptID($row->id_content_comment);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

    /**
    * Serverside load table:ms_content
    * @param Approval = 'A'
    * @return ajax
    **/
    public function loadApprove()
    {
        $database_columns = array('id_content_comment','ms_content.travel_title','users.full_name','comment','comment_date','id_content_comment');

        $from   = "tb_content_comment";
        $where  = "tb_content_comment.is_delete = 'F' AND tb_content_comment.approval = 'A'";
        $join[] = array('users','users.id = tb_content_comment.id_user','');
        $join[] = array('ms_content','ms_content.id_content = tb_content_comment.id_content','');

        $order_by = 'id_content_comment desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content_comment LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR comment_date LIKE '%" . $sSearch . "%'";
            $where .= "OR comment LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content_comment');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id_content_comment;
            $row_value[]   = $row->travel_title;
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->comment_date;
            $row_value[]   = $row->comment;
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    /**
    * Serverside load table:ms_content
    * @param Approval = 'P'
    * @return ajax
    **/
    public function loadReject()
    {
        $database_columns = array('id_content_comment','ms_content.travel_title','users.full_name','comment','comment_date','id_content_comment');

        $from   = "tb_content_comment";
        $where  = "tb_content_comment.is_delete = 'F' AND tb_content_comment.approval = 'R'";
        $join[] = array('users','users.id = tb_content_comment.id_user','');
        $join[] = array('ms_content','ms_content.id_content = tb_content_comment.id_content','');

        $order_by = 'id_content_comment desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_content_comment LIKE '%" . $sSearch . "%'";
            $where .= "OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= "OR comment_date LIKE '%" . $sSearch . "%'";
            $where .= "OR comment LIKE '%" . $sSearch . "%'";
            $where .= "OR full_name LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_content_comment');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('join', $join);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id_content_comment;
            $row_value[]   = $row->travel_title;
            $row_value[]   = $row->full_name;
            $row_value[]   = $row->comment_date;
            $row_value[]   = $row->comment;
            $row_value[]   = encryptID($row->id_content_comment);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }
	

    


    /**
    * Delete data from table:ms_promotion => Soft Delete
    * Delete data from table:ms_promotion_detail => Hard Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        $user               = $this->ion_auth->user()->row();
        $id_content_comment = decryptID($id);
        
        $model              = M_ContentComment::where('id_content_comment',$id_content_comment)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
            	/* Write Log */
            	 $data_notif = array(
                    "Code Content Comment" => $model->id_content_comment,
                    "Content Title"        => $model->travel_title,
                    "Comment"              => $model->comment
                );
                $message = "Content Comment" . $model->id_content_comment . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 12);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site );
		}
    }

    /**
    * Approve Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function approve($id)
    {
        $user               = $this->ion_auth->user()->row();
        $id_content_comment = decryptID($id);
        
        $model              = M_ContentComment::where('id_content_comment',$id_content_comment)->first();

        if(!empty($model)){

            $data_old = array(
                    "Code Content Comment" => $model->id_content_comment,
                    "Content Title"        => $model->travel_title,
                    "Approve Date"         => '-',
                    "Comment"              => $model->comment,
                    "Approval Status"      => 'Pending'
                );

            /* Initialize Data */
            $model->approval      = 'A';
            $model->date_approval = date('Y-m-d H:i:s');
            $model->approve_by    = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new = array(
                    "Code Content Comment" => $model->id_content_comment,
                    "Content Title"        => $model->travel_title,
                    "Approve Date"         => date('d F Y H:i:s'),
                    "Approval Status"      => 'Approve'
                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content Comment" . $model->id_content_comment. " is successfuly approved by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 12);

                $this->session->set_flashdata('success', lang("message_approve_success"));
                redirect(site_url() . $this->site );
            }else{
                $this->session->set_flashdata('error', lang("message_approve_failed"));
                redirect(site_url() . $this->site );
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site );
        }
    }

    /**
    * Reject Content data from table:ms_content
    * @param Id
    * @return page index
    **/
    function reject($id)
    {
        $user               = $this->ion_auth->user()->row();
        $id_content_comment = decryptID($id);
        $model              = M_ContentComment::where('id_content_comment',$id_content_comment)->first();

        if(!empty($model)){

            $data_old   = array(
                "Code Content Comment" => $model->id_content_comment,
                "Content Title"        => $model->travel_title,
                "Approve Date"         => '-',
                "Comment"              => $model->comment,
                "Approval Status"      => 'Pending'
            );


            /* Initialize Data */
            $model->approval      = 'R';
            $model->date_approval = date('Y-m-d H:i:s');
            $model->approve_by    = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_new   = array(
                    "Code Content Comment" => $model->id_content_comment,
                    "Content Title"        => $model->travel_title,
                    "Approve Date"         => date('d F Y H:i:s'),
                    "Comment"              => $model->comment,
                    "Approval Status"      => 'Reject'
                );

                $data_change = array_diff_assoc($data_new, $data_old);
                $message = "Content Comment" . $model->id_content_comment. " is successfuly rejected by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 12);

                $this->session->set_flashdata('success', lang("message_reject_success"));
                redirect(site_url() . $this->site);
            }else{
                $this->session->set_flashdata('error', lang("message_reject_failed"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }

    
}
