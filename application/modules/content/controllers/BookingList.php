<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BookingList extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['approve']          = site_url() . $this->site . '/approve';
        $data['reject']           = site_url() . $this->site . '/reject';
        $data['bookingDetail']    = site_url() . $this->site . '/bookingDetail';
        $data['getTripper']       = site_url() . $this->site . '/getTripper';
        $data['loadTable']        = site_url() . $this->site . '/loadTable';
        
		$this->load_view('content','bookinglist','v_bookinglist',$data);
	}

	/**
    * Serverside load table:users
    * Get agent status pending
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('ms_content.id_content',
                                  'ms_content.id_content',
                                  'travel_title',
                                  'ms_destination.destination',
                                  'ms_package.package_name',
                                  'travel_start',
                                  'travel_end',
                                  'users.first_name',
                                  'users.last_name'
                                  );

        $from   = "ms_content";
        $where  = "users.is_delete = 'F'";
        $join[] = array('ms_content_detail', 'ms_content_detail.id_content = ms_content.id_content', '');
        $join[] = array('ms_destination', 'ms_destination.id_destination = ms_content.id_destination', '');
        $join[] = array('users', 'users.id = ms_content.id_user', '');
        $join[] = array('ms_package', 'ms_package.id_package = ms_content_detail.id_package', '');

        $order_by = 'id_content desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (ms_content.id_content LIKE '%" . $sSearch . "%'";
            $where .= " OR travel_title LIKE '%" . $sSearch . "%'";
            $where .= " OR destination LIKE '%" . $sSearch . "%'";
            $where .= " OR package_name LIKE '%" . $sSearch . "%'";
            $where .= " OR DATE_FORMAT(travel_start, '%d %M %Y') LIKE '%" . $sSearch . "%')";
            $where .= " OR DATE_FORMAT(travel_end, '%d %M %Y') LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('ms_content.id_content');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('join', $join);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
            $row_value[]   = encryptID($row->id_content);
            $row_value[]   = $row->id_content;
			$row_value[]   = $row->first_name . ' ' .$row->last_name;
            $row_value[]   = $row->travel_title;
            $row_value[]   = $row->destination;
            $row_value[]   = $row->package_name;
            $row_value[]   = date('d F Y',strtotime($row->travel_start));
            $row_value[]   = date('d F Y',strtotime($row->travel_end));
            $row_value[]   = encryptID($row->id_content);
            //$row_value[]   = encryptID($row->id_booking);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

    /**
    * Approve Agent data from table:users
    * @param Id
    * @return page index
    **/
    function approve($id)
    {
		$user       = $this->ion_auth->user()->row();
		$id_user    = decryptID($id);
		
		$model      = User::where('id',$id_user)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_approve_agent   = 'A';
            $model->date_approve_agent = date('Y-m-d H:i:s');
            $model->approve_agent_by   = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
            	$data_notif = array(
                    "Code Agent"   => $model->code_agent,
                    "Agent Name"   => $model->first_name . ' ' . $model->last_name,
                    "Approve Date" => date('d F Y H:i:s'),
                    "Approve by"   => $user->first_name . ' ' . $user->last_name,
                );

                $message = "Agent " . $model->first_name . ' ' . $model->last_name . " is successfuly approved by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id,json_encode($data_notif), NULL, NULL, $message, 'C', 10);

            	$this->session->set_flashdata('success', lang("message_approve_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_approve_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }

    /**
    * Reject Agent data from table:users
    * @param Id
    * @return page index
    **/
    function reject($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_user    = decryptID($id);
        
        $model      = User::where('id',$id_user)->first();

        if(!empty($model)){

            /* Initialize Data */
            $model->is_approve_agent   = 'R';
            $model->date_approve_agent = date('Y-m-d H:i:s');
            $model->approve_agent_by   = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_notif   = array(
                    "Code Agent"  => $model->code_agent,
                    "Agent Name"  => $model->first_name . ' ' . $model->last_name,
                    "Reject Date" => date('d F Y H:i:s'),
                    "Reject by"   => $user->first_name . ' ' . $user->last_name,
                );

                $message = "Agent " . $model->first_name . ' ' . $model->last_name . " is successfuly rejected by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id,json_encode($data_notif), NULL, NULL, $message, 'C', 10);

                $this->session->set_flashdata('success', lang("message_reject_success"));
                redirect(site_url() . $this->site);
            }else{
                $this->session->set_flashdata('error', lang("message_reject_failed"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Get data agent table:users
    * @return ajax
    **/
    function bookingDetail(){
        $id_content   = decryptID($this->input->get('id_content'));
    
        $data_booking = M_Content::selectRaw("ms_content.id_content,
                                              ms_content.travel_title,
                                              ms_content.travel_description,
                                              ms_content.travel_start,
                                              ms_content.travel_end,
                                              ms_content.participants,
                                              ms_category.category,
                                              ms_destination.destination,
                                              ms_package.package_name,
                                              ( SELECT CONCAT(users.first_name,' ',users.last_name)
                                                FROM 
                                                    users
                                                WHERE users.id = ms_content.id_user
                                              ) AS agent_name
                                            ")
                            ->join('ms_content_detail','ms_content_detail.id_content','=','ms_content.id_content')
                            ->join('ms_category','ms_category.id_category','=','ms_content.id_category')
                            ->join('ms_destination','ms_destination.id_destination','=','ms_content.id_destination')
                            // ->join('ms_content_images','ms_content_images.id_content','=','ms_content.id_content')
                            ->join('ms_package','ms_package.id_package','=','ms_content_detail.id_package')
                            ->where('ms_content.id_content',$id_content)
                            ->first();

        echo json_encode($data_booking); 
    }

    function getTripper() {
        $id_content = decryptID($this->input->get('id_content'));

        $data_tripper = User::selectRaw("tb_booking.id_booking,
                                         tb_booking.booking_date,
                                         (SELECT CONCAT(users.first_name,' ',users.last_name)
                                          FROM
                                            users
                                          WHERE users.id = tb_booking.id_user
                                          ) AS tripper_name,
                                          ( SELECT package_name 
                                            FROM 
                                                ms_package
                                            JOIN ms_content_detail ON ms_content_detail.id_package = ms_package.id_package
                                            JOIN tb_booking ON tb_booking.id_content = ms_content_detail.id_content
                                            WHERE
                                                ms_content_detail.id_package = ms_package.id_package
                                           ) AS package_name
                                        ")
                        ->join('tb_booking', 'tb_booking.id_user','=','users.id')
                        // ->join('ms_content', 'ms_content.id_content','=','tb_booking.id_content')
                        //->join('ms_content_detail', 'ms_content_detail.id_content','=','tb_booking.id_content')
                        //->join('ms_package', 'ms_package.id_package','=','ms_content_detail.id_package')
                        ->where('tb_booking.id_content', $id_content)
                        ->get();

        echo json_encode($data_tripper);
    }
}