@extends('default.views.layouts.default')

@section('title') KLANA - Content @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content List</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Content List </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        @if($success != "")
                            <div class="alert alert-dismissable alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-check"></i> {{$success}}
                            </div>
                        @endif

                        @if($error != "")
                            <div class="alert alert-dismissable alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-times"></i> {{$error}}
                            </div>
                        @endif

                        <div class="tabbable tabbable-tabdrop">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#all" data-toggle="tab">Pending</a>
                                </li>
                                <li>
                                    <a href="#ap" data-toggle="tab">Approve</a>
                                </li>
                                <li>
                                    <a href="#re" data-toggle="tab">Reject</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active " id="all">
                                    @include('content.views.comment.tab.Tpending')
                                </div>
                                <div class="tab-pane" id="ap">
                                    @include('content.views.comment.tab.Tapprove')
                                </div>
                                <div class="tab-pane " id="re">
                                    @include('content.views.comment.tab.Treject')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var url_approve   = '{{$approve}}';
    var url_publish   = '{{$publish}}';
    var url_unpublish = '{{$unpublish}}';
    var url_reject    = '{{$reject}}';
    var loadPending   = '{{$loadPending}}';
    var loadReject    = '{{$loadReject}}';
    var loadApprove   = '{{$loadApprove}}';
    var url_delete    = '{{$delete}}';

    // Datatable Pending
    var oTable = $('#table-pending').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadPending,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },

            {
                "render": function(data, type, row) {
                    return row[4];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_approve = '<button onClick="approveData(\'' + row[5] + '\',$(this),\''+ row[0] +'\')" class="btn green-meadow btn-icon-only btn-circle" title="Approve"><i class="fa fa-check"></i></button>';
                    var btn_reject = '<button onClick="rejectData(\'' + row[5] + '\',$(this),\''+ row[0] +'\')" class="btn btn-danger btn-icon-only btn-circle" title="Reject"><i class="fa fa-close"></i></button>';
                    var render_html = btn_approve+ ' ' + btn_reject;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Datatable Approve
    var oTable = $('#table-approve').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadApprove,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
          
            {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },

            {
                "render": function(data, type, row) {
                    return row[4];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
           
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Datatable Reject
    var oTable = $('#table-reject').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadReject,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },


            {
                "render": function(data, type, row) {
                    return row[4];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_delete = '<button onClick="deleteData(\'' + row[5] + '\',$(this))" class="btn btn-danger btn-icon-only btn-circle" title="Delete"><i class="fa fa-trash"></i></button>';
                    var render_html = btn_delete;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Delete Data
    function deleteData(value, el) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Are You Sure ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-reject'
                });

                window.location.href = url_delete + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
    // Approve Data
    function approveData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Approve <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-pending'
                });

                window.location.href = url_approve + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Reject Data
    function rejectData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Reject <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-pending'
                });

                window.location.href = url_reject + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Publish Data
    function publishData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Publish <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-approve'
                });

                window.location.href = url_publish + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Publish Data
    function unpublishData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Unpublish <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-approve'
                });

                window.location.href = url_unpublish + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
</script>
@stop