@extends('default.views.layouts.default')

@section('title') KLANA - Content @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content List</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Content List </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        @if($success != "")
                            <div class="alert alert-dismissable alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-check"></i> {{$success}}
                            </div>
                        @endif

                        @if($error != "")
                            <div class="alert alert-dismissable alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-times"></i> {{$error}}
                            </div>
                        @endif

                        <div class="tabbable tabbable-tabdrop">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#all" data-toggle="tab">Pending</a>
                                </li>
                                <li>
                                    <a href="#ap" data-toggle="tab">Approve</a>
                                </li>
                                <li>
                                    <a href="#re" data-toggle="tab">Reject</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active " id="all">
                                    @include('content.views.content.tab.Tpending')
                                </div>
                                <div class="tab-pane" id="ap">
                                    @include('content.views.content.tab.Tapprove')
                                     Note :
                                    <br>
                                    <span class="fa fa-check" style="color: green"></span> : Content Published
                                    <br>
                                    <span class="fa fa-times" style="color: red"></span> : Content Does't Published
                                </div>
                                <div class="tab-pane " id="re">
                                    @include('content.views.content.tab.Treject')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var url_approve   = '{{$approve}}';
    var url_publish   = '{{$publish}}';
    var url_unpublish = '{{$unpublish}}';
    var url_reject    = '{{$reject}}';
    var loadPending   = '{{$loadPending}}';
    var loadReject    = '{{$loadReject}}';
    var loadApprove   = '{{$loadApprove}}';
    var url_delete    = '{{$delete}}';

    // Datatable Pending
    var oTable = $('#table-pending').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadPending,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },

            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[4];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_approve = '<button onClick="approveData(\'' + row[5] + '\',$(this),\''+ row[4] +'\')" class="btn green-meadow btn-icon-only btn-circle" title="Approve"><i class="fa fa-check"></i></button>';
                    var btn_reject = '<button onClick="rejectData(\'' + row[5] + '\',$(this),\''+ row[4] +'\')" class="btn btn-danger btn-icon-only btn-circle" title="Reject"><i class="fa fa-close"></i></button>';
                    var btn_detail = '<a href="{{$detail}}/'+ row[5] +'" type="button" class="btn btn-primary btn-icon-only btn-circle" title="Detail"><i class="fa fa-eye"></i></a>';
                    var btn_edit = '<a href="{{$edit}}/'+ row[5] +'" type="button" class="btn btn-warning btn-icon-only btn-circle" title="Edit"><i class="fa fa-edit"></i></a>';
                    var render_html =  btn_detail+' '+btn_edit + ' ' + btn_approve+ ' ' + btn_reject;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Datatable Approve
    var oTable = $('#table-approve').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadApprove,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    if (row[0]['publish'] == 'T') {
                        return '<span class="fa fa-check" style="color:green"></span>';
                    }else{
                        return '<span class="fa fa-times" style="color:red"></span>';
                    }
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0]['id'];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0]['id'];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },

            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_detail = '<a href="{{$detail}}/'+ row[4] +'" type="button" class="btn btn-primary btn-icon-only btn-circle" title="Detail"><i class="fa fa-eye"></i></a>';
                    var btn_publish = '<a href="javascript:;" onClick="publishData(\'' + row[4] + '\',$(this),\''+ row[3] +'\')"  type="button" class="btn btn-success btn-icon-only btn-circle" title="Publish"><i class="fa fa-newspaper-o"></i></a>';
                    var btn_unpublish = '<a href="javascript:;" onClick="unpublishData(\'' + row[4] + '\',$(this),\''+ row[3] +'\')" type="button" class="btn btn-danger btn-icon-only btn-circle" title="Unpublish"><i class="fa fa-undo"></i></a>';
                    var btn_edit = '<a href="{{$edit}}/'+ row[4] +'" type="button" class="btn btn-warning btn-icon-only btn-circle" title="Edit"><i class="fa fa-edit"></i></a>';
                    if (row[0]['publish'] == 'T') {
                        var render_html =  btn_detail+' '+btn_edit + ' ' + btn_unpublish;
                    }else{
                        var render_html =  btn_detail+' '+btn_edit + ' ' + btn_publish ;
                    }
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(1)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Datatable Reject
    var oTable = $('#table-reject').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadReject,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },

            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_delete = '<button onClick="deleteData(\'' + row[4] + '\',$(this))" class="btn btn-danger btn-icon-only btn-circle" title="Delete"><i class="fa fa-trash"></i></button>';
                    var render_html = btn_delete;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Delete Data
    function deleteData(value, el) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Are You Sure ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-reject'
                });

                window.location.href = url_delete + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
    // Approve Data
    function approveData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Approve <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-pending'
                });

                window.location.href = url_approve + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Reject Data
    function rejectData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Reject <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-pending'
                });

                window.location.href = url_reject + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Publish Data
    function publishData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Publish <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-approve'
                });

                window.location.href = url_publish + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Publish Data
    function unpublishData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Unpublish <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-approve'
                });

                window.location.href = url_unpublish + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
</script>
@stop