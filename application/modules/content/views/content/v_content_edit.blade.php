@extends('default.views.layouts.default')

@section('title') KLANA - Content @stop

@section('body')
<iframe src="#" onload="nana()" style="display: none"></iframe>

<div class="page-content" >
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Content </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form Content</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-promotion', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
                            {{ form_input(array('id' => 'image_exist','name' => 'image_exist','type' => 'hidden')) }}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Agent Name <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_input(array('type' => 'text','name' => 'agent_name','value'=>$agent->full_name,'class' => 'form-control','readonly'=>''))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Travel Title <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_input(array('type' => 'text','name' => 'travel_title','value'=>$content->travel_title,'class' => 'form-control'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status Participants <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        <select name="status_participants" class="form-control" id="status_participants">
                                            <option value="">-Select Status-</option>
                                            <option value="L">Limited</option>
                                            <option value="U">Unlimited</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body hidden" id="max" >
                                <div class="form-group">
                                    <label class="control-label col-md-2">Max Participant <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_input(array('type' => 'text','name' => 'participants','value'=>$content->participants,'class' => 'form-control','id'=>'participants'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Description <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control ckeditor" name="description" id="description">
                                            {{$content->travel_description}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Activity <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control ckeditor" name="activity" id="activity">
                                            {{$content->activity}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Banner Images <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        <div class="alert alert-dismissable alert-danger alert-cover" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                        </div>
                                            @for ($i = 1; $i <=5; $i++)
                                                <?php
                                                    echo form_hidden('image_id_'.$i, '');
                                                    echo form_hidden('image_update_' . $i, 'no');
                                                    echo form_hidden('old_src_image_'.$i, '');
                                                    echo form_hidden('delete_image_'.$i, 'no');
                                                ?>
                                                <input type="file" name="image_{{ $i; }}" id="image_{{ $i; }}" onchange="ValidateImage(this, 'image', {{ $i; }});" style="display: none;">     
                                            <div class="col-xs-4" style="margin-bottom: 5px;">
                                                <a href="javascript:;" class="thumbnail">
                                                    <img src="{{ base_url(); }}assets/img/no_image.png" class="img-responsive img-other" id="image_{{ $i; }}-preview" alt="Gambar Berita {{ $i; }}" style="max-height:93px;">
                                                    <br>
                                                    <div class="overlay-browse">
                                                        <center>
                                                            
                                                        <button type="button" class="btn btn-info" title="Pilih Gambar" onclick="$('#image_{{ $i; }}').click();"><i class="fa fa-upload"></i></button>  
                                                        <button type="button" class="btn btn-danger" title="Hapus Gambar" onclick="clearImage('image', {{ $i; }});"><i class="fa fa-times"></i></button>
                                                        </center>
                                                    </div>
                                                </a>
                                            </div>
                                            @endfor
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Category <span style="color: red">*</span></label>
                                <label class="control-label col-md-1 titikdua">:</label>
                                <div class="col-md-7">
                                    {{ form_dropdown('category', $category_option, '', "class='form-control' data-live-search='true' id='category'"); }}
                                </div>
                            </div>
                             <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Travel Date <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="firstDate" data-target-input="nearest">
                                            <input type="text" name="firstDate" id="firstDateVal" class="form-control datetimepicker-input" data-target="#firstDate" readonly value="{{date('d M Y', strtotime($content->travel_start))}}" />
                                            <span class="input-group-addon" data-target="#firstDate" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-1">Until </label>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="secondDate" data-target-input="nearest">
                                            <input type="text" name="secondDate" class="form-control datetimepicker-input" data-target="#secondDate" readonly value="{{date('d M Y', strtotime($content->travel_end))}}"/>
                                            <span class="input-group-addon" data-target="#secondDate" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Province <span style="color: red">*</span></label>
                                <label class="control-label col-md-1 titikdua">:</label>
                                <div class="col-md-7">
                                    {{ form_dropdown('province', $province_option, '', "class='form-control' data-live-search='true' id='province'"); }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Destination <span style="color: red">*</span></label>
                                <label class="control-label col-md-1 titikdua">:</label>
                                <div class="col-md-7">
                                    {{ form_dropdown('destination', $destination_options, '', "class='form-control' data-live-search='true' id='destination'"); }}
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Package <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_dropdown('package', $package_option, '', "class='form-control' data-live-search='true' id='package'"); }}
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-5">
                                    <button type="button" id='buttonBind' class="btn blue btn-sm" data-bind="click: addTransfer">Add Package to list</button>
                                    <!-- <button type="button" class="btn blue btn-sm" id="cek"><i class="fa fa-plus"></i>cek</button> -->
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <div class="form-group form-md-line-input">
                                            <table class="table table-striped table-bordered table-hover" width="auto" >
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Package</th>
                                                        <th>Package Detail</th>
                                                        <th>Price</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: dataTransfer" id="list-package">
                                                    <tr>
                                                        <td data-bind="text: ($index() + 1)">No</td>
                                                        <td data-bind="text: id_package"></td>
                                                        <td data-bind="text: package_detail"></td>
                                                        <td> <input type="text" class="form-control" name="price_package[]" id="price_package[]"></td>
                                                        <td>
                                                            <input type="hidden" name="id_package[]" data-bind="value: id_package" class="form-control id_package">
                                                            <button type="button" data-bind="click: $parent.removeTransfer" class="btn btn-danger btn-icon-only btn-circle"><i class="fa fa-remove"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                            {{ form_input(array('type' => 'hidden','name' => 'id_content','class' => 'form-control', 'value' => encryptID($content->id_content) ))}}

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
/*show hide participants*/
$('#status_participants').change(function () {
    if ($('#status_participants').val() == 'L') {
        $('#max').removeClass('hidden');
    }else{
        $('#max').addClass('hidden');
    }
});

/*show hide participants*/

/*Set Input Mask for Price*/
$('#price').mask('9999999999');
$('#participants').mask('999');
/* Set Category, Province & Destination Selector*/
$('#province').val('{{encryptID($province->id_province)}}').change();
$('#category').val('{{encryptID($content->id_category)}}').change();
 
/* Ajax Set Destination by Province Parameter When load page*/
 var ajax_get_des = '{{$ajax_get_des}}';
function nana() {
    $('select[name="destination"]').val('').change();
    var province = $('select[name="province"]').val();
    var data     = {province: province};

    $.ajax({
        type: 'GET',
        url: ajax_get_des,
        data: data,
        cache: false,
        success: function(result) {
            var oObj = JSON.parse(result);

            $('select[name="destination"]').html(oObj);
            $('#destination').val('{{encryptID($content->id_destination)}}').change();
        }
    });

    $('#status_participants').val('{{$content->status_participant}}').change();
    if ($('#status_participants').val() == 'L') {
        $('#max').removeClass('hidden');
    }else{
        $('#max').addClass('hidden');
    }
};

/* Ajax Get Destination by Province Parameter*/
    $('select[name="province"]').change(function() {
        $('select[name="destination"]').val('').change();
        var province = $('select[name="province"]').val();
        var data     = {province: province};

        $.ajax({
            type: 'GET',
            url: ajax_get_des,
            data: data,
            cache: false,
            success: function(result) {
                var oObj = JSON.parse(result);

                $('select[name="destination"]').html(oObj);
                $('#destination').val('{{encryptID($content->id_destination)}}').change();
            }
        });
    });

    
    // Pengaturan Form Validation 
    var form_validator = $("#form-promotion").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                promotion: {
                    required: true,
                    maxlenght : 100
                },
            },
        messages: {

        },
    });

    
</script>

<script type="text/javascript">

/* Ajax Set Banner Image*/
 $.getJSON('{{$ajax_images}}', {id: '{{$content->id_content}}'}, function (json, textStatus) {
    if (json[0].status == "success") {
        var i;
        var no = 1;
        for (i in json) {
            $("input[name='image_id_"+ no +"']").val(json[i].id);
            $("input[name='old_src_image_"+ no +"']").val("{{base_url()}}" + json[i].file_path + '/' + json[i].file_name);
            $('#image_' + no + '-preview').attr('src', "{{base_url()}}" + json[i].file_path + '/' + json[i].file_name);
            no++;
        }
    } else if (json.status == "error") {
        toastr.error('Data not found.', 'Notifikasi!');
    }
    App.unblockUI('#form-wrapper');
});   
</script>

<script type="text/javascript">
    ////////////////////////////////Banner Image////////////////////////
    var _validFileExtensions = [".jpg", ".png", ".jpeg"];
    var image_array = [];
    
    function ValidateImage(oInput, type, i) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        readURL(oInput, type, i);
                        break;
                    }
                }
                if (!blnValid) {
                    toastr.error('File Format Not Allowed', 'Notification!');
                    oInput.value = "";
                    return false;
                }
            }
        }
        return true;
    }

    function readURL(input, type, i) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (i != undefined) {
                    $('#image_' + i + '-preview').attr('src', e.target.result);
                    image_array.push(type + "_" + i);
                    $("input[name='image_update_" + i + "']").val("yes");
                    $("input[name='delete_" + type + "_"+ i +"']").val("no");
                } else {
                    $('#image-preview').attr('src', e.target.result);
                }
            };
            
            if(type == 'cover'){
                $("#image_exist").val("yes");
                $("#btn-remove").show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function clearImage(type, i) {
        if (i == undefined) {
            $("#partner_image").val("");
            $("#image_exist").val("");
            $("#btn-remove").hide();
        } else {
            $('#image_' + i + '-preview').attr('src', "{{base_url()}}assets/img/no_image.png");
            image_array = jQuery.grep(image_array, function (value) {
                return value != type + "_" + i;
            });
            if (image_array.length == 0) {
                if(type == 'cover'){
                    $("input[name='image_exist']").val(""); 
                }
            }
            $("#"+type+"_"+i).val("");
            $("input[name='delete_"+type+"_"+i+"']").val("yes");
            $("input[name='image_update_"+i+"']").val("no");
        }
    }
    ////////////////////////////////Banner Image////////////////////////
    
     //Binding KO JS 
     var modelTransfer = function (id_package,package_detail)
        {
            var self = this;
            self.id_package = ko.observable(id_package);
            self.package_detail = ko.observable(package_detail);
        }
        var viewTransferModel = function () {
            var self = this;
            self.dataTransfer = ko.observableArray();

            $.getJSON('{{ base_url() }}content/content/getPackage/{{encryptID($content->id_content)}}' , function (data) {
                $.each(data, function (index, val) {
                    var detail = val.days +' days '+ val.nights +' nights';
                    var row_transfer = new modelTransfer(
                        val.package_name,
                        detail
                    );
                    self.dataTransfer.push(row_transfer);
                    // console.log(self.dataTransfer.push(row_transfer));

                });
            });
            self.addTransfer = function () {

                if (!$('#form-promotion').valid()) {
                    return;
                }
                if ($('#package').val() == '') {
                    toastr.error('Cant select empty value.', 'Notification!');
                    $('#package').val('').change();
                    return;   
                }
                var status_package = true;
                ko.utils.arrayForEach(self.dataTransfer(), function (item) {
                    if ($('#package').val() == item.id_package()) {
                        status_package = false;
                    }
                });
                if (status_package == false) {
                    toastr.error('Package has selected.', 'Notifikasi!');
                    $('#package').val('').change();
                    return;
                }

                var row_transfer = new modelTransfer($('#package').val());
                self.dataTransfer.push(row_transfer);
                $("#package").val('').change();

            }

            self.removeTransfer = function () {
                self.dataTransfer.remove(this);
            }
        }
        ko.applyBindings(new viewTransferModel());
</script>
@stop