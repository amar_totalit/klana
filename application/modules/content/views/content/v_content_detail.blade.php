@extends('default.views.layouts.default')

@section('title') KLANA - Contents @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Content</a>
            	<i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{$back}}">Content List</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Detail Content</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail Content </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    
    <div class="row">
		<div class="col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
                    <div class="caption font-dark pull-right">
                        <a href="{{$back}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>{{lang('button_back')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
            		<div class="row">
            			<div class="col-md-8 col-md-offset-2">
            				<div class="row">
            					<div class="col-sm-12">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <?php $no = 0;$nod = 0; ?>
                                            @foreach($images as $image)
                                                <?php $no++  ?>
                                                <li data-target="#myCarousel" data-slide-to="0" class="{{ ($no == 1) ? 'active' : null }}"></li>
                                            @endforeach
                                        </ol>

                                        <div class="carousel-inner">
                                            @foreach($images as $image)
                                                <?php $nod++  ?>
                                                <div class="item {{ ($nod == 1) ? 'active' : null }}">
                                                    <img src="{{base_url()}}assets/upload/content/{{$image->images}}" alt="Chicago" style="width: 100%;height: 400px;" class="karesel-iner">
                                                </div>
                                            @endforeach
                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
            						<!-- <img src="{{base_url().'assets/upload/promotions/'.$promotion->image_promotion}}" style="width: 100%;height: 100%;"> -->
            					</div>
            				</div>
            			</div>
            		</div>
            		<br>
            		<div class="row">
            			<div class="col-md-10 col-md-offset-1">
            				<div class="row">
            					<div class="col-md-7">
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Content Title</b></div>
	            						<div class="col-sm-1">:</div>
	            						<div class="col-sm-6">{{$content->travel_title}}</div>
	            					</div>
                                    <div class="col-md-12">
                                        <div class="col-sm-5"><b>Category</b></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-6">{{$content->category}}</div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-sm-5"><b>Destination</b></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-6">{{$content->destination}}</div>
                                    </div>
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Periode</b></div>
	            						<div class="col-sm-1">:</div>
	            						<div class="col-sm-6">{{date('d M Y', strtotime($content->travel_start))}} - {{date('d M Y', strtotime($content->travel_end))}}</div>
	            					</div>
                                    <div class="col-md-12">
                                        <div class="col-sm-5"><b>Status Participants</b></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-6">{{$status[$content->status_participant]}}</div>
                                    </div>
                                    @if($content->status_participant == 'L')
                                        <div class="col-md-12">
                                            <div class="col-sm-5"><b>Max Participants</b></div>
                                            <div class="col-sm-1">:</div>
                                            <div class="col-sm-6">{{$content->participants}}</div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="col-sm-5"><b>Price</b></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-6">Rp. {{number_format($content->price, '0',',','.')}}</div>
                                    </div>
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Package</b></div>
	            						<div class="col-sm-1">:</div>
	            						<?php $no = 0 ?>
	            						@foreach($packages as $package)
	            							<div class=" col-sm-6 {{ ($no != 0) ? 'col-md-offset-6' : null }}">- {{$package->package_name}}</div>
	            							<?php $no++ ?>
	            						@endforeach
	            					</div>
            					</div>
            					<div class="col-md-12">
            						<div class="col-sm-12">
            							<div class="col-sm-12">
            								<b>Description</b> :</div>
            							</div>
            						<div class="col-sm-12">
            							<div class="col-sm-12">
            								{{$content->travel_description}}
            							</div>
        							</div>
        						</div>
            				</div>
            		</div>    	
				</div>
				<br>
			</div>
		</div>
	</div>

</div>

@stop

@section('scripts')

@stop