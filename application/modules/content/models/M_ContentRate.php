<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_ContentRate extends Eloquent {

	public $table      = 'tb_content_rate';
	public $primaryKey = 'id_content_rate';
	public $timestamps = false;

}