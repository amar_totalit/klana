<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_ContentDetail extends Eloquent {

	public $table      = 'ms_content_detail';
	public $primaryKey = 'id_content_detail';
	public $timestamps = false;

}