<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Content extends Eloquent {

	public $table      = 'ms_content';
	public $primaryKey = 'id_content';
	public $timestamps = false;

}