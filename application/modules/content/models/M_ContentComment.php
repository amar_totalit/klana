<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_ContentComment extends Eloquent {

	public $table      = 'tb_content_comment';
	public $primaryKey = 'id_content_comment';
	public $timestamps = false;

}