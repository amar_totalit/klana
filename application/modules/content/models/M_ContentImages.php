<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_ContentImages extends Eloquent {

	public $table      = 'ms_content_images';
	public $primaryKey = 'id_content_images';
	public $timestamps = false;

}