<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Booking extends Eloquent {

	public $table      = 'tb_booking';
	public $primaryKey = 'id_booking';
	public $timestamps = false;

}