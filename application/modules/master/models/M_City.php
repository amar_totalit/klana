<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_City extends Eloquent {

	public $table      = 'ms_city';
	public $primaryKey = 'id_city';
	public $timestamps = false;

}