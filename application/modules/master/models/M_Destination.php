<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Destination extends Eloquent {

	public $table      = 'ms_destination';
	public $primaryKey = 'id_destination';
	public $timestamps = false;

}