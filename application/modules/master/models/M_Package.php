<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Package extends Eloquent {

	public $table      = 'ms_package';
	public $primaryKey = 'id_package';
	public $timestamps = false;

}