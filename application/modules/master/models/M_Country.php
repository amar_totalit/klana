<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Country extends Eloquent {

	public $table      = 'ms_country';
	public $primaryKey = 'id_country';
	public $timestamps = false;

}