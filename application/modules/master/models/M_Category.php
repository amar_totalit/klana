<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Category extends Eloquent {

	public $table      = 'ms_category';
	public $primaryKey = 'id_category';
	public $timestamps = false;

}