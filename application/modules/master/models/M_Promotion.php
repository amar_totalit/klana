<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Promotion extends Eloquent {

	public $table      = 'ms_promotion';
	public $primaryKey = 'id_promotion';
	public $timestamps = false;

}