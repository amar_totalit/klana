<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_location extends Eloquent {

	public $table      = 'ms_location';
	public $primaryKey = 'id_location';
	public $timestamps = false;

}