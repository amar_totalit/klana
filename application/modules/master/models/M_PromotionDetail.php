<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_PromotionDetail extends Eloquent {

	public $table      = 'ms_promotion_detail';
	public $primaryKey = 'id_promotion_detail';
	public $timestamps = false;

}