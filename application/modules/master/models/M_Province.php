<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Province extends Eloquent {

	public $table      = 'ms_province';
	public $primaryKey = 'id_province';
	public $timestamps = false;

}