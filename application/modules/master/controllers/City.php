<?php defined('BASEPATH') OR exit('No direct script access allowed');

class City extends MX_Controller {

	public function __construct() {
        parent::__construct();

        /* Dynamic Controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index(){

		$data['add']       = site_url() . $this->site . '/add';
		$data['edit']      = site_url() . $this->site . '/edit';
		$data['delete']    = site_url() . $this->site . '/delete';
		$data['loadTable'] = site_url() . $this->site . '/loadTable';

		$this->load_view('master','city','v_city', $data);
	}

	/**
    * Serverside load table:ms_city
    * @return ajax
    **/
    public function loadTable()
	{
		$database_columns = array('id_city','id_city','city','id_city');

        $from  = "ms_city";
        $where = "ms_city.is_delete = 'F'";

        $order_by = 'id_city desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_city LIKE '%" . $sSearch . "%'";
            $where .= "OR city LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_city');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_city;
			$row_value[]   = $row->city;
			$row_value[]   = encryptID($row->id_city);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
        $data['country_options']   = $this->m_select->selectCountry();
        $data['province_options']  = array('' => '- Select Province -');

        $data['cancel']            = site_url() . $this->site;
        $data['action']            = site_url() . $this->site . '/save';
        $data['ajax_get_province'] = site_url() . $this->site . '/ajax_get_province';

    	$this->load_view('master','city','v_city_add',$data);
    }

    /**
    * Ajax get Province from Country Parameter
    * @return page
    **/
    function ajax_get_province() {
        $id_country  = decryptID($this->input->get('country'));

        $province    = $this->m_select->selectProvince($id_country);
        $result      = form_dropdown('province', $province, '', 'class="form-control"');
        
        echo json_encode($result);
    }

    /**
    * Save data to table:ms_city
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user    	= $this->ion_auth->user()->row();
            $country    = decryptID($this->input->post('country'));
			$province_	= decryptID($this->input->post('province'));
			$city 	 	= ucwords($this->input->post('city'));
			
			/* Generate Code Prefix: CTY2017120001 */
			$year    	= date('Y');
			$month   	= date('m');
			$prefix  	= array('length' => 4, 'prefix' => 'CTY' . $year . $month , 'position' => 'right');
			$kode    	= $this->model_general->code_prefix('ms_city', 'id_city', $prefix);
            
            $data_city = M_City::where('city', $city)->first();
            $country   = M_Country::where('id_country', $country)->first();
            $province  = M_Province::where('id_province', $province_)->first();

            if(empty($data_city)){
            	/* Initialize Data */
            	$model = new M_City();

				$model->id_city    	= $kode;
				$model->id_province	= $province_;
				$model->city       	= $city;
				$model->is_delete  	= 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){

	            	/* Write Log */
            	    $data_notif = array(
						"Code City" => $kode,
                        "Country"   => $country->country,
                        "Province"  => $province->province,
						"City"      => $city
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $city;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 4);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function edit($id)
    {        
        $id_city                    = decryptID($id);
        $id_province                = decryptID($this->input->post('province'));

        $data['country_options']    = $this->m_select->selectCountry();
        $data['province_options']   = $this->m_select->selectProvince_();

        $data['cancel']             = site_url() . $this->site;
        $data['action']             = site_url() . $this->site . '/update';
        $data['ajax_get_province']  = site_url() . $this->site . '/ajax_get_province';
       
        $city                       = M_City::join('ms_province', 'ms_province.id_province', '=', 'ms_city.id_province' )->where('id_city', $id_city)->first();
    
        if(!empty($city)){

            $data['city']               = $city;

    		$this->load_view('master','city','v_city_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:ms_city
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user        = $this->ion_auth->user()->row();

            $id_city     = decryptID($this->input->post('id_city'));
            $id_country  = decryptID($this->input->post('country'));
            $id_province = decryptID($this->input->post('province'));
            $city        = ucwords($this->input->post('city'));
     
            $model       = M_City::where('id_city',$id_city)->first();
            if(!empty($model)){
                $data_old = array(
                        "Code City"     => $model->id_city,
                        "Country"       => M_Province::join('ms_country', 'ms_country.id_country', '=', 'ms_province.id_country')->where('id_province', $model->id_province)->first()->country,
                        "Province"      => M_Province::where('id_province', $model->id_province)->first()->province,
                        "City"          => M_City::where('id_city', $model->id_city)->first()->city
                        );

                /* Initialize Data */
                $model->id_province = $id_province;
                $model->city        = $city;

                /* Update */
                $update = $model->save();

                if($update){
                    $data_new = array(
                            "Code City"     => $model->id_city,
                            "Country"       => M_Province::join('ms_country', 'ms_country.id_country', '=', 'ms_province.id_country')->where('id_province', $model->id_province)->first()->country,
                            "Province"      => M_Province::where('id_province', $id_province)->first()->province,
                            "City"          => $city,
                        );

                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message = "City " . $city . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 4);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                    $this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
            }else{
                $this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Delete data from table:ms_city => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_city    = decryptID($id);
        
        $model      = M_City::where('id_city',$id_city)->first();

        if(!empty($model)){

            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
                /* Write Log */
                 $data_notif = array(
                    "Code City"     => $model->id_province,
                    "Country"       => M_Province::join('ms_country', 'ms_country.id_country', '=', 'ms_province.id_country')->where('id_province', $model->id_province)->first()->country,
                    "Province"      => M_Province::where('id_province', $model->id_province)->first()->province,
                    "City"          => $model->city
                );
                $message = "City " . $model->city . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 4);

                $this->session->set_flashdata('success', lang("message_delete_success"));
                redirect(site_url() . $this->site);
            }else{
                $this->session->set_flashdata('error', lang("message_delete_failed"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }
}