<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
		$data['add']       = site_url() . $this->site . '/add';
		$data['edit']      = site_url() . $this->site . '/edit';
		$data['delete']    = site_url() . $this->site . '/delete';
		$data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['detail']    = site_url() . $this->site . '/detail';

		$this->load_view('master','promotion','v_promotion',$data);
	}

	/**
    * Serverside load table:ms_destination
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('id_promotion','promotion','periode_start','periode_end','id_promotion');

        $from  = "ms_promotion";
        $where = "ms_promotion.is_delete = 'F'";

        $order_by = 'id_promotion desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_promotion LIKE '%" . $sSearch . "%'";
            $where .= "OR promotion LIKE '%" . $sSearch . "%'";
            $where .= "OR periode_start LIKE '%" . $sSearch . "%'";
            $where .= "OR periode_end LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_promotion');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_promotion;
            $row_value[]   = $row->promotion;
			$row_value[]   = date('d M Y', strtotime($row->periode_start)).' - '.date('d M Y', strtotime($row->periode_end));
			$row_value[]   = encryptID($row->id_promotion);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
        $data['package_option']   = $this->m_select->selectPackage();
		$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/save';

    	$this->load_view('master','promotion','v_promotion_add',$data);
    }

	/**
    * Save data to table:ms_promotion & ms_promotion_detail
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user        = $this->ion_auth->user()->row();
            $promotion   = $this->input->post('promotion_name');
            $firstDate   = $this->input->post('firstDate');
            $secondDate  = $this->input->post('secondDate');
            $description = $this->input->post('description');
			
			/* Generate Code Prefix: PRM201712001 */
            $year           = date('Y');
            $month          = date('m');
            $prefix         = array('length' => 3, 'prefix' => 'PRM' . $year . $month , 'position' => 'right');
            $kode           = $this->model_general->code_prefix('ms_promotion', 'id_promotion', $prefix);

            $data_promotion = M_Promotion::where('promotion', $promotion)->first();

            if(empty($data_promotion)){
            	/* Initialize Data */
            	$model = new M_Promotion();

                $image_name = '';

                /** Upload Image * */
                if ($this->input->post('image_exist') != "") {
                    if ($_FILES['photofilename']['error'] != 4) {

                        /* Config Upload */
                        $config['upload_path']   = './assets/upload/promotions';
                        $config['allowed_types'] = '*';
                        $config['file_name']     = $kode . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite']     = TRUE;
                        
                        $this->upload->initialize($config);
                        
                        if ($this->upload->do_upload('photofilename')) {
                            $datafile   = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        } else {
                            $status = array('status' => 'error');
                        }
                    }
                } else {
                    $status = array('status' => 'image-blank');
                }
                /** END Upload Image **/   


                $model->id_promotion    = $kode;
                $model->promotion       = $promotion;
                $model->periode_start   = date('Y-m-d', strtotime($firstDate));
                $model->periode_end     = date('Y-m-d', strtotime($secondDate));
                $model->image_promotion = $image_name;
                $model->description     = $description;
                $model->is_delete       = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){

                    
                    //Detail Promotion
                    $count_package = count($this->input->post('id_package'));

                    for ($i = 0; $i < $count_package; $i++) {

                        /* Generate Code Prefix: PRD201712001 */
                        $year_detail   = date('Y');
                        $month_detail  = date('m');
                        $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
                        $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);


                        $id[$i]                         = M_Package::where('package_name', $this->input->post('id_package')[$i])->first();
                        $model_det                      = new M_PromotionDetail();
                        $model_det->id_promotion_detail = $kode_detail;
                        $model_det->id_promotion        = $kode;
                        $model_det->id_package          = $id[$i]->id_package;
                        $model_det->save();
                    }

	            	/* Write Log */
            	    $data_notif = array(
                        "Code Promotion" => $kode,
                        "Promotion"      => $promotion,
                        "Periode Start"  => $firstDate,
                        "Periode End"    => $secondDate,
                        "Description"    => $description
                    );

                    $message = $user->first_name . " " . $user->last_name . " add data  " . $promotion;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 8);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page Edit data
    * @return page
    **/
    function edit($id)
    {        
        $data['package_option'] = $this->m_select->selectPackage();
        $data['cancel']         = site_url() . $this->site;
        $data['action']         = site_url() . $this->site . '/update';
        $id_promotion           = decryptID($id);
        
        $promotion              = M_Promotion::where('id_promotion',$id_promotion)->first();
		if(!empty($promotion)){

			$data['promotion'] = $promotion;

    		$this->load_view('master','promotion','v_promotion_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Get data Package From detail
    * @return Data
    **/
    function getPackage($id)
    {        
         if ($this->input->is_ajax_request()) {
            $model_detail = M_PromotionDetail::
                    where('ms_promotion_detail.id_promotion', decryptID($id))
                    ->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_promotion_detail.id_package')
                    ->get([
                        'ms_promotion_detail.id_package',
                        'ms_package.package_name'
                        ]);
            $this->output->set_content_type('application/json')->set_output(json_encode($model_detail));
        }
    }

    /**
    * Update data to table:ms_destination
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user         = $this->ion_auth->user()->row();
            $promotion    = $this->input->post('promotion_name');
            $firstDate    = $this->input->post('firstDate');
            $secondDate   = $this->input->post('secondDate');
            $description  = $this->input->post('description');
            $id_promotion = decryptID($this->input->post('id_promotion'));
            
            $model       = M_Promotion::where('id_promotion',$id_promotion)->first();

			if(!empty($model)){
				$data_old = array(
                    "Promotion"      => $model->promotion,
                    "Periode Start"  => $model->periode_start,
                    "Periode End"    => $model->periode_end,
                    "Description"    => $model->description
            	);

                /* Update Image */
                if ($this->input->post('image_exist') != "") {
                    if ($_FILES['photofilename']['error'] != 4) {

                        /* IF File Exist */
                        if (file_exists("./assets/upload/promotions/" . $model->image_promotion)) {
                            unlink("./assets/upload/promotions/" . $model->image_promotion);
                        }

                        /* Config Upload */
                        $config['upload_path'] = './assets/upload/promotions';
                        $config['allowed_types'] = '*';
                        $config['file_name'] = $model->id_promotion . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite'] = TRUE;
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('photofilename')) {
                            $datafile = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        }
                    }
                } else {
                    $image_name = $model->image_promotion;
                }
                /* end upload img */

				/* Initialize Data */
                $model->promotion       = $promotion;
                $model->periode_start   = date('Y-m-d', strtotime($firstDate));
                $model->periode_end     = date('Y-m-d', strtotime($secondDate));
                $model->image_promotion = $image_name;
                $model->description     = $description;

            	/* Update */
                $update = $model->save();

                if($update){
                    $update_package = M_PromotionDetail::where('id_promotion', $model->id_promotion);
                    $update_package->delete();
                    //buat event_tag table
                    $count_package = count($this->input->post('id_package'));

                    for ($i = 0; $i < $count_package; $i++) {
                         /* Generate Code Prefix: PRD201712001 */
                        $year_detail   = date('Y');
                        $month_detail  = date('m');
                        $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
                        $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);

                        $id[$i]                         = M_Package::where('package_name', $this->input->post('id_package')[$i])->first();
                        $model_det                      = new M_PromotionDetail();
                        $model_det->id_promotion_detail = $kode_detail;
                        $model_det->id_promotion        = $model->id_promotion;
                        $model_det->id_package          = $id[$i]->id_package;
                        $model_det->save();
                    }

                	$data_new = array(
                            "Code Promotion" => $kode,
                            "Promotion"      => $promotion,
                            "Periode Start"  => $firstDate,
                            "Periode End"    => $secondDate,
                            "Description"    => $description
                        );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Promotion " . $promotion . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 8);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Delete data from table:ms_promotion => Soft Delete
    * Delete data from table:ms_promotion_detail => Hard Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        $user         = $this->ion_auth->user()->row();
        $id_promotion = decryptID($id);
        
        $model          = M_Promotion::where('id_promotion',$id_promotion)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
                $model_det = M_PromotionDetail::where('id_promotion',$id_promotion);
                $model_det->delete();
            	/* Write Log */
            	 $data_notif = array(
                    "Code Promotion" => $model->id_promotion,
                    "Promotion"      => $model->promotion,
                    "Periode Start"  => $model->periode_start,
                    "Periode End"    => $model->periode_end,
                    "Description"    => $model->description
                );
                $message = "Promotion " . $model->Promotion . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 8);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }

     /**
    * Detail data from table:ms_promotion
    * @param Id
    * @return page index
    **/
    public function detail($id) {
        $data['back'] = site_url() . $this->site;

        $id_promotion = decryptID($id);
        $promotion    = M_Promotion::find($id_promotion);
        if (!empty($promotion)) {
            $data['promotion'] = M_Promotion::where('id_promotion',$id_promotion)->first();
            $data['packages']   = M_PromotionDetail::
                                    where('ms_promotion_detail.id_promotion', decryptID($id))
                                    ->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_promotion_detail.id_package')
                                    ->get([
                                        'ms_promotion_detail.id_package',
                                        'ms_package.package_name',
                                        ]);
            $this->load_view('master','promotion','v_promotion_detail',$data);
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }
}