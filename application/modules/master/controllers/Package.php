<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends MX_Controller {

	public function __construct() {
        parent::__construct();

        /* Dynamic Controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index(){

		$data['add']       = site_url() . $this->site . '/add';
		$data['edit']      = site_url() . $this->site . '/edit';
		$data['delete']    = site_url() . $this->site . '/delete';
		$data['loadTable'] = site_url() . $this->site . '/loadTable';

		$this->load_view('master','package','v_package', $data);
	}

	/**
    * Serverside load table:ms_package
    * @return ajax
    **/
    public function loadTable()
	{
		$database_columns = array('id_package','id_package','package_name', 'days', 'nights', 'id_package');

        $from  = "ms_package";
        $where = "is_delete = 'F'";

        $order_by = 'id_package desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_package LIKE '%" . $sSearch . "%'";
            $where .= "OR package_name LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_package');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_package;
            $row_value[]   = $row->package_name;
            $row_value[]   = $row->days;
			$row_value[]   = $row->nights;
			$row_value[]   = encryptID($row->id_package);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
        $data['cancel']            = site_url() . $this->site;
        $data['action']            = site_url() . $this->site . '/save';

    	$this->load_view('master','package','v_package_add',$data);
    }


    /**
    * Save data to table:ms_package
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user    	   = $this->ion_auth->user()->row();
            $package_name  = ucwords($this->input->post('package_name'));
            $days          = $this->input->post('days');
            $nights        = $this->input->post('nights');
			
			/* Generate Code Prefix: PCK2017120001 */
			$year          = date('Y');
			$month   	   = date('m');
			$prefix  	   = array('length' => 4, 'prefix' => 'PCK' . $year . $month , 'position' => 'right');
			$kode    	   = $this->model_general->code_prefix('ms_package', 'id_package', $prefix);
            
            $data_package  = M_Package::where('package_name', $package_name)->first();

            if(empty($data_package)){

            	/* Initialize Data */
            	$model = new M_Package();

				$model->id_package   = $kode;
				$model->package_name = $package_name;
                $model->days         = $days;
                $model->nights       = $nights;
				$model->is_delete  	 = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){

	            	/* Write Log */
            	    $data_notif = array(
						"Code Package" => $kode,
                        "Package Name" => $package_name,
                        "Days"         => $days,
                        "Night"        => $nights
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $package_name;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 7);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function edit($id)
    {        
        $id_package                 = decryptID($id);

        $data['cancel']             = site_url() . $this->site;
        $data['action']             = site_url() . $this->site . '/update';
       
        $package                    = M_Package::where('id_package', $id_package)->first();
    
        if(!empty($package)){

            $data['package']        = $package;

    		$this->load_view('master','package','v_package_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:ms_package
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            /* Get User */
            $user         = $this->ion_auth->user()->row();
            
            /* Set Input Post */
            $id_package   = decryptID($this->input->post('id_package'));
            $package_name = ucwords($this->input->post('package_name'));
            $days         = $this->input->post('days');
            $nights       = $this->input->post('nights');
            
            $model        = M_Package::where('id_package',$id_package)->first();

            if(!empty($model)){
                $data_old = array(
                        "Code Package"  => $model->id_package,     
                        "Package Name"  => $model->package_name,
                        "Days"          => $model->days,
                        "Nights"        => $model->nights  
                        );

                /* Initialize Data */
                $model->package_name    = $package_name;
                $model->days            = $days;
                $model->nights          = $nights;

                /* Update */
                $update = $model->save();

                if($update){
                    $data_new = array(
                            "Code Package"  => $model->id_package,     
                            "Package Name"  => $model->package_name,
                            "Days"          => $model->days,
                            "Nights"        => $model->nights 
                        );

                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Package " . $package_name . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 7);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                    $this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
            }else{
                $this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
            }
        } else {
            $this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Delete data from table:ms_package => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        /* Get User */
        $user       = $this->ion_auth->user()->row();
        $id_package = decryptID($id);
        
        $model      = M_Package::where('id_package',$id_package)->first();

        if(!empty($model)){

            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
                /* Write Log */
                 $data_notif = array(
                    "Code Package"  => $model->id_package,
                    "Package Name"  => $model->package_name,
                    "Days"          => $model->days,
                    "Nights"        => $model->nights
                );
                $message = "Package " . $model->package_name . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 7);

                $this->session->set_flashdata('success', lang("message_delete_success"));
                redirect(site_url() . $this->site);
            }else{
                $this->session->set_flashdata('error', lang("message_delete_failed"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }
}