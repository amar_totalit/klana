<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['detail']    = site_url() . $this->site . '/detail';

		$this->load_view('master','category','v_category',$data);
	}

	/**
    * Serverside load table:ms_category
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('id_category','id_category','category','createdon','id_category');

        $from  = "ms_category";
        $where = "ms_category.is_delete = 'F'";

        $order_by = 'id_category desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_category LIKE '%" . $sSearch . "%'";
            $where .= "OR category LIKE '%" . $sSearch . "%' ";
            $where .= "OR createdon LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_category');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_category;
			$row_value[]   = $row->category;
			$row_value[]   = date('d F Y H:i:s', strtotime($row->createdon));
			$row_value[]   = encryptID($row->id_category);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
		$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/save';

    	$this->load_view('master','category','v_category_add',$data);
    }

	/**
    * Save data to table:ms_category
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user      = $this->ion_auth->user()->row();
			$category  = ucwords($this->input->post('category'));
			$createdby = $user->id;
			$createdon = date('Y-m-d H:i:s');
			
			/* Generate Code Prefix: CAT2017120001 */
			$year    = date('Y');
			$month   = date('m');
			$prefix  = array('length' => 4, 'prefix' => 'CAT' . $year . $month , 'position' => 'right');
			$kode    = $this->model_general->code_prefix('ms_category', 'id_category', $prefix);
            
            $data_category = M_Category::where('category', $category)->first();

            if(empty($data_category)){
            	/* Initialize Data */
            	$model = new M_Category();

            	$image_name = '';

                /** Upload Image * */
                if ($this->input->post('image_exist') != "") {
                    if ($_FILES['photofilename']['error'] != 4) {

                        /* Config Upload */
                        $config['upload_path']   = './assets/upload/content_category';
                        $config['allowed_types'] = '*';
                        $config['file_name']     = $kode . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite']     = TRUE;
                        
                        $this->upload->initialize($config);
                        
                        if ($this->upload->do_upload('photofilename')) {
                            $datafile   = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        } else {
                            $status = array('status' => 'error');
                        }
                    }
                } else {
                    $status = array('status' => 'image-blank');
                }
                /** END Upload Image **/   

				$model->id_category   = $kode;
				$model->category      = $category;
				$model->photofilename = $image_name;
				$model->createdby     = $createdby;
				$model->createdon     = $createdon;
				$model->is_delete     = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){
	            	/* Write Log */
            	    $data_notif = array(
						"Code Category" => $kode,
						"Category"      => $category,
						"Created By"    => $createdby,
						"Created On"    => $createdon,
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $category;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 6);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function edit($id)
    {        
       	$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/update';
		$id_category     = decryptID($id);
		
		$category        = M_Category::where('id_category',$id_category)->first();
		if(!empty($category)){

			$data['category'] = $category;

    		$this->load_view('master','category','v_category_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:ms_category
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user        = $this->ion_auth->user()->row();
			$id_category = decryptID($this->input->post('id_category'));
			$category    = ucwords($this->input->post('category'));
			$changedby   = $user->id;
			$changedon   = date('Y-m-d H:i:s');
			
			$model      = M_Category::where('id_category',$id_category)->first();

			if(!empty($model)){
				$data_old = array(
                                    "Code Category" => $model->id_category,
                                    "Category"      => $model->category,
            	                    "Changed By"    => $model->changedby,
            	                    "Changed On"    => $model->changedon,
                    	           );

				/* Initialize Data */

				/* Update Image */
				if ($this->input->post('image_exist') != "") {
                    if ($_FILES['photofilename']['error'] != 4) {

                        /* IF File Exist */
                        if (file_exists("./assets/upload/content_category/" . $model->photofilename)) {
                            unlink("./assets/upload/content_category/" . $model->photofilename);
                        }

                        /* Config Upload */
                        $config['upload_path'] = './assets/upload/content_category';
                        $config['allowed_types'] = '*';
                        $config['file_name'] = $model->id_category . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite'] = TRUE;
                        
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('photofilename')) {
                            $datafile = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        }
                    }
                } else {
                    $image_name = $model->photofilename;
                }
                /* end upload img */

				$model->category      = $category;
				$model->changedby     = $changedby;
				$model->changedon     = $changedon;
				$model->photofilename = $image_name;

            	/* Update */
                $update = $model->save();

                if($update){
                	$data_new = array(
                            "Code Category" => $model->id_category,
                            "Category"      => $category,
                            "Changed By"    => $changedby,
                            "Changed On"    => $changedon,
                        );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Category " . $category . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 6);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Detail data from table:ms_category
    * @param Id
    * @return page index
    **/
    public function detail($id) {
    	$data['back'] = site_url() . $this->site;

    	$id_category = decryptID($id);
        $category    = M_Category::find($id_category);
        
        $data['usercreated'] = User::where('id', $category->createdby)->first();
        $data['userchanged'] = User::where('id', $category->changedby)->first();
        $data['createdon']   = date('d F Y h:i', strtotime($category->createdon));
        $data['changedon']   = date('d F Y h:i', strtotime($category->changedon));
		$data['category']    = $category;
		$this->load_view('master','category','v_category_detail',$data);
    }


    /**
    * Delete data from table:ms_category => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
		$user       = $this->ion_auth->user()->row();
		$id_category = decryptID($id);
		
		$model      = M_Category::where('id_category',$id_category)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
            	/* Write Log */
            	 $data_notif = array(
					"Code Category" => $model->id_category,
					"Category"      => $model->category,
					"Changed By"    => $model->changedby,
	                "Changed On"    => $model->changedon,
                );
                $message = "Category " . $model->category . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 6);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }
}