<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
        $this->load_view('error_403','403','forbidden');
        die();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

    }

	public function index()
	{
		$data['add']       = site_url() . $this->site . '/add';
		$data['edit']      = site_url() . $this->site . '/edit';
		$data['delete']    = site_url() . $this->site . '/delete';
		$data['loadTable'] = site_url() . $this->site . '/loadTable';

		$this->load_view('master','location','v_location',$data);
	}

	/**
    * Serverside load table:ms_province
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('ms_location.id_location','ms_city.city','ms_location.id_location');

        $from   = "ms_location";
        $join[] = array('ms_city', 'ms_city.id_city = ms_location.id_city', '');
        $where  = "ms_location.is_delete = 'F'";

        $order_by = 'ms_location.id_location desc';

        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_location LIKE '%" . $sSearch . "%'";
            $where .= "OR ms_city.city LIKE '% " . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_location');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('join', $join);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
			$row_value     = array();
			$row_value[]   = $row->id_location;
			$row_value[]   = $row->city;
			$row_value[]   = encryptID($row->id_province);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
        $data['country_options']   = $this->m_select->selectCountry();
        $data['province_options']  = array(''=>'- Select Province -');
        $data['city_options']      = array(''=>'- Select City -');
        $data['cancel']            = site_url() . $this->site;
        $data['action']            = site_url() . $this->site . '/save';
        $data['ajax_get_province'] = site_url() . $this->site . '/ajax_get_province';
        $data['ajax_get_city']     = site_url() . $this->site . '/ajax_get_city';

    	$this->load_view('master','location','v_location_add',$data);
    }

    /**
    * Ajax get Province from Country Parameter
    * @return page
    * @param id_country
    **/
    function ajax_get_province() {
        $id_country  = decryptID($this->input->get('country'));

        $province    = $this->m_select->selectProvince($id_country);
        $result      = form_dropdown('province', $province, '', 'class="form-control"');

        echo json_encode($result);
    }

    /**
    * Ajax get City from Province Parameter
    * @return page
    * @param id_province
    **/
    function ajax_get_city() {
        $id_province = decryptID($this->input->get('province'));
        
        $city        = $this->m_select->selectCity($id_province);
        $result      = form_dropdown('city', $city, '', 'class="form-control"');

        echo json_encode($result);
    }

	/**
    * Save data to table:ms_province
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user     = $this->ion_auth->user()->row();
            $country  = decryptID($this->input->post('country'));
            $province = ucwords($this->input->post('province'));
            
            /* Generate Code Prefix: PRV2017120001 */
            $year     = date('Y');
            $month    = date('m');
            $prefix   = array('length' => 4, 'prefix' => 'PRV' . $year . $month , 'position' => 'right');
            $kode     = $this->model_general->code_prefix('ms_province', 'id_province', $prefix);
            
            $data_province = M_Province::where('province', $province)->first();

            if(empty($data_province)){

            	/* Initialize Data */
            	$model = new M_Province();

                $model->id_province = $kode;
                $model->id_country  = $country;
                $model->province    = $province;
                $model->is_delete   = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){
	            	/* Write Log */
            	    $data_notif = array(
                        "Code Province" => $kode,
                        "Country"       => M_Country::where('id_country', $country)->first()->country,
                        "Province"      => $province
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $country;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 3);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page edit data
    * @return page
    **/
    function edit($id)
    {        
       $data['country_options'] = $this->m_select->selectCountry();
       $data['cancel'] = site_url() . $this->site;
       $data['action'] = site_url() . $this->site . '/update';
       $id_province    = decryptID($id);
       
       $province       = M_Province::where('id_province',$id_province)->first();
		if(!empty($province)){

			$data['province'] = $province;

    		$this->load_view('master','province','v_province_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:ms_province
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user       = $this->ion_auth->user()->row();
			$id_province = decryptID($this->input->post('id_province'));
            $country     = decryptID($this->input->post('country'));
			$province    = ucwords($this->input->post('province'));

            $model      = M_Province::where('id_province',$id_province)->first();
			if(!empty($model)){
				$data_old = array(
                        "Code Province" => $model->id_province,
                        "Country"       => M_Country::where('id_country', $model->id_country)->first()->country,
                        "Province"      => $model->province,
                    	);

				/* Initialize Data */
                $model->id_country = $country;
                $model->province   = $province;

            	/* Update */
                $update = $model->save();

                if($update){
                	$data_new = array(
                            "Code Province" => $model->id_province,
                            "Country"       => M_Country::where('id_country', $country)->first()->country,
                            "Province"      => $province,
                        );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Province " . $province . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 3);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Delete data from table:ms_province => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
		$user       = $this->ion_auth->user()->row();
		$id_province = decryptID($id);
		
		$model      = M_Province::where('id_province',$id_province)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
            	/* Write Log */
            	 $data_notif = array(
					"Code Province" => $model->id_province,
                    "Country"       => M_Country::where('id_country', $model->id_country)->first()->country,
					"Province"      => $model->province,
                );
                $message = "Province " . $model->country . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 3);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }
}