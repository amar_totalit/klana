<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
		$data['add']       = site_url() . $this->site . '/add';
		$data['edit']      = site_url() . $this->site . '/edit';
		$data['delete']    = site_url() . $this->site . '/delete';
		$data['loadTable'] = site_url() . $this->site . '/loadTable';

		$this->load_view('master','country','v_country',$data);
	}

	/**
    * Serverside load table:ms_country
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('id_country','id_country','country','id_country');

        $from  = "ms_country";
        $where = "ms_country.is_delete = 'F'";

        $order_by = 'id_country desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_country LIKE '%" . $sSearch . "%'";
            $where .= "OR country LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_country');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_country;
			$row_value[]   = $row->country;
			$row_value[]   = encryptID($row->id_country);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
		$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/save';

    	$this->load_view('master','country','v_country_add',$data);
    }

	/**
    * Save data to table:ms_country
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user    = $this->ion_auth->user()->row();
			$country = ucwords($this->input->post('country'));
			
			/* Generate Code Prefix: CTR201712001 */
			$year    = date('Y');
			$month   = date('m');
			$prefix  = array('length' => 3, 'prefix' => 'CTR' . $year . $month , 'position' => 'right');
			$kode    = $this->model_general->code_prefix('ms_country', 'id_country', $prefix);
            
            $data_country = M_Country::where('country', $country)->first();

            if(empty($data_country)){
            	/* Initialize Data */
            	$model = new M_Country();

				$model->id_country = $kode;
				$model->country    = $country;
				$model->is_delete  = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){
	            	/* Write Log */
            	    $data_notif = array(
						"Code Country" => $kode,
						"Country"      => $country,
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $country;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 2);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function edit($id)
    {        
       	$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/update';
		$id_country     = decryptID($id);
		
		$country        = M_Country::where('id_country',$id_country)->first();
		if(!empty($country)){

			$data['country'] = $country;

    		$this->load_view('master','country','v_country_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:ms_country
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user       = $this->ion_auth->user()->row();
			$id_country = decryptID($this->input->post('id_country'));
			$country    = ucwords($this->input->post('country'));
			
			$model      = M_Country::where('id_country',$id_country)->first();

			if(!empty($model)){
				$data_old = array(
                        "Code Country" => $model->id_country,
                        "Country"      => $model->country,
                    	);

				/* Initialize Data */
				$model->country = $country;

            	/* Update */
                $update = $model->save();

                if($update){
                	$data_new = array(
                            "Code Country" => $model->id_country,
                            "Country"      => $country,
                        );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "Country " . $country . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 2);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Delete data from table:ms_contry => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
		$user       = $this->ion_auth->user()->row();
		$id_country = decryptID($id);
		
		$model      = M_Country::where('id_country',$id_country)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
            	/* Write Log */
            	 $data_notif = array(
					"Code Country" => $model->id_country,
					"Country"      => $model->country,
                );
                $message = "Country " . $model->country . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 2);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }
}