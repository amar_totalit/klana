@extends('default.views.layouts.default')

@section('title') KLANA - Destination @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Destination</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Destination </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form Destination</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-destination', 'class' => 'form-horizontal')) }}
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Province <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('province', $province_option, '', "class='form-control' data-live-search='true' id='province'"); }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Destination <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'destination','class' => 'form-control', 'value' => $destination->destination ))}}
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                            {{ form_input(array('type' => 'hidden','name' => 'id_destination','class' => 'form-control', 'value' => encryptID($destination->id_destination) ))}}

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    $('#province').val('{{encryptID($destination->id_province)}}').change();
    // Pengaturan Form Validation 
    var form_validator = $("#form-destination").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                destination: {
                    required: true,
                    maxlenght : 100
                },
            },
        messages: {

        },
    });
</script>
@stop