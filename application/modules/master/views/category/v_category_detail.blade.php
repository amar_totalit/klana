@extends('default.views.layouts.default')

@section('title') KLANA - Category @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Category</a>
            	<i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Detail Category</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail Category </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    
    <div class="row">
		<div class="col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
                    <div class="caption font-dark">
                        <a href="{{$back}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>{{lang('button_back')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
            		<div class="row">
            			<div class="col-md-7 col-md-offset-1">
            				<div class="row">
            					<div class="col-md-4"><h4>Code Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$category->id_category}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$category->category}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Created By</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$usercreated->first_name.$usercreated->last_name}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Created On</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$createdon}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Changed By</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7">
	            					@if(!empty($category->changedby))
            							<h4>{{$userchanged->first_name.$userchanged->last_name }}</h4>
            						@endif
            					</div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Code Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7">
	            					@if(!empty($category->changedon))
		            				<h4>{{$changedon}}</h4>
		            				@endif
	            				</div>	
            				</div>
            				
	                		<!-- <table style="width:100%">
								<tr>
									<th>Code Category</th>
									<td>{{$category->id_category}}</td>
								</tr>
								<tr>
									<th>Category</th>
									<td>{{$category->category}}</td>
								</tr>
								<tr>
									<th>Created By</th>
									<td>{{$usercreated->first_name.$usercreated->last_name}}</td>
								</tr>
								<tr>
									<th>Created On</th>
									<td>{{$createdon}}</td>
								</tr>
								<tr>
									<th>Chaged By</th>
									@if(!empty($category->changedby))
									<td>{{$userchanged->first_name.$userchanged->last_name }}</td>
									@else
									<td>-</td>
									@endif
								</tr>
								<tr>
									<th>Changed On</th>
									@if(!empty($category->changedon))
									<td>{{$changedon}}</td>
									@else
									<td>-</td>
									@endif
								</tr>
							</table> -->
	                	</div>
	                	<div class="col-md-4">
	                		@if(!empty($category->photofilename))
	                		<img src="{{ base_url() . 'assets/upload/content_category/' .$category->photofilename }}" height="200">
	                		@endif
	                		<!-- <table>
	                			<tr>
									<th>Photo</th>
									@if(!empty($category->photofilename))
									<td><img src="{{ base_url() . 'assets/upload/content_category/' .$category->photofilename }}" height="200"></td>
									@else
									<td>-</td>
									@endif
								</tr>
	                		</table> -->
	                	</div>
            		</div>    	
				</div>
				<br>
			</div>
		</div>
	</div>

</div>

@stop

@section('scripts')

@stop