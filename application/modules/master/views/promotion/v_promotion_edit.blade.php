@extends('default.views.layouts.default')

@section('title') KLANA - Promotion @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Promotion</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Promotion </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form Promotion</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-promotion', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
                            {{ form_input(array('id' => 'image_exist','name' => 'image_exist','type' => 'hidden')) }}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Promotion Name <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_input(array('type' => 'text','name' => 'promotion_name','value'=>$promotion->promotion,'class' => 'form-control'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Periode <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="firstDate" data-target-input="nearest">
                                            <input type="text" name="firstDate" id="firstDateVal" class="form-control datetimepicker-input" data-target="#firstDate" readonly value="{{date('d M Y', strtotime($promotion->periode_start))}}" />
                                            <span class="input-group-addon" data-target="#firstDate" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-1">Until </label>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="secondDate" data-target-input="nearest">
                                            <input type="text" name="secondDate" class="form-control datetimepicker-input" data-target="#secondDate" readonly value="{{date('d M Y', strtotime($promotion->periode_end))}}"/>
                                            <span class="input-group-addon" data-target="#secondDate" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Photo <span style="color: red">*</span></label>
                                <label class="control-label col-md-1 titikdua">:</label>
                                <div class="col-md-7">
                                    <input type="file" name="photofilename" id="photofilename" onchange="ValidateImage(this, 'cover');" style="display: none;">
                                    <div align="center">
                                        <img src="{{ base_url() . 'assets/upload/promotions/'.$promotion->image_promotion }}" class="img-responsive" id="image-preview" style="cursor: pointer;max-height:200px;height:200px" onclick="$('#photofilename').click();">
                                        <br>
                                        <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#photofilename').click()">
                                            <i class="fa fa-upload"></i> Upload Image
                                        </button>
                                        <button id="btn-remove" type="button" class="btn red btn-sm" onclick="clearImage()">
                                            <i class="fa fa-remove"></i> Delete
                                        </button>
                                    </div>   
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Description <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control ckeditor" name="description" id="description">
                                            {{$promotion->description}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Package <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-7">
                                        {{ form_dropdown('package', $package_option, '', "class='form-control' data-live-search='true' id='package'"); }}
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-5">
                                    <button type="button" id='buttonBind' class="btn blue btn-sm" data-bind="click: addTransfer">Add Package to list</button>
                                    <!-- <button type="button" class="btn blue btn-sm" id="cek"><i class="fa fa-plus"></i>cek</button> -->
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <div class="form-group form-md-line-input">
                                            <table class="table table-striped table-bordered table-hover" width="auto" >
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Package</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: dataTransfer" id="list-package">
                                                    <tr>
                                                        <td data-bind="text: ($index() + 1)">No</td>
                                                        <td data-bind="text: id_package"></td>
                                                        <td>
                                                            <input type="hidden" name="id_package[]" data-bind="value: id_package" class="form-control id_package">
                                                            <button type="button" data-bind="click: $parent.removeTransfer" class="btn btn-danger btn-icon-only btn-circle"><i class="fa fa-remove"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                            {{ form_input(array('type' => 'hidden','name' => 'id_promotion','class' => 'form-control', 'value' => encryptID($promotion->id_promotion) ))}}

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    // Pengaturan Form Validation 
    var form_validator = $("#form-promotion").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                promotion: {
                    required: true,
                    maxlenght : 100
                },
            },
        messages: {

        },
    });

    //Binding KO JS 
     var modelTransfer = function (id_package)
        {
            var self = this;
            self.id_package = ko.observable(id_package);
        }
        var viewTransferModel = function () {
            var self = this;
            self.dataTransfer = ko.observableArray();

            $.getJSON('{{ base_url() }}master/promotion/getPackage/{{encryptID($promotion->id_promotion)}}' , function (data) {
                $.each(data, function (index, val) {
                    var row_transfer = new modelTransfer(
                        val.package_name
                    );
                    self.dataTransfer.push(row_transfer);
                });
            });
            self.addTransfer = function () {

                if (!$('#form-promotion').valid()) {
                    return;
                }
                if ($('#package').val() == '') {
                    toastr.error('Cant select empty value.', 'Notification!');
                    $('#package').val('').change();
                    return;   
                }
                var status_package = true;
                ko.utils.arrayForEach(self.dataTransfer(), function (item) {
                    if ($('#package').val() == item.id_package()) {
                        status_package = false;
                    }
                });
                if (status_package == false) {
                    toastr.error('Package has selected.', 'Notifikasi!');
                    $('#package').val('').change();
                    return;
                }

                var row_transfer = new modelTransfer($('#package').val());
                self.dataTransfer.push(row_transfer);
                $("#package").val('').change();

            }

            self.removeTransfer = function () {
                self.dataTransfer.remove(this);
            }
        }
        ko.applyBindings(new viewTransferModel());
</script>
@stop