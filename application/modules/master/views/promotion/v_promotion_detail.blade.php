@extends('default.views.layouts.default')

@section('title') KLANA - Promotions @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Promotion</a>
            	<i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Detail Promotion</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail Promotion </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    
    <div class="row">
		<div class="col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
                    <div class="caption font-dark pull-right">
                        <a href="{{$back}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>{{lang('button_back')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
            		<div class="row">
            			<div class="col-md-6 col-md-offset-3">
            				<div class="row">
            					<div class="col-sm-12">
            						<img src="{{base_url().'assets/upload/promotions/'.$promotion->image_promotion}}" style="width: 100%;height: 100%;">
            					</div>
            				</div>
            			</div>
            		</div>
            		<br>
            		<div class="row">
            			<div class="col-md-10 col-md-offset-1">
            				<div class="row">
            					<div class="col-md-6">
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Promotion Name</b></div>
	            						<div class="col-sm-1">:</div>
	            						<div class="col-sm-6">{{$promotion->promotion}}</div>
	            					</div>
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Periode</b></div>
	            						<div class="col-sm-1">:</div>
	            						<div class="col-sm-6">{{date('d M Y', strtotime($promotion->periode_start))}} - {{date('d M Y', strtotime($promotion->periode_end))}}</div>
	            					</div>
	            					<div class="col-md-12">
	            						<div class="col-sm-5"><b>Package</b></div>
	            						<div class="col-sm-1">:</div>
	            						<?php $no = 0 ?>
	            						@foreach($packages as $package)
	            							<div class=" col-sm-6 {{ ($no != 0) ? 'col-md-offset-6' : null }}">- {{$package->package_name}}</div>
	            							<?php $no++ ?>
	            						@endforeach
	            					</div>
            					</div>
            					<div class="col-md-12">
            						<div class="col-sm-12">
            							<div class="col-sm-12">
            								<b>Description</b> :</div>
            							</div>
            						<div class="col-sm-12">
            							<div class="col-sm-12">
            								{{$promotion->description}}
            							</div>
        							</div>
        						</div>
            				</div>
            				<!-- <div class="row">
            					<div class="col-md-4"><h4>Code Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$category->id_category}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$category->category}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Created By</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$usercreated->first_name.$usercreated->last_name}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Created On</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$createdon}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Changed By</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7">
	            					@if(!empty($category->changedby))
            							<h4>{{$userchanged->first_name.$userchanged->last_name }}</h4>
            						@endif
            					</div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Code Category</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7">
	            					@if(!empty($category->changedon))
		            				<h4>{{$changedon}}</h4>
		            				@endif
	            				</div>	
            				</div>
	                	</div>
	                	<div class="col-md-4">
	                		@if(!empty($category->photofilename))
	                		<img src="{{ base_url() . 'assets/upload/content_category/' .$category->photofilename }}" height="200">
	                		@endif
	                	</div> -->
            		</div>    	
				</div>
				<br>
			</div>
		</div>
	</div>

</div>

@stop

@section('scripts')

@stop