@extends('default.views.layouts.default')

@section('title') KLANA - Package @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Package</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Add</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Package </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form Package</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-package', 'class' => 'form-horizontal')) }}
                        
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Package Name <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'package_name','class' => 'form-control'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Package Product <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-1">
                                        {{ form_input(array('type' => 'text','name' => 'days','class' => 'form-control'))}}
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label"><b>Days</b></label>
                                    </div>
                                    <div class="col-md-1">
                                        {{ form_input(array('type' => 'text','name' => 'nights','class' => 'form-control'))}}
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label"><b>Nights</b></label>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

    // Pengaturan Form Validation 
    var form_validator = $("#form-package").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                package_name: {
                    required: true,
                    maxlength: 100
                },
                days: {
                    required: true
                },
                night: {
                    required: true
                },
            },
        messages: {

        },
    });

</script>
@stop