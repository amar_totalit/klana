@extends('default.views.layouts.default')

@section('title') KLANA - Location @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Location</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Location </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <a href="{{$add}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>{{lang('button_add_new')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        @if($success != "")
                            <div class="alert alert-dismissable alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-check"></i> {{$success}}
                            </div>
                        @endif

                        @if($error != "")
                            <div class="alert alert-dismissable alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-times"></i> {{$error}}
                            </div>
                        @endif

                        <table id="table-location" class="table table-striped table-bordered table-hover dt-responsive" width="100%" >
                            <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th width="100">City</th>
                                    <th width="100">Location</th>
                                    <th width="100">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTable  = '{{$loadTable}}';
    var url_delete = '{{$delete}}';

    // Datatable
    var oTable = $('#table-location').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadTable,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_edit = '<a href="{{$edit}}/'+ row[3] +'" type="button" class="btn btn-warning btn-icon-only btn-circle" title="Edit"><i class="fa fa-edit"></i></a>';
                    var btn_delete = '<button onClick="deleteData(\'' + row[3] + '\',$(this))" class="btn btn-danger btn-icon-only btn-circle" title="Delete"><i class="fa fa-trash"></i></button>';
                    var render_html = btn_edit + ' ' + btn_delete;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Delete Data
    function deleteData(value, el) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Are You Sure ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-province'
                });

                window.location.href = url_delete + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
</script>
@stop