@extends('default.views.layouts.default')

@section('title') KLANA - Location @stop

@section('body')
<style type="text/css">
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map{
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Master</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Location</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Add</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Location </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form Location</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-province', 'class' => 'form-horizontal')) }}

                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Country <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('country', $country_options, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Province <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('province', $province_options, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">City <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('city', $city_options, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Location <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'location','class' => 'form-control'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Latitude <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'lat','class' => 'form-control','id'=>'lat','readonly'=>''))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Longitude <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'lng','class' => 'form-control','id'=>'lng','readonly'=>''))}}
                                    </div>
                                </div>
                            </div>
                            <!-- for maps location selector -->
                            <div class="form-group form-md-line-input" id="tgl_acara">
                               <div class="md-radio-inline col-md-12" style="height: 500px">
                                    <div class="" id="map"></div>
                                    <input id="pac-input" class="controls" type="text"
                                    placeholder="Enter a location">
                                    <div id="type-selector" class="controls">
                                      <input type="radio" name="type" id="changetype-all" checked="checked">
                                      <label for="changetype-all">All</label>

                                      <input type="radio" name="type" id="changetype-establishment">
                                      <label for="changetype-establishment">Establishments</label>

                                      <input type="radio" name="type" id="changetype-address">
                                      <label for="changetype-address">Addresses</label>

                                      <input type="radio" name="type" id="changetype-geocode">
                                      <label for="changetype-geocode">Geocodes</label>
                                    </div>
                                </div>
                                <label class="col-md-12">*Drag pointer to select </label>
                            </div>
                            <hr>
                            <!-- for maps location selector -->


                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXB5L3dbCTQeIEHAHTgSjdZwIFLT-lzDI&libraries=places&callback=initMap" async defer></script>

<script type="text/javascript">

    //setting for google maps selector
    $('#pac-input').on('keypress', function(e) {
            return e.which !== 13;
    });
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -6.26293, lng: 106.79918299999997},
            zoom: 10
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input')
        );

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        //marker...
        var marker = new google.maps.Marker({
            position : {lat: -6.26293, lng: 106.79918299999997
            },
            map : map,
            draggable : true
        });

        //dragend event of marker
        google.maps.event.addListener(marker,'dragend',function () {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        });
    };


    /* Ajax Get Province by Country Parameter*/
    var ajax_get_province = '{{$ajax_get_province}}';
    $('select[name="country"]').change(function() {
        $('select[name="province"]').val('').change();
        var country = $(this).val();
        var data    = {country: country};

        $.ajax({
            type: 'GET',
            url: ajax_get_province,
            data: data,
            cache: false,
            success: function(result) {
                var oObj = JSON.parse(result);

                $('select[name="province"]').html(oObj);
            }
        });
    });

    /* Ajax Get City by Province Parameter*/
    var ajax_get_city = '{{$ajax_get_city}}';
    $('select[name="province"]').change(function() {
        $('select[name="city"]').val('').change();
        var province = $(this).val();
        var data     = {province: province};

        $.ajax({
            type: 'GET',
            url: ajax_get_city,
            data: data,
            cache: false,
            success: function(result) {
                var oObj = JSON.parse(result);

                $('select[name="city"]').html(oObj);
            }
        });
    });

    // Pengaturan Form Validation 
    var form_validator = $("#form-province").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                country: {
                    required: true,
                },
                province: {
                    required: true,
                    maxlength: 30
                },
            },
        messages: {

        },
    });
</script>
@stop