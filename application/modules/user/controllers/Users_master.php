<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_master extends MX_Controller {
    public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

    function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        $this->load->library('form_validation');
    }
    
    public function index() {
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['loadTable'] = site_url() . $this->site . '/loadTable';

        $this->load_view('user','master_user','v_user',$data);
    }

    // /**
    // * Serverside load table:users
    // * @return ajax
    // **/
    // public function loadTable() {
    //     $database_columns = array(
    //         'id',
    //         'username',
    //         'email'
    //     );
    //     $from = "users";
    //     $where = '';
    //     $order_by = 'id desc';
    //     if ($this->input->get('sSearch') != '') {
    //         $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
    //         $where .= "username LIKE '%" . $sSearch . "%'";
    //         $where .= "OR email LIKE '%" . $sSearch . "%'";
    //         //$where .= "OR user_level LIKE '%" . $sSearch . "%'";
    //     }

    //     $this->datatables->set_index('id');
    //     $this->datatables->config('database_columns', $database_columns);
    //     $this->datatables->config('from', $from);
    //     $this->datatables->config('where', $where);
    //     $this->datatables->config('order_by', $order_by);
    //     $selected_data = $this->datatables->get_select_data();
    //     $aa_data = $selected_data['aaData'];
    //     $new_aa_data = array();
    //     foreach ($aa_data as $row) {

    //         $user_groups = $this->ion_auth->get_users_groups($row->id)->row()->name;
            
    //         $row_value = array();
    //         $row_value[] = encryptID($row->id);
    //         $row_value[] = $row->username;
    //         $row_value[] = $row->email;
    //         $row_value[] = $user_groups;
    //         $row_value[] = encryptID($row->id);
    //         $new_aa_data[] = $row_value;
    //     }
    //     $selected_data['aaData'] = $new_aa_data;
    //     $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    // }

    /**
    * Serverside load table:users
    * @return ajax
    **/
    public function loadTable()
    {
        $database_columns = array('users.id','users.id','users.username','users.email','groups.name','users.id');

        $from  = "users";
        $join[] = array('users_groups', 'users_groups.user_id = users.id', '');
        $join[] = array('groups', 'groups.id = users_groups.group_id', '');
        $where = "";

        $order_by = 'id desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id LIKE '%" . $sSearch . "%'";
            $where .= "OR username LIKE '%" . $sSearch . "%')";
            $where .= "OR email LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('users.id');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('join', $join);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $user_groups = $this->ion_auth->get_users_groups($row->id)->row()->name;
            
            $row_value     = array();
            $row_value[]   = encryptID($row->id);
            $row_value[]   = $row->username;
            $row_value[]   = $row->email;
            $row_value[]   = $row->name;
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    /**
    * Direct to page input data
    * @return page
    **/
    public function add()
    {   
        $data['cancel']             = site_url() . $this->site;
        $data['action']             = site_url() . $this->site . '/save';
        $data['check_username']     = site_url() . $this->site . '/check_username';
        $data['check_email']        = site_url() . $this->site . '/check_email';
        $data['option_status_user'] = $this->m_select->selectStatusUser();
        $data['option_group']       = $this->m_select->selectGroup();

        $this->load_view('user','master_user','v_user_add',$data);
    }

    /**
    * Save data to table:users
    * @param Post Data
    * @return page index
    **/
    public function save() {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user       = $this->ion_auth->user()->row();
            
            /* Set Post Data From View */
            $username   = $this->input->post('username');
            $first_name = $this->input->post('first_name');
            $last_name  = $this->input->post('last_name');
            $full_name  = $this->input->post('fullname');
            $email      = $this->input->post('email');
            $level      = $this->input->post('user_level');
            $is_aktif   = $this->input->post('is_aktif');
            $tanggal    = date('Y-m-d');


            /* Generate Code Prefix id_user:  USR201712000001 */
            $year    = date('Y');
            $month   = date('m');
            $prefix  = array('length' => 6, 'prefix' => 'USR' . $year . $month , 'position' => 'right');
            $kode    = $this->model_general->code_prefix('users', 'id', $prefix);

            /* Generate Code Prefix code_agent: AGT201712000001 */
            $year      = date('Y');
            $month     = date('m');
            $prefix    = array('length' => 6, 'prefix' => 'AGT' . $year . $month , 'position' => 'right');
            $kode_agen = $this->model_general->code_prefix('users', 'code_agent', $prefix);

            /* Generate Code Prefix code_tripper: TRP201712000001 */
            $year         = date('Y');
            $month        = date('m');
            $prefix       = array('length' => 6, 'prefix' => 'TRP' . $year . $month , 'position' => 'right');
            $kode_tripper = $this->model_general->code_prefix('users', 'code_tripper', $prefix);

            /* Initialize Additional Data to Table users */
            $password        = 'password';
            $password_mobile = md5($password);

            /* note 2 => Agent , 4 => Tripper */
            $additional_data = array(
                                        'id'               => $kode,
                                        'code_agent'       => ($level == 2) ? $kode_agen : null, 
                                        'code_tripper'     => ($level == 4) ? $kode_tripper : null,
                                        'first_name'       => $first_name,
                                        'last_name'        => $last_name,
                                        'username'         => $username,
                                        'created_by'       => $user->id,
                                        'created_on'       => $tanggal,
                                        'email'            => $email,
                                        'password_mobile'  => $password_mobile,
                                        'active'           => $is_aktif,
                                        'full_name'        => $first_name . ' ' . $last_name,
                                        'is_approve_agent' => ($level == 2) ? 'P' : null
                                    );

            /* Set User Group */
            $group          = array($level); 
            $this->ion_auth->register($username, $password, $email, $additional_data, $group, $kode);
            
            /* Set Data User Group */
            $data_group     = $this->ion_auth->group($level)->row();
            $data_notif = array(
                'Full Name'         => $full_name,
                'Username'          => $username,
                'Email'             => $email,
                'Users Level'       => $data_group->description,
                'Registration Date' => date('j F Y')
            );

            /* Set Log Data */
            $message                 = $user->full_name . " has registered " . $full_name . " as " . $data_group->description;
            $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 1);
            $data_email['creator']   = $user->full_name;
            $data_email['full_name'] = $first_name . ' ' . $last_name;
            $data_email['email']     = $email;
            $data_email['password']  = $password;
            $pesan                   = $this->load->view('email/confirm_registration', $data_email, TRUE);
            

            /*  Configurasi Email  */
            require 'phpmailer/PHPMailerAutoload.php';

            //Create a new PHPMailer instance
            $mail = new PHPMailer;

            //Tell PHPMailer to use SMTP
            $mail->isSMTP();

            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            // $mail->SMTPDebug = 1;

            //Ask for HTML-friendly debug output
            $mail->Debugoutput = 'html';

            //Set the hostname of the mail server
            // $mail->Host = 'smtp.gmail.com';
            $mail->Host = gethostbyname('smtp.gmail.com');
            // use
            // $mail->Host = gethostbyname('smtp.gmail.com');
            // if your network does not support SMTP over IPv6

            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            $mail->Port = 587;

            //Set the encryption system to use - ssl (deprecated) or tls
            $mail->SMTPSecure = 'tls';
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            
            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = "infowahanaarta@gmail.com";

            //Password to use for SMTP authentication
            $mail->Password = "totalit.co.id";

            //Set who the message is to be sent from
            $mail->setFrom('infowahanaarta@gmail.com', 'Klana');

            //Set an alternative reply-to address
            // $mail->addReplyTo('tkazumi66@gmail.com', 'First Last');

            //Set who the message is to be sent to
            $mail->addAddress($email, $full_name);

            //Set the subject line
            $mail->Subject = 'Selamat Datang di Klana Management System';

            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $mail->msgHTML($pesan);
                                // 1, 2, 3, 4, 5
            $mail->crlf = "\n";                     // "\r\n" or "\n" or "\r"
            $mail->newline = "\n";  

            //Replace the plain text body with one created manually
            $mail->AltBody = 'This is a plain-text message body';

            //send the message, check for errors
            if ($mail->send()) {
                $this->session->set_flashdata('success', lang("message_save_success"));
                redirect(site_url() . $this->site);
            } else {
                $this->session->set_flashdata('error', lang("message_save_failed"));
                redirect(site_url() . $this->site);
            }            
        }
    }

    public function edit($id) 
    {
        $id_user                    = decryptID($id);

        $data['cancel']             = site_url() . $this->site;
        $data['action']             = site_url() . $this->site . '/update';
        $data['check_username']     = site_url() . $this->site . '/check_username';
        $data['check_email']        = site_url() . $this->site . '/check_email';
        $data['option_status_user'] = $this->m_select->selectStatusUser();
        $data['option_group']       = $this->m_select->selectGroup();

        $user = User::select('users.id AS id_user',
                             'users.*',
                             'users_groups.id AS user_group_id', 
                             'groups.id AS group_id',
                             'groups.name')->join('users_groups', 'users_groups.user_id', '=', 'users.id')->join('groups', 'groups.id', '=', 'users_groups.group_id')->where('users.id', $id_user)->first();
    
        if(!empty($user)){

           $data['data_user']               = $user;

            $this->load_view('user','master_user','v_user_edit',$data);
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Update data to table:users
    * @param Post Data
    * @return page index
    **/
    public function update() {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user         = $this->ion_auth->user()->row();
            
            /* Set Post Data From View */
            $id_user      = decryptID($this->input->post('id'));
            // print_r($id_user);
            // die();
            $code_agent   = decryptID($this->input->post('code_agent'));
            $code_tripper = decryptID($this->input->post('code_tripper'));
            $username     = $this->input->post('username');
            $first_name   = $this->input->post('first_name');
            $last_name    = $this->input->post('last_name');
            $full_name    = $this->input->post('fullname');
            $email        = $this->input->post('email');
            $level        = $this->input->post('user_level');
            $is_aktif     = $this->input->post('is_aktif');

            $data_model = User::where('id', $id_user)->first();
       
            if(!empty($data_model)){
                $data_old = array(
                                    'Full Name'         => $first_name . ' ' . $last_name,
                                    'Username'          => $username,
                                    'Email'             => $email,
                                    'Users Level'       => $level
                                );

                /* Initialize Data */
                $data_model->code_agent    = !empty($code_agent) ? $code_agent : null;
                $data_model->code_tripper  = !empty($code_tripper) ? $code_tripper : null;
                $data_model->first_name    = $first_name;
                $data_model->last_name     = $last_name;
                $data_model->username      = $username;
                $data_model->full_name     = $first_name . ' ' . $last_name;
                $data_model->email         = $email;
                $data_model->active        = $is_aktif;
                $data_model->edited_by     = $user->id;

                /* Update */
                $update = $data_model->save();

                if($update){
                    $data_new = array(
                            'Full Name'         => $first_name . ' ' . $last_name,
                            'Username'          => $username,
                            'Email'             => $email,
                            'Users Level'       => $level
                        );

                    /* Write Log */
                    $data_change = array_diff_assoc($data_new, $data_old);
                    $message = "User " . $full_name . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 1);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                    $this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
            }else{
                $this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
            }  
        }else{
            $this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }

    public function view() {
        if ($this->input->is_ajax_request()) {
            $id          = decryptID($this->input->get('id'));
            $users       = User::find($id);
            $user_groups = $this->ion_auth->get_users_groups($id)->row()->id;
            $model = array(
                'status' => 'success', 
                'data'   => $users,
                'group'  => $user_groups
            );
        } else {
            $model = array('status' => 'error', 'message' => 'Not Found.');
        }
        $data = $model;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    
    public function delete() {
        if ($this->input->is_ajax_request()) {
            $id    = decryptID($this->input->get('id'));
            $user  = $this->ion_auth->user()->row();
            $model = User::find($id);
            if (!empty($model)) {
                
                $delete = $model->delete();
                $data_notif = array(
                    'Full Name'         => $model->full_name,
                    'Username'          => $model->username,
                    'Email'             => $model->email,
                    'Registration Date' => date('j F Y', strtotime($model->created_on))
                );

                $message = "User " . $model->username . " was deleted by " . $user->full_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 1);
                $this->session->set_flashdata('success', lang("message_delete_success"));
                $status = array('status' => 'success');
            } else {
                $this->session->set_flashdata('error', lang("message_delete_failed"));
                $status = array('status' => 'error');
            }
        } else {
            $status = array('status' => 'error');
        }
        $data = $status;
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
    * Check username exist table:users
    * @param Post Data
    **/
    public function check_username() {
        $id_user    = decryptID($this->input->get('id_user'));
        $username   = $this->input->get('username');

        $data_users = User::where('username',$username)->first();

        if(!empty($id_user)){
            $data_users = User::where('username',$username)->whereRaw('users.id != "'.$id_user.'"')->first();
        }else{
            $data_users = User::where('username',$username)->first();
        }

        if(!empty($data_users)){
            echo 'false';
        }else{
            echo 'true';
        }
    }

    /**
    * Check email exist table:user
    * @param Post Data
    **/
    public function check_email() {
        $id_user    = decryptID($this->input->get('id_user'));
        $email      = $this->input->get('email');
        if(!empty($id_user)){
            $data_users = User::where('email',$email)->whereRaw('users.id != "'.$id_user.'"')->first();
        }else{
            $data_users = User::where('email',$email)->first();
        }

        if(!empty($data_users)){
            echo 'false';
        }else{
            echo 'true';
        }
    }

}
