<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('Recaptcha');
        $this->load->model('ion_auth_model');
    }

    function login() {

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == FALSE ) {
            $data['message']         = $this->session->flashdata('message');
            $data['message_error']   = $this->session->flashdata('message_error');
            $data['form_attributes'] = array("autocomplete" => "off", 'class' => 'login-form');

            $data['email'] = array(
                'name'        => 'email',
                'class'       => 'form-control form-control-solid placeholder-no-fix form-group',
                'placeholder' => 'Email',
                'type'        => 'email',
                'required'    => 'required',
                'value'       => $this->form_validation->set_value('email'),
            );

            $data['password'] = array(
                'name'        => 'password',
                'required'    => 'required',
                'class'       => 'form-control form-control-solid placeholder-no-fix form-group',
                'placeholder' => 'Password',
            );
            $data['csrftoken_name']  = $this->security->get_csrf_token_name();
            $data['csrftoken_value'] = $this->security->get_csrf_hash();
            
            $data['captcha']         = $this->recaptcha->getWidget();
            $data['script_captcha']  = $this->recaptcha->getScriptTag();
            
            $this->load->blade('user.views.login.page', $data);
        } else {
            if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'))) {
                $this->ion_auth->clear_login_attempts($this->input->post('email'));
                // Get Data Untuk Login
                $data = array(
                   'email'    => $this->input->post('email'), 
                   'password' => $this->input->post('password')
                );
                $this->session->set_userdata('user_login', $data);
                

                $user                = $this->ion_auth->user()->row();
                $user_groups         = $this->ion_auth->get_users_groups($user->id)->row()->name;

                redirect('dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('login', 'refresh');
            }
        }
    }

    function logout() {
        $this->ion_auth->logout();
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('login', 'refresh');
    }

    function change_password() {
        $this->load_view('user','user','v_page_change_password');
    } 

    function profile() {
        $data['action']       = 'profile-save';

        $this->load_view('user','user','v_profile',$data);
    }

    function profile_save(){
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user    = $this->ion_auth->user()->row();
            $id_user = decryptID($this->input->post('id'));

            $model        = User::where('id',$id_user)->first();
            $filename_old = 'assets/upload/users/' . $model->photo;

            /** Upload Image * */
            if ($_FILES['filename']['error'] != 4) {

                /* Config Upload */
                $config['upload_path']   = './assets/upload/users';
                $config['allowed_types'] = 'jpg|png';
                $config['file_name']     = $user->id . '_' . time();
                $config['overwrite']     = TRUE;
                
                $this->upload->initialize($config);
                
                if ($this->upload->do_upload('filename')) {
                    $datafile   = $this->upload->data();
                    $image_name = $datafile['file_name'];
                }
            }

            /** END Upload Image **/   
            $filename_upload = 'assets/upload/users/' . $image_name;
            if(file_exists($filename_upload)){
                if(file_exists($filename_old)){
                    unlink('assets/upload/users/' . $model->photo);
                }
                $photo = $image_name;
            }else{
                $photo = $model->photo;
            }

            $model->photo = $photo;
            $model->save();
           
            $this->session->set_flashdata('success', lang("message_save_success"));
            redirect('profile');
        }else{
            $this->session->set_flashdata('error', lang("message_save_failed"));
            redirect('profile');
        }
    }

    function change_password_save() {
        if ($this->input->is_ajax_request()) {
            $user   = $this->ion_auth->user()->row();
            $new_pw = $this->input->post('new_password');
            $old_pw = $this->input->post('old_password');
            $ret_pw = $this->input->post('retype_password');

            if ($new_pw != $ret_pw) {
                $status = array('status' => 'wrong_password');
            } else {
                if ($this->ion_auth->change_password($user->email, $old_pw, $new_pw)) {
                    $status = array('status' => 'success');
                } else {
                    $status = array('status' => 'error');
                }
            }

            $data = $status;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    function forgot_password() {
        $this->form_validation->set_rules('email', 'Email Address', 'required');
        $identity = $this->input->post('email');

        /* Set Kondisi Untuk Captcha */
        //if($response['success']) {

            /* Set Kondisi Validasi Post Email*/
            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('messages', validation_errors());
                redirect("login", 'refresh');
            } else {
                $forgotten    = $this->ion_auth->forgotten_password($identity);

                if ($forgotten == 1) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                } else {
                    $this->session->set_flashdata('message_error', $this->ion_auth->errors());
                }
                redirect("login", 'refresh');
            } 
        //}
    }


    function reset_password($code = NULL) {
        if (!$code) {
            show_404();
        }
        $user = $this->ion_auth->forgotten_password_check($code);

        if (!empty($user)) {
            $this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[8]|trim');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_password]|trim');
            
            if ($this->form_validation->run() == false) {
                if (!empty(validation_errors())) {
                    $this->session->set_flashdata('message', validation_errors());
                }

                $data['new_password'] = array(
                    'name'        => 'new_password',
                    'required'    => 'required',
                    'class'       => 'form-control placeholder-no-fix',
                    'placeholder' => 'New Password',
                );

                $data['confirm_password'] = array(
                    'name'        => 'confirm_password',
                    'required'    => 'required',
                    'class'       => 'form-control placeholder-no-fix',
                    'placeholder' => 'Confirm Password',
                );

                $data['user_id']         = $user->id;
                
                $data['form_attributes'] = array("autocomplete" => "off", 'class' => 'login-form');
                $data['code']            = $code;


                $data['message_error']   = $this->session->flashdata('message_error');
                $data['message_success'] = $this->session->flashdata('message_success');
                $data['message']        = $this->session->flashdata('message');

                $this->load->blade('user.views.user.reset_password', $data);
            } else {

                if ($user->id != $this->input->post('user_id')) {
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error($this->lang->line('error_csrf'));
                } else {
                    $identity = $user->email;
                    //$get_username = $this->ion_auth_model->get_username_by_email($identity);
                    $change   = $this->ion_auth->reset_password($identity, $this->input->post('new_password'));
                    
                    if ($change == 1) {
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("login", "refresh");
                    } else {
                        $this->session->set_flashdata('message_error', $this->ion_auth->errors());
                        redirect('reset-password/' . $code);
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', 'Cant Reset Password');
            redirect("login",'refresh');
        }
    }
}