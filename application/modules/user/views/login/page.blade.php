
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>KLANA Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ base_url() }}assets/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/reset.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/slide-show.css" rel="stylesheet" type="text/css" />
    </head>
    <!-- END HEAD -->

    <body class="login">
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset">
                    <div id="container">
                        <ul id="slides">
                            <li class="slide">
                                <div class="slide-partial slide-left"><img src="{{base_url()}}assets/img/slide2-left.jpg"/></div>
                                <div class="slide-partial slide-right"><img  src="{{base_url()}}assets/img/slide2-right.jpg"/></div>
                                <!-- <h1 class="title"><span class="title-text">Bali</span></h1> -->
                            </li>
                            <li class="slide">
                                <div class="slide-partial slide-left"><img src="{{base_url()}}assets/img/slide1-left.jpg"/></div>
                                <div class="slide-partial slide-right"><img  src="{{base_url()}}assets/img/slide1-right.jpg"/></div>
                                <!-- <h1 class="title"><span class="title-text">Pulau Komodo</span></h1> -->
                            </li>
                            <li class="slide">
                                <div class="slide-partial slide-left"><img src="{{base_url()}}assets/img/slide3-left.jpg"/></div>
                                <div class="slide-partial slide-right"><img  src="{{base_url()}}assets/img/slide3-right.jpg"/></div>
                                <!-- <h1 class="title"><span class="title-text">Borobudur</span></h1> -->
                            </li>
                        </ul>
<!--                         <ul id="slide-select">
                            <li class="btn prev fa fa-arrow-left"></li>
                            <li class="selector"></li>
                            <li class="selector"></li>
                            <li class="selector"></li>
                            <li class="btn next fa fa-arrow-right"></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6 login-container bs-reset">
                    <img class="login-logo login-6" src="{{base_url()}}assets/img/klana-logo.png" style="width: 35%;margin-top:9%;left: 32%">
                    <div class="login-content" style="margin-top: 45%">

                        {{ form_open('login', array('autocomplete' => 'off', 'class' => 'login-form')) }}

                            @if (!empty($message))
                            <div class="alert alert-dismissable alert-success fade in" fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ $message }}
                            </div>
                            @endif 

                            @if (!empty($message_error))
                            <div class="alert alert-dismissable alert-danger fade in" fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ $message_error }}
                            </div>
                            @endif 

                            <div class="row">
                                <div class="col-xs-6">
                                   {{ form_input('email', set_value('email'), 'class="form-control form-control-solid placeholder-no-fix" placeholder="Email"') }}
                                </div>
                                <div class="col-xs-6">
                                    {{ form_password('password', '', 'class="form-control form-control-solid placeholder-no-fix" placeholder="Password"') }}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 text-right">

                                </div>
                                <div class="col-sm-6 text-right">
                                    <div class="forgot-password">
                                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                                    </div>
                                    <button class="btn blue" type="submit">Sign In</button>
                                </div>
                            </div>
                        {{ form_close() }}

                        <form class="forget-form" action="{{ base_url() . "forgot-password" }}" method="post">
                            <input type="hidden" name="{{ $csrftoken_name }}" value="{{ $csrftoken_value }}" />
                            <h3 class="font-green">Forgot Password ?</h3>
                            <p> Enter your e-mail address below to reset your password. </p>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn grey btn-default">Back</button>
                                <button type="submit" class="btn blue btn-success uppercase pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-12 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; KLANA 2017</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ base_url() }}assets/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/login-5.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/slide-show.js" type="text/javascript"></script>
    </body>
</html>