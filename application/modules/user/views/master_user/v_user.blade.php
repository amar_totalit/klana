@extends('default.views.layouts.default')

@section('title') 
KLANA - User Management
@stop

@section('body')

<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url() }}dashboard">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User Management</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> User Management</h3>
<!--     <div id="row-user" class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-user font-dark"></i>
                        <span class="caption-subject">Form User
                        </span>
                    </div>
                    <div class="tools">

                    </div>
                </div>
                <div id="form-wrapper" class="portlet-body">
                    <span class="text-danger">(*) required</span>
                    {{ form_open(null,array('id' => 'form-user', 'class' => 'form-horizontal')) }}
                        <div class="form-body">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">First Name <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        {{ form_input('first_name',set_value('first_name'),'id="first_name" class="form-control"') }}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Last Name <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        {{ form_input('last_name',set_value('last_name'),'id="last_name" class="form-control"') }}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Username <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        {{ form_input('username',set_value('username'),'id="username" class="form-control"') }}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        <input type="email" id="email" name="email" class="form-control">
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">User Level <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        <select id="user_level" name="user_level" class="form-control" data-live-search="true">               
                                            <option value="">- Select Level - </option>                                       
                                            <option value="1">Admin</option>
                                            <option value="2">Content Admin</option>
                                            <option value="3">Member</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group status_agent" style="display: none;">
                                    <label class="col-md-3 control-label">Status Agent <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                         <select id="is_agent" name="is_agent" class="form-control" data-live-search="true">
                                                                         
                                            <option value="">- Select Status Agent - </option>                                       
                                            <option value="t">Agent</option>
                                            <option value="f">Non Agent</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status User <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        <select id="is_aktif" name="is_aktif" class="form-control" data-live-search="true">               
                                            <option value="">- Select Status User - </option>                                       
                                            <option value="t" selected>Active</option>
                                            <option value="f">Inactive</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status Block <span class="text-danger bold">*</span></label>
                                    <div class="col-md-9">
                                        <select id="is_block" name="is_block" class="form-control" data-live-search="true">             
                                            <option value="">- Select Status Block - </option>                                       
                                            <option value="t">Block</option>
                                            <option value="f">Unblock</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-actions right">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-6">
                                    <button id="btn-cancel" type="button" class="btn default btn-sm"><i class="fa fa-remove"></i>{{lang('button_cancel')}}</button>
                                    <button id="btn-save" type="submit" class="btn green btn-sm"><i class="fa fa-save"></i>{{lang('button_insert')}}</button>
                                </div>
                            </div>
                        </div>

                        {{ form_input(array('id' => 'id','name' => 'id','type' => 'hidden')) }}
                        {{ form_input(array('id' => 'submit','name' => 'submit','type' => 'hidden')) }}
                    {{ form_close() }}
                </div>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-12">
            <div id="table-wrapper" class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <a href="{{$add}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>{{lang('button_add_new')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
                    @if($success != "")
                        <div class="alert alert-dismissable alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-check"></i> {{$success}}
                        </div>
                    @endif

                    @if($error != "")
                        <div class="alert alert-dismissable alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-times"></i> {{$error}}
                        </div>
                    @endif

                    <table id="table-user" class="table table-striped table-bordered table-hover dt-responsive" width="100%" >
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th width="100">Username</th>
                                <th width="100">Email</th>
                                <th width="100">User Level</th>
                                <th width="100">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var loadTable  = '{{$loadTable}}';
    var url_delete = '{{$delete}}';

    // Datatable
    var oTable = $('#table-user').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadTable,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
           {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_edit    = '<a href="{{$edit}}/'+ row[0] +'" type="button" class="btn btn-warning btn-icon-only btn-circle" title="Edit"><i class="fa fa-edit"></i></a>';
                    var btn_delete  = '<button onClick="deleteData(\'' + row[0] + '\',$(this))" class="btn btn-danger btn-icon-only btn-circle" title="Delete"><i class="fa fa-trash"></i></button>';
                    var render_html = btn_edit + ' ' + btn_delete;
                    return render_html;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Delete Data
    function deleteData(value, el) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Are You Sure ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-country'
                });

                window.location.href = url_delete + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
</script>
@stop