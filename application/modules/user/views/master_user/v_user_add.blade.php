@extends('default.views.layouts.default')

@section('title') KLANA - User Management @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">User Management</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Add</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> User Management </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form User Management</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-user', 'class' => 'form-horizontal')) }}

                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">First Name <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input('first_name',set_value('first_name'),'id="first_name" class="form-control"') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Last Name <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input('last_name',set_value('last_name'),'id="last_name" class="form-control"') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Username <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input('username',set_value('username'),'id="username" class="form-control"') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Email <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input('email',set_value('email'),'id="email" class="form-control"') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">User Level <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('user_level', $option_group, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Status User <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_dropdown('is_aktif', $option_status_user, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var check_username = '{{$check_username}}';
    var check_email    = '{{$check_email}}'; 

    // Pengaturan Form Validation 
    var form_validator = $("#form-user").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                first_name: {
                    required: true,
                    maxlength: 100
                },
                last_name: {
                    required: true,
                    maxlength: 100
                },
                username: {
                    required:true,
                    remote: {
                        type : 'GET',
                        url  : check_username,
                        data : {
                            username: function() {
                                return $('input[name="username"]').val();
                            }
                        }
                    }
                },
                email: {
                    required:true,
                    remote: {
                        type : 'GET',
                        url  : check_email,
                        data : {
                            email: function() {
                                return $('input[name="email"]').val();
                            }
                        }
                    }
                },
                user_level: {
                    required:true
                },
                is_aktif: {
                    required:true
                },
            },
        messages: {
            username : {
                remote: jQuery.validator.format("This username is already taken.")
            },
            email : {
                remote: jQuery.validator.format("This email is already taken.")
            },
        },
    });
</script>
@stop