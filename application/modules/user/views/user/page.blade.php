@extends('views.layouts.default')

@section('title') KLANA

@section('body')

<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Profil</span>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Profil </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-bars font-dark"></i>
                        <span class="caption-subject">Profil</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div id="form-wrapper" class="portlet-body">
                    <span class="text-danger">(*) Wajib</span>
                    {{ form_open(null,array('id' => 'form-user', 'class' => 'form-horizontal')) }}
                    {{ form_input(array('id' => 'id','name' => 'id','type' => 'hidden')) }}
                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Nama Lengkap <span class="text-danger bold">*</span></label>
                            <div class="col-md-3">
                                {{ form_input('first_name',set_value('first_name'),'id="first_name" class="form-control" placeholder="Nama Depan"') }}
                                <div class="form-control-focus"> </div>
                            </div>
                            <div class="col-md-3">
                                {{ form_input('last_name',set_value('last_name'),'id="last_name" class="form-control" placeholder="Nama Belakang"') }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Email <span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_input(array('type'=>'email','name'=>'email','value'=>set_value('email'),'id'=>'email','class'=>'form-control')) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Alamat <span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_textarea('address',set_value('address'),'id="address" class="form-control"') }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Provinsi <span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_dropdown('province', $province_options, '', $province_properties) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Kota <span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                <select name="city" class="form-control select2" id="city" style="width: 100%">
                                    <option value="">-- Choose City --</option>
                                    @if(!empty($cities))
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id_city }}" class="{{ $city->id_province }}">{{ $city->city_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">Telepon <span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_input('phone',set_value('phone'),'id="phone" class="form-control phone"') }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-6">
                                <a href="{{ base_url() }}fk-admin/change-password" class="btn default btn-sm"><i class="fa fa-lock"></i>{{lang('button_change_password')}}</a>
                                <button type="submit" class="btn blue btn-sm"><i class="fa fa-save"></i>{{lang('button_insert')}}</button>
                            </div>
                        </div>
                    </div>
                    {{ form_close() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">

    // Pengaturan awal halaman
    toastr.options = {"positionClass": "toast-top-right", };
    // Menampilkan data pada form
    function viewData(value)
    {
        App.blockUI({
            target: '#form-wrapper'
        });
        $.getJSON('{{base_url()}}fk-admin/profile/data', {id: value}, function (json, textStatus) {
            if (json.status == "success") {
                var row = json.data;
                var row_group = json.data_group;
                $('#id').val(row.id);
                $('#first_name').val(row.first_name);
                $('#last_name').val(row.last_name);
                $('#email').val(row.email);
                $('#address').val(row.address);
                $('#province').select2("val", row.province);
                $('#city').select2("val", row.city);
                $('#phone').val(row.phone);
            } else if (json.status == "error") {
                toastr.error('Data tidak ditemukan.', 'Notifikasi!');
            }
            App.unblockUI('#form-wrapper');
        });
    }

    // Pengaturan Form Validation
    var form_validator = $("#form-user").validate({
        errorPlacement: function (error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
            first_name: "required",
            last_name: "required",
            email: "required",
            address: "required",
            province: "required",
            city: "required",
            phone: "required",
        },
        messages: {
            first_name: "first name must be filled",
            last_name: "last name must be filled",
            email: "email must be filled",
            address: "address name must be filled",
            province: "province must be filled",
            city: "city must be filled",
            phone: "phone must be filled",
        },
        submitHandler: function (form) {
            App.blockUI({
                target: '#form-wrapper'
            });
            $(form).ajaxSubmit({
                beforeSubmit: showRequest,
                success: showResponse,
                url: '{{base_url()}}fk-admin/profile/save',
                type: 'POST',
                clearForm: true,
                resetForm: true,
            });
            function showRequest(formData, jqForm, options) {
                var queryString = $.param(formData);
                return true;
            }


            function showResponse(responseText, statusText, xhr, $form) {

                if (responseText.status == "success") {

                    toastr.success('{{lang("message_update_success")}}', 'Notifikasi!');
                } else if (responseText.status == "error") {

                    toastr.error('{{lang("message_update_faied")}}', 'Notifikasi!');
                } else if (responseText.status == "unique") {

                    toastr.error('{{lang("message_data_exist")}}', 'Notifikasi!');
                }

                App.unblockUI('#form-wrapper');
                setTimeout(function () {
                    window.location.reload()
                }, 1000);
            }

            return false;
        }
    });

</script>
<script>
    $(function () {
        $('#city').chained('#province');
        $(".select2").select2();
        $(".phone").inputmask("+6299999999999");
        viewData();
    });
</script>
@stop