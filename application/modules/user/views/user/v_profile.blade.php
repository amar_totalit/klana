@extends('default.views.layouts.default')

@section('title') KLANA - Profile @stop

@section('body')

<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."klana" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Profile</span>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">Profile </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-bars font-dark"></i>
                        <span class="caption-subject">Profile</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div id="form-wrapper" class="portlet-body">
                    @if($success != "")
                        <div class="alert alert-dismissable alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-check"></i> {{$success}}
                        </div>
                    @endif

                    @if($error != "")
                        <div class="alert alert-dismissable alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <i class="fa fa-times"></i> {{$error}}
                        </div>
                    @endif

                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#profile" data-toggle="tab">Profile</a>
                            </li>
                            <li>
                                <a href="#change-password" data-toggle="tab">Change Password</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <br>
                                {{ form_open($action,array('id' => 'form-profile', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) }}
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="portlet light bordered">
                                                    <input type="file" name="filename" id="filename" onchange="ValidateImage(this, 'cover');" style="display: none;">
                                                    <div align="center">
                                                        @if(!empty($user_db->photo))
                                                        <img src="{{ base_url() }}assets/upload/users/{{$user_db->photo}}" class="img-circle" id="image-preview" onclick="$('#filename').click();" width="270" height="270">
                                                        @endif

                                                        @if(empty($user_db->photo))
                                                        <img src="{{ base_url() }}assets/upload/users/no-image.jpg" class="img-circle" id="image-preview" onclick="$('#filename').click();" width="270" height="270">
                                                        @endif

                                                        <br>
                                                        <br>
                                                        <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#filename').click()">
                                                            <i class="fa fa-upload"></i> Upload Image
                                                        </button>
                                                    </div>   
                                                    <br>
                                                    <h4 class="profile-desc-title text-center">{{$user_db->first_name}} {{$user_db->last_name}}</h4>
                                                    <div class="margin-top-20 profile-desc-link">
                                                        <i class="fa fa-calendar"></i> Join Date:
                                                        <a href="#">{{ date('d F Y',strtotime($user_db->created_on)) }}</a>
                                                    </div>
                                                    <div class="margin-top-20 profile-desc-link">
                                                        <i class="fa fa-globe"></i> Email:
                                                        <a href="#">{{ $user_db->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="portlet light bordered">
                                                    <div>
                                                        <h4 class="profile-desc-title"></h4>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-actions text-right">
                                        <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                        <a href="{{ base_url()}}dashboard" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                                    </div>

                                    {{ form_input(array('id' => 'id','name' => 'id','type' => 'hidden', 'value' => encryptID($user_db->id))) }}
                                {{ form_close() }}
                            </div>
                            <div class="tab-pane" id="change-password">
                                <br>
                                {{ form_open(null,array('id' => 'form-user', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) }}

                                <div class="form-body col-md-offset-1">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Old Password <span style="color: red">*</span></label>
                                        <label class="control-label col-md-1 titikdua">:</label>
                                        <div class="col-md-4">
                                           {{ form_input(array('type'=>'password','name'=>'old_password','value'=>set_value('old_password'),'id'=>'old_password','class'=>'form-control')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body col-md-offset-1">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">New Password <span style="color: red">*</span></label>
                                        <label class="control-label col-md-1 titikdua">:</label>
                                        <div class="col-md-4">
                                            {{ form_input(array('type'=>'password','name'=>'new_password','value'=>set_value('new_password'),'id'=>'new_password','class'=>'form-control')) }}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body col-md-offset-1">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Reenter Password <span style="color: red">*</span></label>
                                        <label class="control-label col-md-1 titikdua">:</label>
                                        <div class="col-md-4">
                                            {{ form_input(array('type'=>'password','name'=>'retype_password','value'=>set_value('retype_password'),'id'=>'retype_password','class'=>'form-control')) }}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-actions text-right">
                                    <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                    <a href="{{ base_url()}}dashboard" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                                </div>

                                {{ form_input(array('id' => 'id','name' => 'id','type' => 'hidden')) }}
                                {{ form_close() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

    // Pengaturan awal halaman
    toastr.options = {"positionClass": "toast-top-right", };

    // Pengaturan Form Validation
    var form_validator = $("#form-user").validate({
        errorPlacement: function (error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
            old_password: "required",
            new_password: "required",
            retype_password: "required",
        },
        messages: {
            old_password: "old password must be filled",
            new_password: "new password must be filled",
            retype_password: "confirm password must be filled",
        },
        submitHandler: function (form) {
            App.blockUI({
                target: '#form-wrapper'
            });
            $(form).ajaxSubmit({
                beforeSubmit: showRequest,
                success: showResponse,
                url: 'change-password/save',
                type: 'POST',
                clearForm: true,
                resetForm: true,
            });
            function showRequest(formData, jqForm, options) {
                var queryString = $.param(formData);
                return true;
            }


            function showResponse(responseText, statusText, xhr, $form) {

                if (responseText.status == "success") {

                    toastr.success('{{lang("message_update_success")}}', 'Notifikasi!');
                } else if (responseText.status == "error") {

                    toastr.error('{{lang("message_update_failed")}}', 'Notifikasi!');
                } else if (responseText.status == "wrong_password") {

                    toastr.error('{{lang("message_wrong_password")}}', 'Notifikasi!');
                }

                App.unblockUI('#form-wrapper');
                setTimeout(function () {
                    window.location.reload()
                }, 1000);
            }

            return false;
        }
    });

</script>
@stop