@extends('default.views.layouts.default')

@section('title') Wahana - Profil @stop

@section('body')

<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."klana" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Ubah Password</span>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">Ubah Password </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-bars font-dark"></i>
                        <span class="caption-subject">Form Ubah Password</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div id="form-wrapper" class="portlet-body">
                    <span class="text-danger">(*) Wajib</span>
                    {{ form_open(null,array('id' => 'form-user', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) }}
                    {{ form_input(array('id' => 'id','name' => 'id','type' => 'hidden')) }}
                    <div class="form-body">
                        <div class="form-group form-md-line-input" id="password-pengguna">
                            <label class="col-md-2 control-label">Password Lama<span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_input(array('type'=>'password','name'=>'old_password','value'=>set_value('old_password'),'id'=>'old_password','class'=>'form-control')) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input" id="password-pengguna">
                            <label class="col-md-2 control-label">Password Baru<span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_input(array('type'=>'password','name'=>'new_password','value'=>set_value('new_password'),'id'=>'new_password','class'=>'form-control')) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input" id="password-pengguna">
                            <label class="col-md-2 control-label">Ulang Password<span class="text-danger bold">*</span></label>
                            <div class="col-md-6">
                                {{ form_input(array('type'=>'password','name'=>'retype_password','value'=>set_value('retype_password'),'id'=>'retype_password','class'=>'form-control')) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-6">
                                <a class="btn default btn-sm" href="{{ base_url() }}dashboard"><i class="fa fa-chevron-circle-left"></i>{{lang('button_back')}}</a>
                                <button type="submit" class="btn blue btn-sm"><i class="fa fa-save"></i>{{lang('button_insert')}}</button>
                            </div>
                        </div>
                    </div>
                    {{ form_close() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

    // Pengaturan awal halaman
    toastr.options = {"positionClass": "toast-top-right", };

    // Pengaturan Form Validation
    var form_validator = $("#form-user").validate({
        errorPlacement: function (error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
            old_password: "required",
            new_password: "required",
            retype_password: "required",
        },
        messages: {
            old_password: "old password must be filled",
            new_password: "new password must be filled",
            retype_password: "confirm password must be filled",
        },
        submitHandler: function (form) {
            App.blockUI({
                target: '#form-wrapper'
            });
            $(form).ajaxSubmit({
                beforeSubmit: showRequest,
                success: showResponse,
                url: 'change-password/save',
                type: 'POST',
                clearForm: true,
                resetForm: true,
            });
            function showRequest(formData, jqForm, options) {
                var queryString = $.param(formData);
                return true;
            }


            function showResponse(responseText, statusText, xhr, $form) {

                if (responseText.status == "success") {

                    toastr.success('{{lang("message_update_success")}}', 'Notifikasi!');
                } else if (responseText.status == "error") {

                    toastr.error('{{lang("message_update_failed")}}', 'Notifikasi!');
                } else if (responseText.status == "wrong_password") {

                    toastr.error('{{lang("message_wrong_password")}}', 'Notifikasi!');
                }

                App.unblockUI('#form-wrapper');
                setTimeout(function () {
                    window.location.reload()
                }, 1000);
            }

            return false;
        }
    });

</script>
@stop