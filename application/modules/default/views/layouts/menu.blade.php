<br>
<li class="nav-item {{ (uri_segment(1) == "dashboard") ? 'active open' : null }}">
    <a href="{{base_url()}}dashboard" class="nav-link ">
        <i class="fa fa-home"></i>
        <span class="title">Dashboard</span>
       @if(uri_segment(1) == "dashboard")
        <span class="selected"></span>
        @endif
    </a>
</li>
<li class="nav-item {{ (uri_segment(1) == "master") ? 'active open' : null }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-database"></i>
        <span class="title">Master</span>
        @if(uri_segment(1) == "master")
        <span class="selected"></span>
        <span class="arrow open"></span>
        @else
        <span class="arrow"></span>
        @endif
    </a>
    <ul class="sub-menu">
        <!-- <li class="nav-item {{ (uri_segment(2) == "country") ? 'active' : null }}">
            <a href="{{base_url()}}master/country" class="nav-link ">
                <span class="title">Country</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "province") ? 'active' : null }}">
            <a href="{{base_url()}}master/province" class="nav-link ">
                <span class="title">Province</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "city") ? 'active' : null }}">
            <a href="{{base_url()}}master/city" class="nav-link ">
                <span class="title">City</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "location") ? 'active' : null }}">
            <a href="{{base_url()}}master/location" class="nav-link ">
                <span class="title">Location</span>
            </a>
        </li> -->
        <li class="nav-item {{ (uri_segment(2) == "category") ? 'active' : null }}">
            <a href="{{base_url()}}master/category" class="nav-link ">
                <span class="title">Category</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "package") ? 'active' : null }}">
            <a href="{{base_url()}}master/package" class="nav-link ">
                <span class="title">Package</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "promotion") ? 'active' : null }}">
            <a href="{{base_url()}}master/promotion" class="nav-link ">
                <span class="title">Promotion</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "destination") ? 'active' : null }}">
            <a href="{{base_url()}}master/destination" class="nav-link ">
                <span class="title">Destination</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item {{ (uri_segment(1) == "member") ? 'active open' : null }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-users"></i>
        <span class="title">Member</span>
        @if(uri_segment(1) == "member")
        <span class="selected"></span>
        <span class="arrow open"></span>
        @else
        <span class="arrow"></span>
        @endif
    </a>
    <ul class="sub-menu">
        <li class="nav-item {{ (uri_segment(2) == "agent") ? 'active' : null }}">
            <a href="{{base_url()}}member/agent" class="nav-link ">
                <span class="title">Agent</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "tripper") ? 'active' : null }}">
            <a href="{{base_url()}}member/tripper" class="nav-link ">
                <span class="title">Tripper</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item {{ (uri_segment(1) == "content") ? 'active open' : null }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="icon-note"></i>
        <span class="title">Content</span>
        @if(uri_segment(1) == "content")
        <span class="selected"></span>
        <span class="arrow open"></span>
        @else
        <span class="arrow"></span>
        @endif
    </a>
    <ul class="sub-menu">
        <li class="nav-item {{ (uri_segment(2) == "contentList") ? 'active' : null }}">
            <a href="{{base_url()}}content/contentList" class="nav-link ">
                <span class="title">Content List</span>
            </a>
        </li>
        <li class="nav-item {{ (uri_segment(2) == "bookingList") ? 'active' : null }}">
            <a href="{{base_url()}}content/bookingList" class="nav-link ">
                <span class="title">Booking List</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item {{ (uri_segment(1) == "faq") ? 'active open' : null }}">
    <a href="{{ base_url()}}faq" class="nav-link ">
        <i class="fa fa-info-circle"></i>
        <span class="title">FAQ</span>
        @if(uri_segment(1) == "faq")
        <span class="selected"></span>
        @endif
    </a>
</li>
<li class="nav-item {{ (uri_segment(1) == "user") ? 'active open' : null }}">
    <a href="{{ base_url()}}user/users_master" class="nav-link ">
        <i class="fa fa-user"></i>
        <span class="title">User Management</span>
        @if(uri_segment(1) == "user")
        <span class="selected"></span>
        @endif
    </a>
</li>
<li class="nav-item {{ (uri_segment(1) == "log") ? 'active open' : null }}">
    <a href="{{base_url()}}log" class="nav-link ">
        <i class="fa fa-clock-o"></i>
        <span class="title">Riwayat</span>
        @if(uri_segment(1) == "log")
        <span class="selected"></span>
        @endif
    </a>
</li>