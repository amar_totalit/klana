<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@section('title') @show</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

       <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="{{ base_url() }}assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

        <link href="{{ base_url() }}assets/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.core.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-toastr/toastr.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/confirm/css/jquery-confirm.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/select2/css/select2-bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/easy-autocomplete/easy-autocomplete.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/tipso/src/tipso.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ base_url() }}assets/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.core.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.arrow.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.autocomplete.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.clear.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.focus.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.prompt.css" rel="stylesheet" type="text/css">
        <link href="{{ base_url() }}assets/plugins/jquery-textext/src/css/textext.plugin.tags.css" rel="stylesheet" type="text/css">


        <link href="{{ base_url() }}assets/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/themes/light2.css" rel="stylesheet" type="text/css" id="style_color" />
        <!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{ base_url() }}assets/plugins/elfinder/css/elfinder.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/plugins/elfinder/css/theme.css" rel="stylesheet" type="text/css" />
        <link href="{{ base_url() }}assets/css/custom.css" rel="stylesheet" type="text/css" />

        <link rel="icon" href="{{ base_url() }}assets/img/icon-klana.png" />
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md" id="body">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <!--<a href="index.html">
                        <img src="{{ base_url() }}assets/img/logo.png" alt="logo" class="logo-default" /> </a>-->
                    <h4 class="logo-default font-white pull-left">K L A N A</h4>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(empty($user_db->photo))
                                <img alt="" class="img-circle" src="{{ base_url() }}assets/img/avatar.png" />
                                @endif

                                @if(!empty($user_db->photo))
                                <img alt="" class="img-circle" src="{{ base_url() }}assets/upload/users/{{$user_db->photo}}" width="30" height="30" />
                                @endif

                                <span class="username username-hide-on-mobile"> 
                                    {{ ucfirst($user_db->first_name) }} {{ucfirst($user_db->last_name)}}
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ base_url() }}profile">
                                        <i class="fa fa-gear"></i> Profile </a>
                                </li>
                                <li>
                                    <a href="{{ base_url() }}logout">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->

                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        @include('default.views.layouts.menu')
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                @yield('body')
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; K L A N A
                <a href="#" title="" target="_blank"></a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>

        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="{{ base_url() }}assets/plugins/respond.min.js"></script>
        <script src="{{ base_url() }}assets/plugins/excanvas.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ base_url() }}assets/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/moment.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ base_url() }}assets/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/datatables/fnPagingInfo.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/datatables/jquery.dataTables.delay.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ base_url() }}assets/plugins/elfinder/js/elfinder.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/app.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ base_url() }}assets/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ base_url() }}assets/scripts/layout.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/demo.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/scripts/knockout-3.4.0.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/form/jquery.form.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/confirm/js/jquery-confirm.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/currency/currency.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-mask/jquery.mask.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery.chained.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery.chained.remote.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/easy-autocomplete/jquery.easy-autocomplete.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/select2/js/select2.full.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/input-mask/inputmask.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/tipso/src/tipso.min.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/widget/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/widgetselection/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/lineutils/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/uploadimage/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/uploadwidget/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/filetools/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/notificationaggregator/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/html5video/dialogs/html5video.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/ckeditor/plugins/html5video/plugin.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.core.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.ajax.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.arrow.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.autocomplete.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.clear.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.filter.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.focus.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.prompt.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.suggestions.js" type="text/javascript"></script>
        <script src="{{ base_url() }}assets/plugins/jquery-textext/src/js/textext.plugin.tags.js" type="text/javascript"></script>

        <script src="{{ base_url() }}assets/scripts/general-plugins.js" type="text/javascript"></script>
        
        <!-- END THEME LAYOUT SCRIPTS -->
        @section('scripts') @show

        <script type="text/javascript">
                $('select').select2({
                theme: "bootstrap",
                width: "100%"
            });
        </script>

        <!-- For Validate Image -->
        <script type="text/javascript">
            // Untuk Gambar
            var _validFileExtensions = [".jpg", ".png"];
            var image_array = [];
            function ValidateImage(oInput, type, i) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                readURL(oInput, type, i);
                                break;
                            }
                        }
                        if (!blnValid) {
                            toastr.error('File Format Not Allowed', 'Notifikasi!');
                            $('#image-preview').attr('src', "{{base_url()}}assets/img/no_images.png");
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function readURL(input, type, i) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        if (i != undefined) {
                            $('#image_' + i + '-preview').attr('src', e.target.result);
                            image_array.push(type + "_" + i);
                            $("input[name='image_update_" + i + "']").val("yes");
                            $("input[name='delete_" + type + "_"+ i +"']").val("no");
                        } else {
                            $('#image-preview').attr('src', e.target.result);
                        }
                    };
                    
                    if(type == 'cover'){
                        $("#image_exist").val("yes");
                        $("#btn-remove").show();

                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            
            function clearImage(type, i) {
                if (i == undefined) {
                    $('#image-preview').attr('src', "{{base_url()}}assets/img/no_images.png");
                    $("#partner_image").val("");
                    $("#image_exist").val("");
                    $("#btn-remove").hide();
                } else {
                    $('#image_' + i + '-preview').attr('src', "{{base_url()}}assets/img/no_image.png");
                    image_array = jQuery.grep(image_array, function (value) {
                        return value != type + "_" + i;
                    });
                    if (image_array.length == 0) {
                        if(type == 'cover'){
                            $("input[name='image_exist']").val(""); 
                        }
                    }
                    $("#"+type+"_"+i).val("");
                    $("input[name='delete_"+type+"_"+i+"']").val("yes");
                    $("input[name='image_update_"+i+"']").val("no");
                }
            }
        </script>

        <!-- setting Date Picker -->
        <script type="text/javascript">
            $('#firstDate').datetimepicker({
                format : 'DD MMM YYYY',
                ignoreReadonly: true,
                disabledDates: [
                        ]
              });
            // if ($('#firstDateVal').data('target') != '#firstDate') {
            //     var fd = $('#firstDateVal').data('target');
            // }else{
            //     var fd = '01 Jan 2018';
            // }
            $('#secondDate').datetimepicker({
                format : 'DD MMM YYYY',
                ignoreReadonly: true,
                // minDate : fd,
                disabledDates: [
                        ]
              });
        </script>
    </body>

</html>