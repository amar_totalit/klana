<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Klana | 403 Forbidden</title>
	<link href="{{base_url()}}assets/css/404/style.css" rel='stylesheet' type='text/css'/>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href="//fonts.googleapis.com/css?family=Ravi+Prakash" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Custom Error Page Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
<body>
	<div class="top-bar-agile">
		<div class="logo-agileits">
			<a href="{{base_url()}}"><img src="{{base_url()}}assets/img/404/logo.png" alt=" " /></a>
		</div>
		<div class="nav-agileinfo">
		</div>
		<div class="clear"></div>
	</div>
	<div class="content-w3">
		<h1>403</h1>
		<h2>This page is forbidden! </h2>
		<p>The page you are looking for has unavailable right now.</p>
	</div>
</body>