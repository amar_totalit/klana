<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activities extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        $this->load->helper('indonesian_date');
    }

    public function index() {

        $this->load_view('log','log','list_group');
    }

    public function fetch_data_group() {
        $database_columns = array(
            'logs.id_logs',
            'logs.type',
            '(CASE logs.type
                WHEN "1" THEN "Users Management"
                WHEN "2" THEN "Country"
                WHEN "3" THEN "Province"
                WHEN "4" THEN "City"
                WHEN "5" THEN "Destination"
                WHEN "6" THEN "Content Category"
                WHEN "7" THEN "Package"
                WHEN "8" THEN "Promotion"
                WHEN "9" THEN "FAQ"
                WHEN "10" THEN "Agent"
                WHEN "11" THEN "Content"
                ELSE "-"
             END) AS type_caption',
            'MAX(logs.created_on) as created_on'
        );

        $from = "logs";


        $group_by = 'logs.type';

        $order_by = "MAX(logs.created_on) desc";


        $where = '';

        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where = "(CASE logs.type
                WHEN '1' THEN 'Users Management'
                WHEN '2' THEN 'Country'
                WHEN '3' THEN 'Province'
                WHEN '4' THEN 'City'
                WHEN '5' THEN 'Destination'
                WHEN '6' THEN 'Content Category'
                WHEN '7' THEN 'Package'
                WHEN '8' THEN 'Promotion'
                WHEN '9' THEN 'FAQ'
                WHEN '10' THEN 'Agent'
                WHEN '11' THEN 'Content'
                ELSE '-'
             END) LIKE '%" . $sSearch . "%'";
            $where .= "OR DATE_FORMAT(logs.created_on,'%d %M %Y') LIKE '%" . $sSearch . "%'";
        }

        $this->datatables->set_index('logs.id_logs');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('group_by', $group_by);
        $this->datatables->config('order_by', $order_by);
        $selected_data = $this->datatables->get_select_data();
        $aa_data = $selected_data['aaData'];
        $new_aa_data = array();
        foreach ($aa_data as $row) {
            $row_value = array();
            $row_value[] = $row->id_logs;
            $row_value[] = $row->type_caption;
            $row_value[] = indonesian_format($row->created_on) . ' ' .date('H:i:s', strtotime($row->created_on));
            $row_value[] = $row->type;
            $new_aa_data[] = $row_value;
        }
        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    // List Setelah Grup
    public function lists($type) {
        // Wajib Ada
        $user = $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->row()->name;
        //
        if ($type == 1) {
            $title = 'Users Management';
        } elseif ($type == 2 ) {
            $title = 'Country';
        }elseif ($type == 3) {
            $title = 'Province';
        }elseif ($type == 4) {
            $title = 'City';
        }elseif ($type == 5) {
            $title = 'Destination';
        }elseif ($type == 6) {
            $title = 'Content Category';
        } elseif ($type == 7) {
            $title = 'Package';
        } elseif ($type == 8) {
            $title = 'Promotion';
        } elseif ($type == 9) {
            $title = 'FAQ';
        } elseif ($type == 10) {
            $title = 'Agent';
        } elseif ($type == 11) {
            $title = 'Content';
        } else {
            $title = '-';
        }

        //$data['group'] = $this->ion_auth->in_group(1);
        $data = array(
            'title'       => $title,
            'type'        => $type,
            'group'       => $user_groups
        );

        $this->load_view('log','log','list',$data);
    }

    public function fetch_data($type) {
        $database_columns = array(
            'logs.id_logs',
            'concat(users.first_name, " ", users.last_name) As full_name',
            'groups.description',
            '(CASE logs.activity
                WHEN "C" THEN "Create"
                WHEN "D" THEN "Delete"
                WHEN "U" THEN "Update"
                ELSE "-"
             END) AS activity',
            'logs.message',
            'logs.created_on',
        );

        $from = "logs";

        $join[] = array('users', 'users.id = logs.id_user', 'left');
        $join[] = array('users_groups', 'users_groups.user_id = users.id', 'left');
        $join[] = array('groups', 'groups.id = users_groups.group_id', 'left');

        $where = "type = '" . $type . "' ";

        $order_by = "logs.created_on desc";

        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= "And (CASE logs.activity
                WHEN 'C' THEN 'Create'
                WHEN 'U' THEN 'Delete'
                WHEN 'D' THEN 'Update'
                ELSE '-'
             END) LIKE '%" . $sSearch . "%'";
            $where .= "OR concat(users.first_name, ' ', users.last_name) LIKE '%" . $sSearch . "%'";
            $where .= "OR groups.description LIKE '%" . $sSearch . "%'";
            $where .= "OR logs.message LIKE '%" . $sSearch . "%'";
            $where .= "OR DATE_FORMAT(logs.created_on,'%d %M %Y') LIKE '%" . $sSearch . "%'";
        }

        $this->datatables->set_index('logs.id_logs');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('join', $join);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);
        $selected_data = $this->datatables->get_select_data();
        $aa_data = $selected_data['aaData'];
        $new_aa_data = array();
        foreach ($aa_data as $row) {
            $row_value = array();
            $row_value[] = $row->id_logs;
            $row_value[] = $row->full_name;
            $row_value[] = $row->description;
            if ($row->activity == 'Create') {
                $aktivitas = '<span class="label label-success">Create</span>';
            } else if ($row->activity == 'Update') {
                $aktivitas = '<span class="label label-warning">Update</span>';
            } else {
                $aktivitas = '<span class="label label-danger">Delete</span>';
            }
            $row_value[] = $aktivitas;
            $row_value[] = $row->message;
            $row_value[] = indonesian_format($row->created_on) . ' ' .date('H:i:s', strtotime($row->created_on));
            $row_value[] = $row->id_logs;
            $new_aa_data[] = $row_value;
        }
        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    function detail($id) {
        // Wajib Ada
        $user = $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->row()->name;
        $data['status_user'] = $user_groups;
        //$data['group'] = $this->ion_auth->in_group(2);
       
        //
        $user = $this->ion_auth->user()->row();
        $log = $this->activity_log->detail_log($id);
        if ($log->activity == 'C') {
            $activity = "Create";
        } else if ($log->activity == 'U') {
            $activity = "Update";
        } else {
            $activity = "Delete";
        }

        if ($log->type == 1) {
            $title = 'Users Management';
            $type = $log->type;
        } elseif ($log->type == 2 ) {
            $title = 'Country';
            $type = $log->type;
        }elseif ($log->type == 3) {
            $title = 'Province';
            $type = $log->type;
        }elseif ($log->type == 4) {
            $title = 'City';
            $type = $log->type;
        }elseif ($log->type == 5) {
            $title = 'Destination';
            $type = $log->type;
        }elseif ($log->type == 6) {
            $title = 'Content Category';
            $type = $log->type;
        }elseif ($log->type == 7) {
            $title = 'Package';
            $type = $log->type;
        }elseif ($log->type == 8) {
            $title = 'Promotion';
            $type = $log->type;
        }elseif ($log->type == 9) {
            $title = 'FAQ';
            $type = $log->type;
        }elseif ($log->type == 10) {
            $title = 'Agent';
            $type = $log->type;
        }elseif ($log->type == 11) {
            $title = 'Content';
            $type = $log->type;
        }else{
            $title = '-';
            $type = '';
        }

        //$data['group'] = $this->ion_auth->in_group(1);
        $data = array(
            'title'       => $title,
            'type'        => $type,
            'log'         => $log,
            'activity'    => $activity,
            'group'       => $user_groups
        );
        $this->load_view('log','log','detail',$data);
    }

}
