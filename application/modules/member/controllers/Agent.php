<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['approve']          = site_url() . $this->site . '/approve';
        $data['reject']           = site_url() . $this->site . '/reject';
        $data['agentDetail']      = site_url() . $this->site . '/agentDetail';
        $data['loadTablePending'] = site_url() . $this->site . '/loadTablePending';
        $data['loadTableApprove'] = site_url() . $this->site . '/loadTableApprove';
        $data['loadTableReject']  = site_url() . $this->site . '/loadTableReject';

		$this->load_view('member','agent','v_agent',$data);
	}

	/**
    * Serverside load table:users
    * Get agent status pending
    * @return ajax
    **/
	public function loadTablePending()
	{
		$database_columns = array('id','code_agent','first_name','last_name','created_on','id');

        $from  = "users";
        $where = "users.is_delete = 'F' AND users.code_agent IS NOT NULL AND users.is_approve_agent = 'P'";

        $order_by = 'code_agent desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (code_agent LIKE '%" . $sSearch . "%'";
            $where .= " OR first_name LIKE '%" . $sSearch . "%'";
            $where .= " OR last_name LIKE '%" . $sSearch . "%'";
            $where .= " OR DATE_FORMAT(created_on, '%d %M %Y') LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id;
			$row_value[]   = $row->code_agent;
            $row_value[]   = $row->first_name . ' ' . $row->last_name;
            $row_value[]   = date('d F Y',strtotime($row->created_on));
			$row_value[]   = encryptID($row->id);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

    /**
    * Serverside load table:users
    * Get agent status approve
    * @return ajax
    **/
    public function loadTableApprove()
    {
        $database_columns = array('id','code_agent','first_name','last_name','created_on','id');

        $from  = "users";
        $where = "users.is_delete = 'F' AND users.code_agent IS NOT NULL AND users.is_approve_agent = 'A'";

        $order_by = 'code_agent desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (code_agent LIKE '%" . $sSearch . "%'";
            $where .= " OR first_name LIKE '%" . $sSearch . "%'";
            $where .= " OR last_name LIKE '%" . $sSearch . "%'";
            $where .= " OR DATE_FORMAT(created_on, '%d %M %Y') LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id;
            $row_value[]   = $row->code_agent;
            $row_value[]   = $row->first_name . ' ' . $row->last_name;
            $row_value[]   = date('d F Y',strtotime($row->created_on));
            $row_value[]   = encryptID($row->id);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    /**
    * Serverside load table:users
    * Get agent status reject
    * @return ajax
    **/
    public function loadTableReject()
    {
        $database_columns = array('id','code_agent','first_name','last_name','created_on','id');

        $from  = "users";
        $where = "users.is_delete = 'F' AND users.code_agent IS NOT NULL AND users.is_approve_agent = 'R'";

        $order_by = 'code_agent desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (code_agent LIKE '%" . $sSearch . "%'";
            $where .= " OR first_name LIKE '%" . $sSearch . "%'";
            $where .= " OR last_name LIKE '%" . $sSearch . "%'";
            $where .= " OR DATE_FORMAT(created_on, '%d %M %Y') LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id;
            $row_value[]   = $row->code_agent;
            $row_value[]   = $row->first_name . ' ' . $row->last_name;
            $row_value[]   = date('d F Y',strtotime($row->created_on));
            $row_value[]   = encryptID($row->id);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }

    /**
    * Approve Agent data from table:users
    * @param Id
    * @return page index
    **/
    function approve($id)
    {
		$user       = $this->ion_auth->user()->row();
		$id_user    = decryptID($id);
		
		$model      = User::where('id',$id_user)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_approve_agent   = 'A';
            $model->date_approve_agent = date('Y-m-d H:i:s');
            $model->approve_agent_by   = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
            	$data_notif = array(
                    "Code Agent"   => $model->code_agent,
                    "Agent Name"   => $model->first_name . ' ' . $model->last_name,
                    "Approve Date" => date('d F Y H:i:s'),
                    "Approve by"   => $user->first_name . ' ' . $user->last_name,
                );

                $message = "Agent " . $model->first_name . ' ' . $model->last_name . " is successfuly approved by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id,json_encode($data_notif), NULL, NULL, $message, 'C', 10);

            	$this->session->set_flashdata('success', lang("message_approve_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_approve_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }

    /**
    * Reject Agent data from table:users
    * @param Id
    * @return page index
    **/
    function reject($id)
    {
        $user       = $this->ion_auth->user()->row();
        $id_user    = decryptID($id);
        
        $model      = User::where('id',$id_user)->first();

        if(!empty($model)){

            /* Initialize Data */
            $model->is_approve_agent   = 'R';
            $model->date_approve_agent = date('Y-m-d H:i:s');
            $model->approve_agent_by   = $user->id;

            /* Update */
            $update = $model->save();

            if($update){

                /* Write Log */
                $data_notif   = array(
                    "Code Agent"  => $model->code_agent,
                    "Agent Name"  => $model->first_name . ' ' . $model->last_name,
                    "Reject Date" => date('d F Y H:i:s'),
                    "Reject by"   => $user->first_name . ' ' . $user->last_name,
                );

                $message = "Agent " . $model->first_name . ' ' . $model->last_name . " is successfuly rejected by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id,json_encode($data_notif), NULL, NULL, $message, 'C', 10);

                $this->session->set_flashdata('success', lang("message_reject_success"));
                redirect(site_url() . $this->site);
            }else{
                $this->session->set_flashdata('error', lang("message_reject_failed"));
                redirect(site_url() . $this->site);
            }
        }else{
            $this->session->set_flashdata('error', lang("message_data_not_found"));
            redirect(site_url() . $this->site);
        }
    }

    /**
    * Get data agent table:users
    * @return ajax
    **/
    function agentDetail(){
        $id_user   = decryptID($this->input->get('id_user'));

        $data_user = User::selectRaw("users.* , CONCAT(agent.first_name,' ',agent.last_name) AS approve_agent_by_name")->leftjoin('users as agent','agent.id','=','users.approve_agent_by')->where('users.id',$id_user)->first();

        echo json_encode($data_user); 
    }
}