<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tripper extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['loadTable']     = site_url() . $this->site . '/loadTable';
        $data['tripperDetail'] = site_url() . $this->site . '/tripperDetail';

		$this->load_view('member','tripper','v_tripper',$data);
	}

    /**
    * Serverside load table:users
    * Get Tripper
    * @return ajax
    **/
    public function loadTable()
    {
        $database_columns = array('id','code_tripper','first_name','last_name','created_on','id');

        $from  = "users";
        $where = "users.is_delete = 'F' AND users.code_tripper IS NOT NULL ";

        $order_by = 'code_tripper desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (code_tripper LIKE '%" . $sSearch . "%'";
            $where .= " OR first_name LIKE '%" . $sSearch . "%'";
            $where .= " OR last_name LIKE '%" . $sSearch . "%'";
            $where .= " OR DATE_FORMAT(created_on, '%d %M %Y') LIKE '%" . $sSearch . "%')";
        }
        
        $iSortCol_0   = $this->input->get('iSortCol_0', true);
        $iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

        $selected_data = $this->datatables->get_select_data();
        $aa_data       = $selected_data['aaData'];
        $new_aa_data   = array();

        foreach ($aa_data as $row) {
            
            $row_value     = array();
            $row_value[]   = $row->id;
            $row_value[]   = $row->code_tripper;
            $row_value[]   = $row->first_name . ' ' . $row->last_name;
            $row_value[]   = date('d F Y',strtotime($row->created_on));
            $row_value[]   = encryptID($row->id);
            $new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
    }


    /**
    * Get data Tripper table:users
    * @return ajax
    **/
    function tripperDetail(){
        $id_user   = decryptID($this->input->get('id_user'));

        $data_user = User::where('users.id',$id_user)->first();

        echo json_encode($data_user); 
    }

}