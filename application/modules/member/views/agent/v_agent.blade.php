@extends('default.views.layouts.default')

@section('title') KLANA - Agent @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Member</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Agent</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Agent </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">

                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        @if($success != "")
                            <div class="alert alert-dismissable alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-check"></i> {{$success}}
                            </div>
                        @endif

                        @if($error != "")
                            <div class="alert alert-dismissable alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <i class="fa fa-times"></i> {{$error}}
                            </div>
                        @endif


                        <div class="tabbable tabbable-tabdrop">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#pending" data-toggle="tab">Pending</a>
                                </li>
                                <li>
                                    <a href="#approved" data-toggle="tab">Approved</a>
                                </li>
                                <li>
                                    <a href="#rejected" data-toggle="tab">Rejected</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="pending">
                                    <table id="table-agent-pending" class="table table-striped table-bordered table-hover dt-responsive" width="100%" >
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th width="10">No</th>
                                                <th width="100">Code Agent</th>
                                                <th width="100">Agent Name</th>
                                                <th width="100">Registration Date</th>
                                                <th width="100">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="tab-pane" id="approved">
                                    <table id="table-agent-approve" class="table table-striped table-bordered table-hover dt-responsive" width="100%" >
                                        <thead>
                                            <tr>
                                                <th width="1"></th>
                                                <th width="10">No</th>
                                                <th width="100">Code Agent</th>
                                                <th width="100">Agent Name</th>
                                                <th width="100">Registration Date</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="tab-pane " id="rejected">
                                    <table id="table-agent-reject" class="table table-striped table-bordered table-hover dt-responsive" width="100%" >
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th width="10">No</th>
                                                <th width="100">Code Agent</th>
                                                <th width="100">Agent Name</th>
                                                <th width="100">Registration Date</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    var url_approve      = '{{$approve}}';
    var url_reject       = '{{$reject}}';
    var agentDetail      = '{{$agentDetail}}';
    var loadTablePending = '{{$loadTablePending}}';
    var loadTableApprove = '{{$loadTableApprove}}';
    var loadTableReject  = '{{$loadTableReject}}';

    // Datatable Pending
    var table_pending = $('#table-agent-pending').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadTablePending,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
            {
                "render": function(data, type, row) {
                    return '<input type="hidden" id="id" value="'+ row[4] +'">';
                },
                "visible": true,
                "class": 'text-center expanding',
                "orderable": false,
                "width": '5px'
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
            {
                "render": function(data, type, row) {
                    var btn_approve = '<button onClick="approveData(\'' + row[4] + '\',$(this),\''+ row[2] +'\')" class="btn green-meadow btn-icon-only btn-circle" title="Approve"><i class="fa fa-check"></i></button>';
                    var btn_reject = '<button onClick="rejectData(\'' + row[4] + '\',$(this),\''+ row[2] +'\')" class="btn btn-danger btn-icon-only btn-circle" title="Reject"><i class="fa fa-close"></i></button>';
                    return btn_approve + btn_reject;
                }, 
                "sClass": "text-center", "orderable": false
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(1)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    /* expand table agent pending */
    $('#table-agent-pending').on('click', 'tbody tr td.expanding', function () {
        var tr      = $(this).closest('tr');
        var td      = tr.children().first();
        var id      = tr.find('#id').val();
        var details = tr.next('tr.details');

       if (details.length == 0) {
            var header = function() {
                var format = '<div class="col-md-12">';

                $.ajax({
                    type    : 'GET',
                    url     : agentDetail,
                    data    : 'id_user=' + id,
                    cache   : false,
                    async   : false,
                    global  : false,
                    success : function(result) {
                        var oObj = JSON.parse(result);

                        if (Object.keys(oObj).length > 0) {
                            /* condition status users */
                            if(oObj.active == '1'){
                                var status = '<span class="label label-success">Active</span>';
                            }else{
                                var status = '<span class="label label-danger">Not Active</span>';
                            }

                            /* condition image */
                            if(oObj.photo == '' || oObj.photo == null){
                                var photo = 'no-image.jpg';
                            }else{
                                var photo = oObj.photo;
                            }

                            format += '<div class="row">';

                            format += 
                                '<div class="col-md-3">' +
                                    '<img alt="" class="img-circle" src="{{ base_url() }}assets/upload/users/'+ photo +'" width="250" height="250" style="margin-top:20px">'+
                                '</div>';

                            format +=
                                '<div class="col-md-9">' +
                                '   <table style="padding-left:50px;width:100%" class="table table-striped">' +
                                '       <tr>' +
                                '           <td style="width:20%">Code Agent</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.code_agent + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Agent Name</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.first_name + ' ' + oObj.last_name + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Username</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.username + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Email</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.email + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Registration Date</td>' +
                                '           <td style="width:2%">:</td>' +
                                '       <td>' + formattedDateddmmyyyyDash(oObj.created_on) + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Status</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>'+ status +'</td>' +
                                '       </tr>' +
                                '   </table>'+
                                '</div>';

                            format += '</div>';

                        } else {
                            format += '<div class="alert alert-info text-center"><strong>No Data</strong></div>';
                        }

                    }
                })
                format += '</div>';

                return format;
            }();

            table_pending.fnOpen(tr, header, 'details');
            td.addClass('shown');
       } else {
            if ($(this).hasClass('shown')) {
                details.hide();
                td.removeClass('shown');
            } else {
                details.show();
                td.addClass('shown');
            }
        }
    });

    // Datatable Approve
    var table_approve = $('#table-agent-approve').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadTableApprove,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
            {   
                "render": function(data, type, row) {
                    return '<input type="hidden" id="id" value="'+ row[4] +'">';
                },
                "visible": true,
                "class": 'text-center expanding',
                "orderable": false,
                "width": '5px'
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(1)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    /* expand table agent approve*/
    $('#table-agent-approve').on('click', 'tbody tr td.expanding', function () {
        var tr      = $(this).closest('tr');
        var td      = tr.children().first();
        var id      = tr.find('#id').val();
        var details = tr.next('tr.details');

       if (details.length == 0) {
            var header = function() {
                var format = '<div class="col-md-12">';

                $.ajax({
                        type    : 'GET',
                        url     : agentDetail,
                        data    : 'id_user=' + id,
                        cache   : false,
                        async   : false,
                        global  : false,
                        success : function(result) {
                            var oObj = JSON.parse(result);

                            if (Object.keys(oObj).length > 0) {
                                /* condition status users */
                                if(oObj.active == '1'){
                                    var status = '<span class="label label-success">Active</span>';
                                }else{
                                    var status = '<span class="label label-danger">Not Active</span>';
                                }

                                /* condition image */
                                if(oObj.photo == '' || oObj.photo == null){
                                    var photo = 'no-image.jpg';
                                }else{
                                    var photo = oObj.photo;
                                }

                                format += '<div class="row">';

                                format += 
                                    '<div class="col-md-3">' +
                                        '<img alt="" class="img-circle" src="{{ base_url() }}assets/upload/users/'+ photo +'" width="250" height="250" style="margin-top:20px">'+
                                    '</div>';

                                format +=
                                    '<div class="col-md-9">' +
                                    '   <table style="padding-left:50px;width:100%" class="table table-striped">' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Code Agent</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>' + oObj.code_agent + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Agent Name</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>' + oObj.first_name + ' ' + oObj.last_name + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Username</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>' + oObj.username + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Email</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>' + oObj.email + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Registration Date</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '       <td>' + formattedDateddmmyyyyDash(oObj.created_on) + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Approve Date</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>' + formattedDateddmmyyyyDash(oObj.date_approve_agent) + '</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Approve By</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>'+ oObj.approve_agent_by_name +'</td>' +
                                    '       </tr>' +
                                    '       <tr>' +
                                    '           <td style="width:20%">Status</td>' +
                                    '           <td style="width:2%">:</td>' +
                                    '           <td>'+ status +'</td>' +
                                    '       </tr>' +
                                    '   </table>'+
                                    '</div>';

                                format += '</div>';

                            } else {
                                format += '<div class="alert alert-info text-center"><strong>No Data</strong></div>';
                            }

                        }
                    })
                    format += '</div>';

                    return format;
                }();

            table_approve.fnOpen(tr, header, 'details');
            td.addClass('shown');
       } else {
            if ($(this).hasClass('shown')) {
                details.hide();
                td.removeClass('shown');
            } else {
                details.show();
                td.addClass('shown');
            }
        }
    });

    /* expand table agent reject*/
    $('#table-agent-reject').on('click', 'tbody tr td.expanding', function () {
        var tr      = $(this).closest('tr');
        var td      = tr.children().first();
        var id      = tr.find('#id').val();
        var details = tr.next('tr.details');

       if (details.length == 0) {
            var header = function() {
               var format = '<div class="col-md-12">';

                $.ajax({
                    type    : 'GET',
                    url     : agentDetail,
                    data    : 'id_user=' + id,
                    cache   : false,
                    async   : false,
                    global  : false,
                    success : function(result) {
                        var oObj = JSON.parse(result);

                        if (Object.keys(oObj).length > 0) {
                            /* condition status users */
                            if(oObj.active == '1'){
                                var status = '<span class="label label-success">Active</span>';
                            }else{
                                var status = '<span class="label label-danger">Not Active</span>';
                            }

                            /* condition image */
                            if(oObj.photo == '' || oObj.photo == null){
                                var photo = 'no-image.jpg';
                            }else{
                                var photo = oObj.photo;
                            }

                            format += '<div class="row">';

                            format += 
                                '<div class="col-md-3">' +
                                    '<img alt="" class="img-circle" src="{{ base_url() }}assets/upload/users/'+ photo +'" width="250" height="250" style="margin-top:20px">'+
                                '</div>';

                            format +=
                                '<div class="col-md-9">' +
                                '   <table style="padding-left:50px;width:100%" class="table table-striped">' +
                                '       <tr>' +
                                '           <td style="width:20%">Code Agent</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.code_agent + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Agent Name</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.first_name + ' ' + oObj.last_name + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Username</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.username + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Email</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + oObj.email + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Registration Date</td>' +
                                '           <td style="width:2%">:</td>' +
                                '       <td>' + formattedDateddmmyyyyDash(oObj.created_on) + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Reject Date</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>' + formattedDateddmmyyyyDash(oObj.date_approve_agent) + '</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Reject By</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>'+ oObj.approve_agent_by_name +'</td>' +
                                '       </tr>' +
                                '       <tr>' +
                                '           <td style="width:20%">Status</td>' +
                                '           <td style="width:2%">:</td>' +
                                '           <td>'+ status +'</td>' +
                                '       </tr>' +
                                '   </table>'+
                                '</div>';

                            format += '</div>';

                        } else {
                            format += '<div class="alert alert-info text-center"><strong>No Data</strong></div>';
                        }

                    }
                })
                format += '</div>';

                return format;
            }();

            table_reject.fnOpen(tr, header, 'details');
            td.addClass('shown');
       } else {
            if ($(this).hasClass('shown')) {
                details.hide();
                td.removeClass('shown');
            } else {
                details.show();
                td.addClass('shown');
            }
        }
    });

    // Datatable Reject
    var table_reject = $('#table-agent-reject').dataTable({
        "bProcessing"   : true,
        "bServerSide"   : true,
        "bLengthChange" : true,
        "sServerMethod" : "GET",
        "sAjaxSource"   : loadTableReject,
        /*"aaSorting": [[5, 'desc']],*/
        "autoWidth"     : false,
        "columns": [
            {
                "render": function(data, type, row) {
                    return '<input type="hidden" id="id" value="'+ row[4] +'">';
                },
                "visible": true,
                "class": 'text-center expanding',
                "orderable": false,
                "width": '5px'
            },
            {
                "render": function(data, type, row) {
                    return row[0];
                },
            },
            {
                "render": function(data, type, row) {
                    return row[1];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[2];
                }, 
            },
            {
                "render": function(data, type, row) {
                    return row[3];
                }, 
            },
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var page = this.fnPagingInfo().iPage;
            var length = this.fnPagingInfo().iLength;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(1)', nRow).html(index);
        }
    }).fnSetFilteringDelay(1000);

    // Approve Data
    function approveData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Approve <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-agent-pending'
                });

                window.location.href = url_approve + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }

    // Reject Data
    function rejectData(value, el, name) {

        $("html, body").animate({
            scrollTop: 0
        }, 500);

        $.confirm({
            content: "Reject <b>" + name + "</b> ?",
            title: "Coution!", confirm: function () {

                App.blockUI({
                    target: '#table-agent-pending'
                });

                window.location.href = url_reject + '/' + value;
            },
            cancel: function (button) {

            },
            confirmButton: "Yes", cancelButton: "No", confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
        });
    }
</script>
@stop