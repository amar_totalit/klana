<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Destination extends REST_Controller {

    public function list_destination_post(){
        $destination = M_Destination::where('is_delete','f')->get();
        if(!empty($destination)){
            $data = array('status' => 'success', 'data' => $destination);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function view_destination_post(){
        $id_destination = $this->security->xss_clean(ucwords($this->post('id_destination')));

        $destination = M_Destination::join('ms_province', 'ms_province.id_province', '=', 'ms_destination.id_province')->whereRaw("ms_destination.is_delete = 'f' AND ms_destination.id_destination = '".$id_destination."'")->get();
        if(!empty($destination)){
            $data = array('status' => 'success', 'data' => $destination);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function search_destination_post(){
        $data_search = $this->security->xss_clean(ucwords($this->post('data_search')));

        $where = "( ms_destination.id_destination LIKE '%" . $data_search . "%'";
        $where .= " OR ms_destination.destination LIKE '%" . $data_search . "%' )";

        $destination = M_Destination::where('ms_destination.is_delete','f')->whereRaw($where)->get();
        if($destination != "[]"){
            $data = array('status' => 'success', 'message' => lang("message_data_found"), 'data' => $destination);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function save_destination_post(){
        $destination   = $this->security->xss_clean(ucwords($this->post('destination')));
        $id_province = $this->security->xss_clean(ucwords($this->post('id_province')));/* ms_province.id_province = PRV2017120001 */
        
        /* Generate Code Prefix: DST2017120001 */
        $year     = date('Y');
        $month    = date('m');
        $prefix   = array('length' => 4, 'prefix' => 'DST' . $year . $month , 'position' => 'right');
        $kode     = $this->model_general->code_prefix('ms_destination', 'id_destination', $prefix);
        
        $data_destination = M_Destination::where('destination', $destination)->first();

        if(empty($data_destination)){
            /* Initialize Data */
            $model = new M_Destination();

            $model->id_destination = $kode;
            $model->destination    = $destination;
            $model->id_province    = $id_province;
            $model->is_delete      = 'f';
                
            /* Save */
            $save = $model->save();

            if($save){
                $destination_success = M_Destination::where('id_destination',$kode)->first();
                $data = array('status' => 'success', 'message' => lang("message_save_success"), "data" => $destination_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_save_failed"));
            }
            
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function update_destination_post(){
        $id_destination = $this->security->xss_clean(ucwords($this->post('id_destination')));
        $destination    = $this->security->xss_clean(ucwords($this->post('destination')));
        $id_province    = $this->security->xss_clean(ucwords($this->post('id_province')));
        
        $model = M_Destination::where('id_destination', $id_destination)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->destination = $destination;
            $model->id_province = $id_province;
                
            /* Update */
            $update = $model->save();

            if($update){
                $destination_success = M_Destination::where('id_destination',$id_destination)->first();
                $data = array('status' => 'success', 'message' => lang("message_update_success"), "data" => $destination_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_update_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function delete_destination_post(){
        $id_destination = $this->security->xss_clean(ucwords($this->post('id_destination')));
        
        $model = M_Destination::where('id_destination', $id_destination)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date("Y-m-d H:i:s");
                 
            /* Update */
            $update = $model->save();

            if($update){
                $destination_success = M_Destination::where('id_destination',$id_destination)->first();
                $data = array('status' => 'success', 'message' => lang("message_delete_success"), "data" => $destination_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_delete_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

}