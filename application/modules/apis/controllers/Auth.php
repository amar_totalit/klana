<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function login_post()
    {
        $email    = $this->security->xss_clean($this->post('email'));
        $password = $this->security->xss_clean($this->post('password'));

        $user     = User::where('email',$email)
                        ->where('password_mobile',$password)
                        ->where('is_delete','f')
                        ->where('active','1')
                        ->first();
                        
        if ($user != null) {
            $tokenData        = array();
            $tokenData['id']  = $user->id;
            $tokenData['exp'] = Authorization::expiresTimes("+1 month");
            $data = array(
                            'status' => 'success', 
                            'message' => 'User successfuly login.',
                            'data'   =>  
                                        array(
                                            'id_user'      => $user->id, 
                                            'code_agent'   => $user->code_agent, 
                                            'code_tripper' => $user->code_tripper, 
                                            'username'     => $user->username, 
                                            'email'        => $user->email, 
                                            'first_name'   => $user->first_name, 
                                            'last_name'    => $user->last_name,
                                            'photo'        => $user->photo
                                            ),
                            'token' => Authorization::generateToken($tokenData),

                            );
            $this->set_response($data, REST_Controller::HTTP_OK);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_UNAUTHORIZED,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function logout_post()
    {
        if (Authorization::hasLogin()) {

            $header = $this->input->request_headers();
            
            Authorization::clearToken($header['Authorization']);

            $msg = [
                "status" => 'success',
                "message" => "User logout successfuly",
            ];
            $this->set_response($msg, self::HTTP_OK);
        }
    }
}
