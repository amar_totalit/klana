<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Master_province extends REST_Controller {

    public function list_province_post(){
        if (Authorization::hasLogin()) {
            $province = M_Province::where('is_delete','f')->get();
            if(!empty($province)){
                $data = array('status' => 'success', 'data' => $province);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
            }
            $this->response($data);
        }
    }

    public function search_province_post(){
        if (Authorization::hasLogin()) {
        	$data_search = $this->security->xss_clean(($this->post('data_search')));

        	$where = "(ms_province.id_province LIKE '%" . $data_search . "%'";
            $where .= " OR ms_province.province LIKE '%" . $data_search . "%')";

            $data_province = M_Province::where('ms_province.is_delete','f')->whereRaw($where)->get();
            if($data_province != '[]'){
            	$data = array('status' => 'success', 'message' => lang("message_data_found"), "data" => $data_province);
            }else{
            	$data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
            }

            $this->response($data);
        }
    }

    public function view_province_post(){
        if (Authorization::hasLogin()) {
    		$id_province   = $this->security->xss_clean(ucwords($this->post('id_province')));
    		
    		$data_province = M_Province::where("id_province",$id_province)->first();

    		if(!empty($data_province)){
    			$data = array('status' => 'success', 'message' => lang("message_data_found"), "data" => $data_province);
    		}else{
    			$data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
    		}

    		$this->response($data);
        }
    }

    public function save_province_post(){
        if (Authorization::hasLogin()) {
    		$country  = 'CTR201712001';//Default Indonesia
    		$province = $this->security->xss_clean(ucwords($this->post('province')));

            /* Generate Code Prefix: PRV2017120001 */
            $year     = date('Y');
            $month    = date('m');
            $prefix   = array('length' => 4, 'prefix' => 'PRV' . $year . $month , 'position' => 'right');
            $kode     = $this->model_general->code_prefix('ms_province', 'id_province', $prefix);
            
            $data_province = M_Province::where('province', $province)->first();

            if(empty($data_province)){
            	/* Initialize Data */
            	$model = new M_Province();

                $model->id_province = $kode;
                $model->id_country  = $country;
                $model->province    = $province;
                $model->is_delete   = 'f';
                    
            	/* Save */
                $save = $model->save();

                if($save){
                	$province_success = M_Province::where('id_province',$kode)->first();
                	$data = array('status' => 'success', 'message' => lang("message_save_success"), "data" => $province_success);
                }else{
                	$data = array('status' => 'failed', 'message' => lang("message_save_failed"));
                }

            }else{
            	$data = array('status' => 'failed', 'message' => lang("message_data_exist"));
            }

            $this->response($data);
        }
    }

    public function update_province_post(){
        if (Authorization::hasLogin()) {
        	$id_province = $this->security->xss_clean(ucwords($this->post('id_province')));
    		$province    = $this->security->xss_clean(ucwords($this->post('province')));
            
            $model = M_Province::where('id_province', $id_province)->first();

            if(!empty($model)){
            	/* Initialize Data */
                $model->province    = $province;
                    
            	/* Update */
                $update = $model->save();

                if($update){
                	$province_success = M_Province::where('id_province',$id_province)->first();
                	$data = array('status' => 'success', 'message' => lang("message_update_success"), "data" => $province_success);
                }else{
                	$data = array('status' => 'failed', 'message' => lang("message_update_failed"));
                }

            }else{
            	$data = array('status' => 'failed', 'message' => lang("message_data_exist"));
            }

            $this->response($data);
        }
    }

    public function delete_province_post(){
        if (Authorization::hasLogin()) {
        	$id_province = $this->security->xss_clean(ucwords($this->post('id_province')));
            
            $model = M_Province::where('id_province', $id_province)->first();

            if(!empty($model)){
            	/* Initialize Data */
                $model->is_delete   = 't';
                $model->delete_date = date("Y-m-d H:i:s");
                     
            	/* Update */
                $update = $model->save();

                if($update){
                	$province_success = M_Province::where('id_province',$id_province)->first();
                	$data = array('status' => 'success', 'message' => lang("message_delete_success"), "data" => $province_success);
                }else{
                	$data = array('status' => 'failed', 'message' => lang("message_delete_failed"));
                }

            }else{
            	$data = array('status' => 'failed', 'message' => lang("message_data_exist"));
            }

            $this->response($data);
        }
    }
}