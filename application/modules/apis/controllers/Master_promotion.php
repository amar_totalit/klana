<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Promotion extends REST_Controller {

    public function list_promotion_post(){
        $promotion = M_Promotion::where('is_delete','f')->get();
        if(!empty($promotion)){
            $data = array('status' => 'success', 'data' => $promotion);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function view_promotion_post(){
        $id_promotion = $this->security->xss_clean(ucwords($this->post('id_promotion')));

        $promotion = M_PromotionDetail::leftjoin('ms_promotion','ms_promotion.id_promotion', '=' , 'ms_promotion_detail.id_promotion')->leftjoin('ms_package','ms_package.id_package', '=' , 'ms_promotion_detail.id_package')->whereRaw("ms_promotion.is_delete = 'f' AND ms_promotion.id_promotion = '".$id_promotion."'")->get();
        if(!empty($promotion)){
            $data = array('status' => 'success', 'data' => $promotion);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function search_promotion_post(){
        $data_search = $this->security->xss_clean(ucwords($this->post('data_search')));

        $where = "( ms_promotion.id_promotion LIKE '%" . $data_search . "%'";
        $where .= " OR ms_promotion.promotion LIKE '%" . $data_search . "%'";
        $where .= " OR ms_promotion.periode_start LIKE '%" . $data_search . "%'";
        $where .= " OR ms_promotion.periode_end LIKE '%" . $data_search . "%' )";

        $promotion = M_Promotion::where('ms_promotion.is_delete','f')->whereRaw($where)->get();
        if($promotion != "[]"){
            $data = array('status' => 'success', 'message' => lang("message_data_found"), 'data' => $promotion);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function save_promotion_post(){
        $promotion   = $this->security->xss_clean(ucwords($this->post('promotion')));
        $first_date  = $this->security->xss_clean(ucwords($this->post('first_date')));
        $second_date = $this->security->xss_clean(ucwords($this->post('second_date')));
        $description = $this->security->xss_clean(ucwords($this->post('description')));
        
        /* Generate Code Prefix: PRM2017120001 */
        $year     = date('Y');
        $month    = date('m');
        $prefix   = array('length' => 3, 'prefix' => 'PRM' . $year . $month , 'position' => 'right');
        $kode     = $this->model_general->code_prefix('ms_promotion', 'id_promotion', $prefix);
        
        $data_promotion = M_Promotion::where('promotion', $promotion)->first();

        if(empty($data_promotion)){
            /* Initialize Data */
            $model = new M_Promotion();

            /* Upload Image Base64 Encode */
            $image_promotion = $this->post('image_promotion');
            $nama = $kode . "_" . date('Y') . date('m') . date('d') . ".png";

            $pattern = "~data:image/[a-zA-Z]*;base64,~";
            
            $encoding = preg_replace($pattern, "", $image_promotion);
            
            $decode_image = base64_decode($encoding);

            $createImgFromStr = imagecreatefromstring($decode_image);

            $imageName = "./assets/upload/promotions/" . $nama;

            $imageSave = imagejpeg($createImgFromStr ,$imageName, 100);

            $info = getimagesize($imageName);

            $extension = image_type_to_extension($info[2]);

            $image_name = $nama;
            /* End Upload Base64 Endcode */

            $model->id_promotion    = $kode;
            $model->promotion       = $promotion;
            $model->periode_start   = date('Y-m-d', strtotime($first_date));
            $model->periode_end     = date('Y-m-d', strtotime($second_date));
            $model->image_promotion = $image_name;
            $model->description     = $description;
            $model->is_delete       = 'f';
                
            /* Save */
            $save = $model->save();

            if($save){

                // Save Detail Promotion
                $id_package    = $this->security->xss_clean(ucwords($this->post('id_package')));

                /* Generate Code Prefix: PRD201712001 */
                $year_detail   = date('Y');
                $month_detail  = date('m');
                $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
                $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);

                $model_det                      = new M_PromotionDetail();
                $model_det->id_promotion_detail = $kode_detail;
                $model_det->id_promotion        = $kode;
                $model_det->id_package          = $id_package;
                $model_det->save();
                /* End Save Detail Promotion */

                $promotion_success = M_Promotion::where('id_promotion',$kode)->first();
                $data = array('status' => 'success', 'message' => lang("message_save_success"), "data" => $promotion_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_save_failed"));
            }
            
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function update_promotion_post(){
        $id_promotion = $this->security->xss_clean(ucwords($this->post('id_promotion')));
        $promotion    = $this->security->xss_clean(ucwords($this->post('promotion')));
        $first_date   = $this->security->xss_clean(ucwords($this->post('first_date')));
        $second_date  = $this->security->xss_clean(ucwords($this->post('second_date')));
        $description  = $this->security->xss_clean(ucwords($this->post('description')));
        
        $model = M_Promotion::where('id_promotion', $id_promotion)->first();

        if(!empty($model)){
            
            /* Upload Base64 Endcode */
            if ($this->input->post('image_promotion') != "") {
                if (!empty($model->image_promotion)) {
                    if (file_exists("./assets/upload/promotions/" . $model->image_promotion)) {
                        unlink("./assets/upload/promotions/" . $model->image_promotion);
                    }
                }
                $nama = $model->id_promotion . "_" . date('Y') . date('m') . date('d') . ".png";

                $pattern = "~data:image/[a-zA-Z]*;base64,~";
                
                $encoding = preg_replace($pattern, "", $image_promotion);
                
                $decode_image = base64_decode($encoding);

                $createImgFromStr = imagecreatefromstring($decode_image);

                $imageName = "./assets/upload/promotions/" . $nama;

                $imageSave = imagejpeg($createImgFromStr ,$imageName, 100);

                $info = getimagesize($imageName);

                $extension = image_type_to_extension($info[2]);

                $image_name = $nama;
            }else{
                $image_name = $model->image_promotion;
            }
            /* End Upload Base64 Endcode */

            /* Initialize Data */
            $model->promotion       = $promotion;
            $model->periode_start   = date('Y-m-d', strtotime($first_date));
            $model->periode_end     = date('Y-m-d', strtotime($second_date));
            $model->image_promotion = $image_name;
            $model->description     = $description;
                
            /* Update */
            $update = $model->save();

            if($update){
                $update_package = M_PromotionDetail::where('id_promotion', $model->id_promotion);
                $update_package->delete();

                // Save Detail Promotion
                $id_package    = $this->security->xss_clean(ucwords($this->post('id_package')));

                /* Generate Code Prefix: PRD201712001 */
                $year_detail   = date('Y');
                $month_detail  = date('m');
                $prefix_detail = array('length' => 3, 'prefix' => 'PRD' . $year_detail . $month_detail , 'position' => 'right');
                $kode_detail   = $this->model_general->code_prefix('ms_promotion_detail', 'id_promotion_detail', $prefix_detail);

                $model_det                      = new M_PromotionDetail();
                $model_det->id_promotion_detail = $kode_detail;
                $model_det->id_promotion        = $model->id_promotion;
                $model_det->id_package          = $id_package;
                $model_det->save();
                /* End Save Detail Promotion */
                
                $promotion_success = M_Promotion::where('id_promotion',$id_promotion)->first();
                $data = array('status' => 'success', 'message' => lang("message_update_success"), "data" => $promotion_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_update_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function delete_promotion_post(){
        $id_promotion = $this->security->xss_clean(ucwords($this->post('id_promotion')));
        
        $model = M_Promotion::where('id_promotion', $id_promotion)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date("Y-m-d H:i:s");
                 
            /* Update */
            $update = $model->save();

            if($update){
                $promotion_success = M_Promotion::where('id_promotion',$id_promotion)->first();
                $data = array('status' => 'success', 'message' => lang("message_delete_success"), "data" => $promotion_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_delete_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

}