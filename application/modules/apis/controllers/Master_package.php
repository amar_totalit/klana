<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Package extends REST_Controller {

    public function list_package_post(){
        $package = M_Package::where('is_delete','f')->get();
        if(!empty($package)){
            $data = array('status' => 'success', 'data' => $package);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function view_package_post(){
        $id_package = $this->security->xss_clean(ucwords($this->post('id_package')));

        $package = M_Package::whereRaw("is_delete = 'f' AND id_package = '".$id_package."'")->get();
        if(!empty($package)){
            $data = array('status' => 'success', 'data' => $package);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function search_package_post(){
        $data_search = $this->security->xss_clean(ucwords($this->post('data_search')));

        $where = "( ms_package.id_package LIKE '%" . $data_search . "%'";
        $where .= " OR ms_package.package_name LIKE '%" . $data_search . "%' )";

        $package = M_Package::where('ms_package.is_delete','f')->whereRaw($where)->get();
        if($package != "[]"){
            $data = array('status' => 'success', 'message' => lang("message_data_found"), 'data' => $package);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function save_package_post(){
        $package = $this->security->xss_clean(ucwords($this->post('package')));
        $days    = $this->security->xss_clean(ucwords($this->post('days')));
        $nights  = $this->security->xss_clean(ucwords($this->post('nights')));

        
        /* Generate Code Prefix: PCK2017120001 */
        $year     = date('Y');
        $month    = date('m');
        $prefix   = array('length' => 4, 'prefix' => 'PCK' . $year . $month , 'position' => 'right');
        $kode     = $this->model_general->code_prefix('ms_package', 'id_package', $prefix);
        
        $data_package = M_Package::where('package_name', $package)->first();

        if(empty($data_package)){
            /* Initialize Data */
            $model = new M_Package();

            $model->id_package   = $kode;
            $model->package_name = $package;
            $model->days         = $days;
            $model->nights       = $nights;
            $model->is_delete    = 'f';
                
            /* Save */
            $save = $model->save();

            if($save){
                $package_success = M_Package::where('id_package',$kode)->first();
                $data = array('status' => 'success', 'message' => lang("message_save_success"), "data" => $package_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_save_failed"));
            }
            
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function update_package_post(){
        $id_package = $this->security->xss_clean(ucwords($this->post('id_package')));
        $package    = $this->security->xss_clean(ucwords($this->post('package')));
        $days       = $this->security->xss_clean(ucwords($this->post('days')));
        $nights     = $this->security->xss_clean(ucwords($this->post('nights')));
        
        $model = M_Package::where('id_package', $id_package)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->package_name = $package;
            $model->days         = $days;
            $model->nights       = $nights;
                
            /* Update */
            $update = $model->save();

            if($update){
                $package_success = M_Package::where('id_package',$id_package)->first();
                $data = array('status' => 'success', 'message' => lang("message_update_success"), "data" => $package_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_update_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function delete_package_post(){
        $id_package = $this->security->xss_clean(ucwords($this->post('id_package')));
        
        $model = M_Package::where('id_package', $id_package)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date("Y-m-d H:i:s");
                 
            /* Update */
            $update = $model->save();

            if($update){
                $package_success = M_Package::where('id_package',$id_package)->first();
                $data = array('status' => 'success', 'message' => lang("message_delete_success"), "data" => $package_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_delete_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

}