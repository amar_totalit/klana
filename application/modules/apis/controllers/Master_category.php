<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Category extends REST_Controller {

    public function list_category_post(){
        $category = M_Category::where('is_delete','f')->get();
        if(!empty($category)){
            $data = array('status' => 'success', 'data' => $category);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function view_category_post(){
        $id_category = $this->security->xss_clean(ucwords($this->post('id_category')));

        $category = M_Category::whereRaw("is_delete = 'f' AND id_category = '".$id_category."'")->get();
        if(!empty($category)){
            $data = array('status' => 'success', 'data' => $category);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function search_category_post(){
        $data_search = $this->security->xss_clean(ucwords($this->post('data_search')));

        $where = "( ms_category.id_category LIKE '%" . $data_search . "%'";
        $where .= " OR ms_category.category LIKE '%" . $data_search . "%' )";

        $category = M_Category::where('ms_category.is_delete','f')->whereRaw($where)->get();
        if($category != "[]"){
            $data = array('status' => 'success', 'message' => lang("message_data_found"), 'data' => $category);
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_not_found"));
        }
        $this->response($data);
    }

    public function save_category_post(){
        $category   = $this->security->xss_clean(ucwords($this->post('category')));
        $created_by = $this->security->xss_clean(ucwords($this->post('created_by')));/* users.id = USR201712000001 */
        $created_on = date('Y-m-d H:i:s');
        
        /* Generate Code Prefix: CAT2017120001 */
        $year     = date('Y');
        $month    = date('m');
        $prefix   = array('length' => 4, 'prefix' => 'CAT' . $year . $month , 'position' => 'right');
        $kode     = $this->model_general->code_prefix('ms_category', 'id_category', $prefix);
        
        $data_category = M_Category::where('category', $category)->first();

        if(empty($data_category)){
            /* Initialize Data */
            $model = new M_Category();

            $model->id_category = $kode;
            $model->category    = $category;
            $model->createdby   = $created_by;
            $model->createdon   = $created_on;
            $model->is_delete   = 'f';
                
            /* Save */
            $save = $model->save();

            if($save){
                $category_success = M_Category::where('id_category',$kode)->first();
                $data = array('status' => 'success', 'message' => lang("message_save_success"), "data" => $category_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_save_failed"));
            }
            
        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function update_category_post(){
        $id_category = $this->security->xss_clean(ucwords($this->post('id_category')));
        $category    = $this->security->xss_clean(ucwords($this->post('category')));
        $changed_by  = $this->security->xss_clean(ucwords($this->post('changed_by')));
        $changed_on  = date('Y-m-d H:i:s');
        
        $model = M_Category::where('id_category', $id_category)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->category    = $category;
            $model->changedby   = $changed_by;
            $model->changedon   = $changed_on;
            $model->is_delete   = 'f';
                
            /* Update */
            $update = $model->save();

            if($update){
                $category_success = M_Category::where('id_category',$id_category)->first();
                $data = array('status' => 'success', 'message' => lang("message_update_success"), "data" => $category_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_update_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

    public function delete_category_post(){
        $id_category = $this->security->xss_clean(ucwords($this->post('id_category')));
        
        $model = M_Category::where('id_category', $id_category)->first();

        if(!empty($model)){
            /* Initialize Data */
            $model->is_delete   = 't';
            $model->delete_date = date("Y-m-d H:i:s");
                 
            /* Update */
            $update = $model->save();

            if($update){
                $category_success = M_Category::where('id_category',$id_category)->first();
                $data = array('status' => 'success', 'message' => lang("message_delete_success"), "data" => $category_success);
            }else{
                $data = array('status' => 'failed', 'message' => lang("message_delete_failed"));
            }

        }else{
            $data = array('status' => 'failed', 'message' => lang("message_data_exist"));
        }

        $this->response($data);
    }

}