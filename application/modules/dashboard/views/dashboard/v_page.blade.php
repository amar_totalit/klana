@extends('default.views.layouts.default')

@section('title') KLANA - Dashboard @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Dashboard </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-newspaper-o font-dark"></i>
                        <span class="caption-subject">Dashboard</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')

@stop