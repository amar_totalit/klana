<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Model {

    protected $table = "tb_participant";

    function finds($group) {
        $this->db->select('tb_participant.*, tb_lokasi.lokasi, tb_kendaraan.kendaraan');
        $this->db->from('tb_participant');
        $this->db->join('tb_kendaraan', 'tb_kendaraan.id_kendaraan = tb_participant.id_kendaraan');
        $this->db->join('tb_lokasi', 'tb_lokasi.id_lokasi = tb_participant.id_lokasi');
        // $this->db->join('provinces', 'provinces.id = participant.province', 'left');
        // $this->db->join('cities', 'cities.id = participant.city', 'left');
        if ($group == 'Admin' ) {
        }elseif ($group == 'Sales%20Area') {
            $this->db->where('tb_participant.approve_by_sales = "T"');
        }elseif ($group == 'Dealer'){
            $this->db->where('tb_participant.status_absen = "T"');
        }elseif ( $group == 'HC3') {
            $this->db->where('tb_participant.status_hc3 = "A"');
        }
        // $this->db->where('registration_date >=',$start_date);
        // if ($payment_method != '0') {
        //     if ($payment_method == 'transfer') {
        //         $where= 'payment_by = BRI OR payment_by = BCA OR payment_by = BNI OR payment_by = mandiri';
        //         $this->db->where($where);
        //     }else{
        //         $this->db->where('payment_by',$payment_method);
        //     }
        // }
        // if ( $registration_type != '0') {
        //     $this->db->where('status_register',$registration_type);
        // }

        $this->db->order_by('tb_participant.tgl_registrasi', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
}
