<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MX_Controller {
	public $site        = "";
    public $module      = "";
    public $folder      = "";
    public $class       = "";

	public function __construct() {
        parent::__construct();

        /* Dynamical controller */
        $this->module  = $this->router->fetch_module();
        $this->folder  = $this->uri->segment(2);
        $this->class   = $this->router->fetch_class();
        $this->site    = $this->module . '/' . $this->class;

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }       
    }

	public function index()
	{
        $data['add']       = site_url() . $this->site . '/add';
        $data['edit']      = site_url() . $this->site . '/edit';
        $data['delete']    = site_url() . $this->site . '/delete';
        $data['loadTable'] = site_url() . $this->site . '/loadTable';
        $data['detail']    = site_url() . $this->site . '/detail';

		$this->load_view('faq','faq','v_faq',$data);
	}

	/**
    * Serverside load table:tb_faq
    * @return ajax
    **/
	public function loadTable()
	{
		$database_columns = array('id_faq','id_faq','created_on','title','status','id_faq');

        $from  = "tb_faq";
        $where = "tb_faq.is_delete = 'F'";

        $order_by = 'id_faq desc';
        if ($this->input->get('sSearch') != '') {
            $sSearch = str_replace(array('.', ','), '', $this->db->escape_str($this->input->get('sSearch')));
            $where .= " AND (id_faq LIKE '%" . $sSearch . "%'";
            $where .= "OR created_on LIKE '%" . $sSearch . "%' ";
            $where .= "OR title LIKE '%" . $sSearch . "%' ";
            $where .= "OR status LIKE '%" . $sSearch . "%')";
        }
		
		$iSortCol_0   = $this->input->get('iSortCol_0', true);
		$iSortingCols = $this->input->get('iSortingCols', true);

        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol  = $this->input->get('iSortCol_'.$i, true);
                $bSortable = $this->input->get('bSortable_'.intval($iSortCol), true);
                $sSortDir  = $this->input->get('sSortDir_'.$i, true);

                if($bSortable == 'true')
                {
                    $this->db->order_by($database_columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        $this->datatables->set_index('id_faq');
        $this->datatables->config('database_columns', $database_columns);
        $this->datatables->config('from', $from);
        $this->datatables->config('where', $where);
        $this->datatables->config('order_by', $order_by);

		$selected_data = $this->datatables->get_select_data();
		$aa_data       = $selected_data['aaData'];
		$new_aa_data   = array();

        foreach ($aa_data as $row) {
            
			$row_value     = array();
			$row_value[]   = $row->id_faq;
            $row_value[]   = date('d F Y H:i', strtotime($row->created_on));
            $row_value[]   = $row->title;
            if ($row->status == 'P') {
                $row_value[]   = 'Published';
            }else{
                $row_value[]   = 'Unpublished';
            }
			$row_value[]   = encryptID($row->id_faq);
			$new_aa_data[] = $row_value;
        }

        $selected_data['aaData'] = $new_aa_data;
        $this->output->set_content_type('application/json')->set_output(json_encode($selected_data));
	}

	/**
    * Direct to page input data
    * @return page
    **/
    function add()
    {   
        $data['status_options']   = $this->m_select->selectStatusPublish();

		$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/save';

    	$this->load_view('faq','faq','v_faq_add',$data);
    }

	/**
    * Save data to table:tb_faq
    * @param Post Data
    * @return page index
    **/
    function save()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user      = $this->ion_auth->user()->row();
            $title     = ucwords($this->input->post('title'));
            $content   = ucwords($this->input->post('content'));
            $status    = ucwords($this->input->post('status'));
            $created_on = date('Y-m-d H:i:s');
			
			/* Generate Code Prefix: FAQ201712001 */
			$year    = date('Y');
			$month   = date('m');
			$prefix  = array('length' => 3, 'prefix' => 'FAQ' . $year . $month , 'position' => 'right');
			$kode    = $this->model_general->code_prefix('tb_faq', 'id_faq', $prefix);
            
            $data_faq = M_Faq::where('title', $title)->first();

            if(empty($data_faq)){
            	/* Initialize Data */
            	$model = new M_Faq();

            	$image_name = '';

                /** Upload Image * */
                if ($this->input->post('image_exist') != "") {
                    if ($_FILES['filename']['error'] != 4) {

                        /* Config Upload */
                        $config['upload_path']   = './assets/upload/faq';
                        $config['allowed_types'] = '*';
                        $config['file_name']     = $kode . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite']     = TRUE;
                        
                        $this->upload->initialize($config);
                        
                        if ($this->upload->do_upload('filename')) {
                            $datafile   = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        } else {
                            $status = array('status' => 'error');
                        }
                    }
                } else {
                    $status = array('status' => 'image-blank');
                }
                /** END Upload Image **/   

                $model->id_faq = $kode;
                $model->title       = $title;
                $model->content     = $content;
                $model->file_name   = $image_name;
                $model->created_on  = $created_on;
                $model->status      = $status;
                $model->is_delete   = 'f';
                    
            	/* Save */
                $save = $model->save();
                if($save){
	            	/* Write Log */
            	    $data_notif = array(
                        "Code FAQ"   => $kode,
                        "Title"      => $title,
                        "Content"    => $content,
                        "File Name"  => $image_name,
                        "Status"     => $status,
                        "Created On" => $created_on,
                    );

                    $message = $user->first_name . " " . $user->last_name . " add to data  " . $title;
                    $this->activity_log->create($user->id, json_encode($data_notif), NULL, NULL, $message, 'C', 9);

		        	$this->session->set_flashdata('success', lang("message_save_success"));
		            redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_save_failed"));
           			redirect(site_url() . $this->site);
                }
            }else{
            	$this->session->set_flashdata('error', lang("message_data_exist"));
           		redirect(site_url() . $this->site);
            }
        } else {
        	$this->session->set_flashdata('error', lang("message_save_failed"));
           	redirect(site_url() . $this->site);
        }
    }

    /**
    * Direct to page input data
    * @return page
    **/
    function edit($id)
    {        
        $data['status_options']   = $this->m_select->selectStatusPublish();
       	$data['cancel'] = site_url() . $this->site;
		$data['action'] = site_url() . $this->site . '/update';
		$id_faq     = decryptID($id);
		
		$faq        = M_Faq::where('id_faq',$id_faq)->first();
		if(!empty($faq)){

			$data['faq'] = $faq;

    		$this->load_view('faq','faq','v_faq_edit',$data);
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
           	redirect(site_url() . $this->site);
		}
    }

    /**
    * Update data to table:tb_faq
    * @param Post Data
    * @return page index
    **/
    function update()
    {
        /* Get Data Post */
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user    = $this->ion_auth->user()->row();
            $id_faq  = decryptID($this->input->post('id_faq'));
            $title   = ucwords($this->input->post('title'));
            $content = ucwords($this->input->post('content'));
            $status  = ucwords($this->input->post('status'));
            
            
            $model   = M_Faq::where('id_faq',$id_faq)->first();

			if(!empty($model)){
				$data_old = array(
                    "Code FAQ"   => $model->id_faq,
                    "Title"      => $model->title,
                    "Content"    => $model->content,
                    "File Name"  => $model->file_name,
                    "Status"     => $model->status,
                    "Created On" => $model->created_on,
                );

				/* Initialize Data */

				/* Update Image */
				if ($this->input->post('image_exist') != "") {
                    if ($_FILES['filename']['error'] != 4) {

                        /* IF File Exist */
                        if (file_exists("./assets/upload/faq/" . $model->file_name)) {
                            unlink("./assets/upload/faq/" . $model->file_name);
                        }

                        /* Config Upload */
                        $config['upload_path'] = './assets/upload/faq';
                        $config['allowed_types'] = '*';
                        $config['file_name'] = $model->id_faq . "_" . date('Y') . date('m') . date('d');
                        $config['overwrite'] = TRUE;
                        
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('filename')) {
                            $datafile = $this->upload->data();
                            $image_name = $datafile['file_name'];
                        }
                    }
                } else {
                    $image_name = $model->file_name;
                }
                /* end upload img */

                $model->title      = $title;
                $model->content    = $content;
                $model->file_name  = $image_name;
                $model->status     = $status;

            	/* Update */
                $update = $model->save();

                if($update){
                	$data_new = array(
                        "Code FAQ"   => $kode,
                        "Title"      => $title,
                        "Content"    => $content,
                        "File Name"  => $image_name,
                        "Status"     => $status,
                        "Created On" => $createdon,
                    );

                	/* Write Log */
                	$data_change = array_diff_assoc($data_new, $data_old);
                    $message = "FAQ " . $title . " is successfuly changed by " . $user->first_name . " " . $user->last_name;
                    $this->activity_log->create($user->id, json_encode($data_new), json_encode($data_old), json_encode($data_change), $message, 'U', 9);

                    $this->session->set_flashdata('success', lang("message_update_success"));
                    redirect(site_url() . $this->site);
                }else{
                	$this->session->set_flashdata('error', lang("message_update_failed"));
                    redirect(site_url() . $this->site);
                }
			}else{
				$this->session->set_flashdata('error', lang("message_data_not_found"));
                redirect(site_url() . $this->site);
			}
        } else {
        	$this->session->set_flashdata('error', lang("message_update_failed"));
            redirect(site_url() . $this->site);
        }
    }


    /**
    * Detail data from table:tb_faq
    * @param Id
    * @return page index
    **/
    public function detail($id) {
        $data['back']       = site_url() . $this->site;
        
        $id_faq             = decryptID($id);
        $faq                = M_Faq::find($id_faq);
        $data['created_on'] = date('d F Y h:i', strtotime($faq->created_on));
        $data['faq']        = $faq;
		$this->load_view('faq','faq','v_faq_detail',$data);
    }


    /**
    * Delete data from table:tb_faq => Soft Delete
    * @param Id
    * @return page index
    **/
    function delete($id)
    {
        $user   = $this->ion_auth->user()->row();
        $id_faq = decryptID($id);
        
        $model  = M_Faq::where('id_faq',$id_faq)->first();

		if(!empty($model)){

			/* Initialize Data */
			$model->is_delete   = 't';
			$model->delete_date = date('Y-m-d H:i:s');

            /* Update */
            $update = $model->save();

            if($update){
            	/* Write Log */
            	 $data_notif = array(
					"Code FAQ"   => $model->id_faq,
                    "Title"      => $model->title,
                    "Content"    => $model->content,
                    "File Name"  => $model->file_name,
                    "Status"     => $model->status,
                    "Created On" => $model->created_on,
                );
                $message = "FAQ " . $model->title . " is successfuly deleted by " . $user->first_name . " " . $user->last_name;
                $this->activity_log->create($user->id, NULL, json_encode($data_notif), NULL, $message, 'D', 9);

            	$this->session->set_flashdata('success', lang("message_delete_success"));
				redirect(site_url() . $this->site);
            }else{
            	$this->session->set_flashdata('error', lang("message_delete_failed"));
				redirect(site_url() . $this->site);
            }
		}else{
			$this->session->set_flashdata('error', lang("message_data_not_found"));
			redirect(site_url() . $this->site);
		}
    }
}