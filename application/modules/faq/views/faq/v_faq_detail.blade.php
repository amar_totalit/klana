@extends('default.views.layouts.default')

@section('title') KLANA - FAQ @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">FAQ</a>
            	<i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Detail FAQ</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Detail FAQ </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    
    <div class="row">
		<div class="col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
                    <div class="caption font-dark">
                        <a href="{{$back}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>{{lang('button_back')}}</a>
                    </div>
                </div>
                <div class="portlet-body">
            		<div class="row">
            			<div class="col-md-7 col-md-offset-1">
            				<div class="row">
            					<div class="col-md-4"><h4>Code FAQ</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$faq->id_faq}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Status</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7">
	            					@if($faq->status == 'P')
            							<h4>Published</h4>
            						@else
            							<h4>Unpublished</h4>
            						@endif
            					</div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Title</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$faq->title}}</h4></div>	
            				</div>
            				<div class="row">
            					<div class="col-md-4"><h4>Date</h4></div>
	            				<div class="col-md-1"><h4>:</h4></div>
	            				<div class="col-md-7"><h4>{{$created_on}}</h4></div>	
            				</div>
            			</div>
	                	<div class="col-md-4">
	                		@if(!empty($faq->file_name))
	                		<img src="{{ base_url() . 'assets/upload/faq/' .$faq->file_name }}" height="200" width="300px">
	                		@endif
	                	</div>
            		</div>
                    <br>
                    <div class="row">
                        <div class="col-md-12  col-md-offset-1">
                            <div class="row">
                                <div class="col-md-2"><h4>Content</h4></div>
                                <div class="col-md-1" align="center"><h4>:</h4></div>
                                <div class="col-md-7"><h4>{{$faq->content}}</h4></div>  
                            </div>
                        </div>
                    </div>    	
				</div>
				<br>
			</div>
		</div>
	</div>

</div>

@stop

@section('scripts')

@stop