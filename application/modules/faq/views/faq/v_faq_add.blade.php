@extends('default.views.layouts.default')

@section('title') KLANA - FAQ @stop

@section('body')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
   
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ base_url()."dashboard" }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">FAQ</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Add</a>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> FAQ </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <div class="caption">
                        <i class="fa fa-edit"></i>
                        <span class="caption-subject">Form FAQ</span>
                    </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <span class="text-danger">(*) Required</span>

                        {{ form_open($action, array('id' => 'form-faq', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}

                            {{ form_input(array('id' => 'image_exist','name' => 'image_exist','type' => 'hidden')) }}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Title <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        {{ form_input(array('type' => 'text','name' => 'title','class' => 'form-control', 'value' => ''))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Content <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-8">
                                        {{ form_textarea('content', '','id="content" class="form-control ckeditor" ') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Photo <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                        <input type="file" name="filename" id="filename" onchange="ValidateImage(this, 'cover');" style="display: none;">
                                        <div align="center">
                                            <img src="{{ base_url() . 'assets/img/no_images.png' }}" class="img-responsive" id="image-preview" style="cursor: pointer;max-height:200px;height:200px" onclick="$('#filename').click();">
                                            <br>
                                            <button id="btn-image" type="button" class="btn blue btn-sm" onClick="$('#filename').click()">
                                                <i class="fa fa-upload"></i> Upload Image
                                            </button>
                                            <button id="btn-remove" type="button" class="btn red btn-sm" onclick="clearImage()">
                                                <i class="fa fa-remove"></i> Delete
                                            </button>
                                        </div>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status <span style="color: red">*</span></label>
                                    <label class="control-label col-md-1 titikdua">:</label>
                                    <div class="col-md-4">
                                       {{ form_dropdown('status', $status_options, '', "class='form-control' data-live-search='true'"); }}
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-actions text-right">
                                <button type="submit" class="save btn blue btn-sm" title="{{lang('button_insert')}}"><i class="fa fa-save"></i> {{lang('button_insert')}}</button>
                                <a href="{{$cancel}}" class="cancel btn default btn-sm" title="{{lang('button_cancel')}}"><i class="fa fa-times"></i>{{lang('button_cancel')}}</a>
                            </div>

                        {{ form_close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    // Pengaturan Form Validation 
    var form_validator = $("#form-faq").validate({
        errorPlacement: function(error, element) {
            $(element).parent().closest('.form-group').append(error);
        },
        errorElement: "span",
        rules: {
                title: {
                    required: true,
                    maxlength: 30
                },
                content: {
                    // required: function(textarea) {
                    //     CKEDITOR.instances['content'].updateElement(); // update textarea
                    //     var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                    //     return editorcontent.length === 0;
                    // }
                },
                status: {
                    required: true,
                },
            },
        messages: {
            
        },
    });

    // Upload images
    var _validFileExtensions = [".jpg", ".png"];
    var image_array = [];
    function ValidateImage(oInput, type, i) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        readURL(oInput, type, i);
                        break;
                    }
                }
                if (!blnValid) {
                    toastr.error('File Format Not Allowed', 'Notifikasi!');
                    $('#image-preview').attr('src', "{{base_url()}}assets/img/no_images.png");
                    oInput.value = "";
                    return false;
                }
            }
        }
        return true;
    }

    function readURL(input, type, i) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (i != undefined) {
                    $('#image_' + i + '-preview').attr('src', e.target.result);
                    image_array.push(type + "_" + i);
                    $("input[name='image_update_" + i + "']").val("yes");
                    $("input[name='delete_" + type + "_"+ i +"']").val("no");
                } else {
                    $('#image-preview').attr('src', e.target.result);
                }
            };
            
            if(type == 'cover'){
                $("#image_exist").val("yes");
                $("#btn-remove").show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    /* end upload image */

    ///////////////////////////////////
</script>
@stop