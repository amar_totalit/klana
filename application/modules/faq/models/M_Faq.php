<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class M_Faq extends Eloquent {

	public $table      = 'tb_faq';
	public $primaryKey = 'id_faq';
	public $timestamps = false;

}